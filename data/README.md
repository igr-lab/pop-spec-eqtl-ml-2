
# data subfolders

**Note**: Since data is not to be stored / versioned using git, this folder just contains READMEs documenting where to find data (with links), sample information on data, and column data / labels. For information on data processing see relevant scripts. You can work out which scripts are relevant using this overview of the [general pipeline](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/pipeline_overview.html).  


| Subfolder | Description | When accessed download site (dd/mm/yyyy) | 
| --- | --- | --- | 
| **duplicated_genes_database** | Containts datasets from the [Duplication Genes Database](http://dgd.genouest.org/browse) on the duplication status of genes. Further information can be found in [Ouedraogo and Bettembourg et al. 2012](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0050653) | 01/11/2021 | 
|**encode_dnase_motif**| Contains DNAse-seq HINT20 footprints .bed files from http://data.nemoarchive.org/other/grant/sament/sament/footprint_atlas/bed/, relevant paper [ENCODE, Funk et al 2020](https://www.cell.com/cell-reports/pdf/S2211-1247(20)31014-7.pdf)) | 26/08/2021 | 
|**eqtl_summary_stats**| Contain eQTL summary statistics from multiple studies. Check out the README.md for sample information for each study population eQTL summary statistics. Subfolders ([**Mogil_2018**](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1007586), [**Natri_2020**](https://www.biorxiv.org/content/10.1101/2020.09.25.313726v2), [**Shang_2020**](https://www.cell.com/ajhg/fulltext/S0002-9297(20)30078-1)) contain READMEs about column data in their respective summary statistics. Also contains minor allele frequency data, if this was available for the study population and if it already combined with summary statistics. | Varies by study, latest 03/11/2021 | 
| **gene_function_or_ontology_resources** | Lists of genes or gene ontology terms which form particular functional groups. | 18/09/2021 | 
| **gnomad_resources** | Resources downloaded from [https://gnomad.broadinstitute.org/downloads](https://gnomad.broadinstitute.org/downloads), for example, gnomAD calculated constraint scores and LD scores for gnomAD populations. | Various, from 20/06/2021 - 1/12/2021 | 
| **gtex_resources** | Resources downloaded from [https://gtexportal.org/home/datasets](https://gtexportal.org/home/datasets). Includes expression information, mashr information, and the high confidence 'population-biased' SNPs identified in GTEx 2020| 15/08/2021 | 
| **indo_testing_data** | Contains datasets of a small subset of eQTLs to use as testing data (from Natri et al. 2020 - random 100,000 eQTL mashr was applied to) | Got directly from Heini 02/04/2021 | 
| **illumina_array_info** | Contains information for mapping illumina array probes to the genome, and which illumina probes do not mapp where they should | Various, from 08/10/2021 - 17/11/2021 | 
| **interpro_information** | Resources about Interpro accession IDs from https://www.ebi.ac.uk/interpro/download/. Includes release information, entry information, and mapping between child and parent terms. More information [here](https://interpro-documentation.readthedocs.io/_/downloads/en/latest/pdf/) and [Blum et al. 2021](https://academic.oup.com/nar/article/49/D1/D344/5958491)| 03/11/2021 | 




