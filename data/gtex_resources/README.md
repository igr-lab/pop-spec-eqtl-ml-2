

# GTEx Resources 


| File | Description |Taken from | When accessed | 
| --- | --- | ---| --- | 
| GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_reads.gct | File of gene-wise GTEx read counts across samples | [https://gtexportal.org/home/datasets](https://gtexportal.org/home/datasets) | 20/06/2021 | 
| GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct | File of gene-wise GTEx median tpm across tissues | [https://gtexportal.org/home/datasets](https://gtexportal.org/home/datasets) | 20/06/2021 | 
| GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_tpm.gct | File of gene-wise GTEx tpm across samples | [https://gtexportal.org/home/datasets](https://gtexportal.org/home/datasets) | 12/08/2021 | 
| GTEx_Analysis_v8_Annotations_SampleAttributesDS.txt | File matching GTEx sample ID to various sample attributes (tissue, sex, age bracket etc.) | [https://gtexportal.org/home/datasets](https://gtexportal.org/home/datasets) | 15/08/2021 | 
|gtex_v8_cis_qtls_mashr.tar| Cross-tissue QTL effect size estimates from mashr (Urbut et al., 2019)| [https://gtexportal.org/home/datasets](https://gtexportal.org/home/datasets) | 20/06/2021 | 
|GTEx_v8_pb_eQTLs.fdr_0.25_high_confidence_set.xlsx | High-confidence (FDR < 0.25) population biased eQTLs (pb-eQTLs)| [https://gtexportal.org/home/datasets](https://gtexportal.org/home/datasets)| 20/06/2021 | 
| GTEx_Analysis_2017-06-05_v8_RSEMv1.3.0_transcript_tpm.gct.gz | 31/10/2021 | Transcript TPMs | [https://gtexportal.org/home/datasets](https://gtexportal.org/home/datasets) |



