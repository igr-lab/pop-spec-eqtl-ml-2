
# Gene ontology and gene functon resources

## List of files in the folder:

---

### NIGO_OWL.owl 
* Accessed: 18/09/2021
* From: https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-11-458#MOESM6
* The Neural/Immune Gene Ontology: clipping the Gene Ontology for neurological and immunological systems
* GO terms in neural / immune processes

### innatedb_curated_genes.xls
* Accessed: 18/09/2021
* From: http://www.innatedb.com/annotatedGenes.do?type=innatedb
* Curated list of genes involved the innate immune response (InnatedDB list)

### GeneList.txt
* Accessed: 18/09/2021
* https://www.immport.org/shared/genelists
* Webpage lists last update as July 2020
* List of immport annotated immunology genes

### GeneListGOAnnotations.txt
* Accessed: 18/09/2021
* https://www.immport.org/shared/genelists
* Webpage lists last update as July 2020
* List of immport annotated immunology genes - with their GO annotations



## Future ideas / resources to consider: 

---

* immport and innatedb resources gene symbols rather than ensembl_gene_ids -  resource to consider for converting gene symbols to ensembl_gene_id
https://bioinformatics.stackexchange.com/questions/5229/converting-gene-symbol-to-ensembl-id-in-r

(Not added but maybe will add?)
Other curated gene lists (?)
* https://innatedb.com/redirect.do?go=resourcesGeneLists
