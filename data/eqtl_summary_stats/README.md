
# **This folder contains cis-eQTL summary statistics directly taken from previously conducted studies...**

> The structure of this folder is such that each each tissue has an 'anchor' study, for which subfolders in this folder are named after. Each subfolder contains eQTL summary statistics from the study they are named after, and for studies in the same tissue. (within a ./similar_eqtl_studies subfolder within this subfolder). 

> For all eQTL studies considered see this [table](https://docs.google.com/document/d/16ze1qF_SBkI8_fLIEhcF6AMMAdLLcBH1q62Z1sltb1o/edit?usp=sharing), otherwsie basic information on the studies can be found below, and in the relevant subfolder. 


<br> </br> 

# Folder: Mogil_2018 (Studies in monocytes) 

***

### Anchor / motiviating study: Mogil et al. 2018

- Article: [https://doi.org/10.1371/journal.pgen.1007586](https://doi.org/10.1371/journal.pgen.1007586)
- Summary statistics from: https://github.com/WheelerLab/DivPop
- Expression data from CD14 + monocytes
- Microarray study
- Three study populations:

   1. **African American** [AFA, n = 233]
   2. **Hispanic** [HIS, n = 352]
   3. **European American** [CAU, n = 578]
   
- Summary statistics from similar eQTL studies we're taken from the eQTL catalogue. Information on them is can be found in Mogil_2018/README.md


<br> </br> 


# Folder: Natri_2020 (Studies in whole blood) 

***

### Anchor / motiviating study: Natri et al. 2020 

- Preprint: https://doi.org/10.1101/2020.09.25.313726
- Expression data from whole blood tissue samples
- RNAseq study
- Four datasets/study populations:

    1. **Indonesian**: from Natri et al. 2020 paper [Indo, n = 115]
    2. **European**: Buil et al. 2015 [Twins UK, n =  433]
    3. **European**: Lepik et al. 2017 [Estonian Biobank, n = 670] 
    4. **European**:  GTEx. 2020 [GTEx, n =  491]
    
- eQTL summary statistics similar eQTL studies we're taken from the eQTL catalogue. Information on them is can be found in Natri_2020/README.md

<br> </br> 



# Folder: Shang_2020 (Studies in lymphoblastoid cells) 

*** 

### Anchor / motiviating study: Shang and Smith et al. 2020 

- Article: https://doi.org/10.1016/j.ajhg.2020.03.002
- Expression data taken from lymphoblastoid cell lines 
- Microarray study
- Two populations:
   1. **African American** (AA)
   2. **European American** (EA)
- Summary statistics for both populations taken from: [http://www.xzlab.org/data.html](http://www.xzlab.org/data.html)
- eQTL summary statistics similar eQTL studies we're taken from the eQTL catalogue. Information on them is can be found in Shang_2020/README.md


