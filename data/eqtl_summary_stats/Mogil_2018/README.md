
# File descriptions: 

### For full study details see this [table](https://docs.google.com/document/d/16ze1qF_SBkI8_fLIEhcF6AMMAdLLcBH1q62Z1sltb1o/edit?usp=sharing)

| Subfolder | File | Relevant study name | Population Description | When accessed download site (dd/mm/yyyy) | Download site | 
| --- | --- | --- | --- | --- |  --- | 
| Mogil_2018 | AFA_cis_eqtl_summary_statistics.txt |  Mogil et al 2018 | African American population only | 20/05/2021 | https://github.com/WheelerLab/DivPop |
| Mogil 2018 | AFHI_cis_eqtl_summary_statistics.txt | Mogil et al 2018 | African American and Hispanic populations | 20/05/2021 | https://github.com/WheelerLab/DivPop | 
| Mogil 2018 | ALL_cis_eqtl_summary_statistics.txt |  Mogil et al 2018 | All Mogil 2018 populations (African American, Hispanic, and European American) | 20/05/2021 | https://github.com/WheelerLab/DivPop |
| Mogil 2019 | CAU_cis_eqtl_summary_statistics.txt |  Mogil et al. 2018 | European American population only | 20/05/2021 | https://github.com/WheelerLab/DivPop |
| Mogil 2018 | HIS_cis_eqtl_summary_statistics.txt | Mogil et al. 2018 | Hispanic population only | 20/05/2021 | https://github.com/WheelerLab/DivPop |  
| Mogil_2018/similar_eqtl_studies | BLUEPRINT_ge_monocyte.all.tsv |  BLUEPRINT | European superpopulation assigned by the eQTL catalogue | 02/09/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/BLUEPRINT/ge/BLUEPRINT_ge_monocyte.all.tsv.gz |
| Mogil_2018/similar_eqtl_studies | CEDAR_microarray_monocyte_CD14.all.tsv | CEDAR |  European superpopulation assigned by the eQTL catalogue | 02/10/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/CEDAR/microarray/CEDAR_microarray_monocyte_CD14.all.tsv.gz |
| Mogil_2018/similar_eqtl_studies | Fairfax_2014_microarray_monocyte_CD14.all.tsv | Fairfax et al 2014 | Majority European superpopulation assigned by eQTL catalogue | 02/09/2021 | 	ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/Fairfax_2014/microarray/Fairfax_2014_microarray_monocyte_naive.all.tsv.gz | 
| Mogil_2018/similar_eqtl_studies | Quach_2016_ge_monocyte_naive.all.tsv | Quach et al 2016 | African and European superpopulation assigned by the eQTL catalogue | 02/09/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/Quach_2016/ge/Quach_2016_ge_monocyte_naive.all.tsv.gz | 
| Mogil_2018/similar_eqtl_studies | Quach_2016_tx_monocyte_naive.all.tsv | Quach et al 2016 | African and European superpopulation assigned by the eQTL catalogue | 03/11/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/Quach_2016/tx/Quach_2016_tx_monocyte_naive.all.tsv.gz | 
