

# (1) File descriptions: 

### For full study details see this [table](https://docs.google.com/document/d/16ze1qF_SBkI8_fLIEhcF6AMMAdLLcBH1q62Z1sltb1o/edit?usp=sharing)


| Subfolder | File | Relevant study name | Population Description | File description | When accessed download site (dd/mm/yyyy) | Download site | 
| --- | --- | --- | --- | --- |  --- | --- | 
| Shang_2020 | EA_summary_statistics.txt | Shang and Smith et al. 2020 | European American | eQTL summary statistics | 20/05/2021 | http://www.xzlab.org/data.html | 
| Shang_2020 | AA_summary_statistics.txt | Shang and Smith et al. 2020 | African American | eQTL summary statistics | 20/05/2021 | http://www.xzlab.org/data.html | 
| Shang_2020/similar_eqtl_studies | EUR373.gene.cis.FDR5.all.rs137.txt | GEUVADIS | European | eQTL summary statistics, FDR < 0.05 | 23/06/2021 | https://www.ebi.ac.uk/arrayexpress/files/E-GEUV-1/analysis_results/ | 
| Shang_2020/similar_eqtl_studies | YRI89.gene.cis.FDR5.all.rs137.txt | GEUVADIS | Yoruba (African) | eQTL summary statistics, FDR < 0.05 | 23/06/2021 | https://www.ebi.ac.uk/arrayexpress/files/E-GEUV-1/analysis_results/ | 
| Shang_2020/similar_eqtl_studies | GEUVADIS_ge_LCL.all.tsv | GEUVADIS | European and Yoruba (African) | eQTL summary statistics | 20/05/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/GEUVADIS/ge/GEUVADIS_ge_LCL.all.tsv.gz | 
| Shang_2020/similar_eqtl_studies | TwinsUK_ge_LCL.all.tsv | TwinsUK | European superpopulation assigned by the eQTL catalogue  | eQTL summary statistics | 20/05/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/TwinsUK/ge/TwinsUK_ge_LCL.all.tsv.gz | 
| Shang_2020/similar_eqtl_studies | GTEx_ge_LCL.all.tsv | GTEx | Majority assigned majority European ancestry superpopulation by eQTL catalogue | eQTL summary statistics | 20/05/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/GTEx/ge/GTEx_ge_LCL.all.tsv.gz | 


# (2) Columns (for Shang and Smith et al. summary statistics): 

These are the columns for EA_summary_statistics.txt and AA_summary_statistics.txt: 

| column_head |	description | 
| --- | --- | 
| chr| chromosome |
| GENE | ENSG id | 
| rs  | rsid hg19 | 
| ps  | base pair positions on the chromosome | 
| allele1 | minor allele | 
| allele0 | major allele | 
| af | allele frequency | 
| beta | beta estimates | 
| se | standard errors for beta | 
| p_wald | p values from Wald test | 
