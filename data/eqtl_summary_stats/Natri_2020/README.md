
# (1) Subfolder descriptions: 


| Subfolder | Description | 
| --- | --- | 
| similar_eqtl_studies | eQTL mapping studies which have been added to Natri et al. 2020 Indonesian eQTL summary statistics. These datasets are from the eQTL catalogue. | 
| other_whole_blood | eQTL mapping studies which have not been added to Natri et al. 2020 Indonesian eQTL summary statistics. To see why these datasets weren't added check the relevant entry in this [table](https://docs.google.com/document/d/16ze1qF_SBkI8_fLIEhcF6AMMAdLLcBH1q62Z1sltb1o/edit?usp=sharing).| 

<br> </br> 

# (2) File descriptions: 

### For full study details see this [table](https://docs.google.com/document/d/16ze1qF_SBkI8_fLIEhcF6AMMAdLLcBH1q62Z1sltb1o/edit?usp=sharing)


| Subfolder | File | Relevant study name | Population Description | File description | When accessed download site (dd/mm/yyyy) | Download site | 
| --- | --- | --- | --- | --- |  --- | --- | 
| Natri_2020 | beta_matrix.tsv | Natri et al. 2020 | Indonesian p| Matrix of all eQTL slopes / betas for Natri et al. 2020, includes Indonesian population, GTEx, Lepik, TwinsUK |  31/05/2021 |  Got directly from Heini | 
| Natri_2020 | indonesia_varinfo.tsv | Natri et al. 2020 | Indonesian | Information on the variants investigated in the Indonesia population dataset from Natri et al. 2020 (minor allele frequency, reference and alternative alleles) | 10/07/2021 | Got directly from Heini | 
| Natri_2020 | Natri_2020_indo_blood.tsv | Natri et al. 2020 | Indonesian | eQTL summary statistics (columns listed below) | 20/05/2021 | Got directly from Heini | 
| Natri_2020/similar_eqtl_studies | GTEx_ge_blood.all.tsv | GTEx 2020 | Predominately European | eQTL summary statistics from the eQTL catalogue | 20/05/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/GTEx/ge/GTEx_ge_blood.all.tsv.gz | 
| Natri_2020/similar_eqtl_studies | Lepik_2017_ge_blood.all.tsv | Lepik et al. 2017 | Predominately European | eQTL summary statistics from the eQTL catalogue | 20/05/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/Lepik_2017/ge/Lepik_2017_ge_blood.all.tsv.gz | 
| Natri_2020/similar_eqtl_studies | TwinsUK_ge_blood.all.tsv | TwinsUK | Predominately European | eQTL summary statistics from the eQTL catalogue | 20/05/2021 | ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/csv/TwinsUK/ge/TwinsUK_ge_blood.all.tsv.gz | 
| Natri_2020/other_whole_blood | HGVDeQTL-V8_1-cis.tar | Narahara et al. 2014 | Japanese | Compressed eQTL summary statistics / variation information | 30/08/2021 | http://www.genome.med.kyoto-u.ac.jp/SnpDB/; Relevant paper: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0100924#s4 | 
| Natri_2020/other_whole_blood | Korean_CD_eQTL_analysis_135164eQTLs.txt | Korean CD | Korean | eQTL summary statistics, FDR < 0.05 | 30/08/2021 | http://asan.crohneqtl.com/ | 
| Natri_2020/other_whole_blood | local.SNP.all.results.tar.gz |  Narahara et al. 2014 | Japanese | SNP / variant information / minor allele frequency table | 30/08/2021 | http://www.genome.med.kyoto-u.ac.jp/SnpDB/; Relevant paper: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0100924#s4 | 
| Natri_2020/other_whole_blood | probeID.table |  Narahara et al. 2014 | Japanese | Expression probe table| 30/08/2021 | http://www.genome.med.kyoto-u.ac.jp/SnpDB/; Relevant paper: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0100924#s4 | 
| Natri_2020/other_whole_blood | snpID.table | Narahara et al. 2014 | Japanese | Compressed eQTL summary statistics | 30/08/2021 | http://www.genome.med.kyoto-u.ac.jp/SnpDB/; Relevant paper: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0100924#s4 | 



# (3) Natri_2020_indo_blood.tsv columns 

> Information on column data on Indonesian population eQTL summary statistics (Natri et al. 2020), this is just Supp. file 3 from Natri et al. 2020. 

Columns: 
 
 1. target, 
 2. target chromosome
 3. target start
 4. N of tested SNPs
 5. SNP distance to the target
 6. rsID, 
 7. SNP position
 8. nominal p
 9. slope


