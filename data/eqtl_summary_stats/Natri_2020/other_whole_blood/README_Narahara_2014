**************************************************
Human Genetic Variation Browser --- eQTL map ---
Version: eQTLmap.v.8.0
Created: Tue Jul  1 13:42:23 JST 2014
Last Update: Tue Jul 01, 2014  02:20PM
Contact: Maiko Narahara < mnarahara@genome.med.kyoto-u.ac.jp >
**************************************************

-----------------------------------------------------------------------------
< Database tables >

1. SNP tables (snpID.table)
    -------------------------------------------------------------------------------------------
      ID | KID | chr | pos | rsID | A1 | A2 | GENO | MAF | F_MISS | HWE_P | ref | var | QCflag
    -------------------------------------------------------------------------------------------
        KID : KID
        chr : chromosome
        pos : position
        rsID: corresponding rs ID if exists
        A1  : allele 1 (minor allele)
        A2  : allele 2 (major allele)
        GENO: genotype frequencies (11/12/22)
        MAF : minor allele frequency
        F_MISS: Proportion of sample missing
        HWE_P: Hardy-Weinberg Equilibrium test p-value
        ref : reference allele
        var : variant allele
        QCflag: default QC (0: fail, 1: pass)
                pass: F_MISS <= 0.01 & MAF >= 0.05 & HWE_P >= 1e-7

2. Expression probe table (probeID.table)

        + Position information is based on probe annotation provided by Agilent eArray 
          (Design ID:028004 2012.04.11 release, hg19/GRCh37)
        + 42,405 records

    --------------------------------------------------------------------------------------
      ID | ProbeName | chr | start | stop | RefSeqAccession | Entrez.GeneID | Matchedbase 
    --------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------
    | RNAtype | Description | inProbeSNP_database | inProbeSNP_custom | inProbeSNP_hapmapJPT
    ------------------------------------------------------------------------------------------

        ProbeName     : probe names of Agilent SurePrint G3 Human Gene Expression 8x60K Microarray Kit ver1 kit
        chr           : chromosome
        start         : start position
        stop          : stop position
        RefSeqAccession: RefSeq accession
        Entrez.GeneID : Entrez gene IDs that overlapps probe position
        Matchedbase   : how many bases are matched on a reference
        RNAtype       : probes are labelled based on Agilent's annotation
                            RefSeq_NM_target probe ... probes annotated by RefSeq NM ID
                            lincRNA probe ... probes described as lincRNA by Agilent
                            Others ... any others not categorized above
        inProbeSNP_database : rs ID of SNPs located within expression probes
                              all SNPs available on the NCBI database excluding 
                              ones with multiple map locations and ones withdrawn
        inProbeSNP_custom : B37KID of SNPs located within expression probes
                            SNPs included in Ilumina Human Omni5Exome array 
                            (including only SNPs polymorphic in the FLU samples)
        inProbeSNP_hapmapJPT : rsID of SNPs located within expression probes
                               SNPs that are polymorphic in the HapMap JPT samples

3. Statistics table (local.SNP.all.results.tar.gz)
    This compressed file contains sva.corrected.pheno.*.cis.gz files

    All association tests between probes and SNP genotypes.

    ------------------------------------------------------------------------------------------------------------
      SNP | Expression | Condition | F_A | F_U | CHISQ | CHISQ.P | OR | Beta | SE | R2 | T.Wald | P.Wald | N | CisFlag
    ------------------------------------------------------------------------------------------------------------
        SNP        : ID in the SNP table of Build 37
        Expression : ID in the Expression probe table
        Condition  : all 13 (tested with the same condition, see below)
        F_A        : Not used
        F_P        : Not used
        CHISQ      : Not used
        CHISQ.P    : Not used
        OR         : Not used
        Beta       : PLINK --assoc output, regression coefficient
        SE         : PLINK --assoc output, standard error
        R2         : PLINK --assoc output, regression r-squared
        T.Wald     : PLINK --assoc output, Wald test (based on t-distribution)
        P.Wald     : PLINK --assoc output, Wald test asymptotic p-value
        BH_Q.cis   : Not used
        BH_Q.trans : Not used
        N          : Number of samples used for test
        CisFlag    : 0=distant SNP, 1=local SNP

Condition
    Sample: whole blood (peripheral blood cells) without stimulation
    Normalization: 75-percentile shift
    Covariates: gender, age, two surrogate variables
    Genotic model: additive
    (Refer to citation for details)


Citation
    Narahara, M., Higasa, K., Nakamura, S., Tabara, Y., Kawaguchi, T., Ishii, M., Yamada, R. (2014). Large-Scale East-Asian eQTL Mapping Reveals Novel Candidate Genes for LD Mapping and the Genomic Landscape of Transcriptional Effects of Sequence Variants. PLoS ONE, 9(6), e100924. doi:10.1371/journal.pone.0100924
    URL: http://dx.plos.org/10.1371/journal.pone.0100924
