

# ld_scores

Folder containing LD scores and related information, segregated by gnomAD population / grouping  (gnomad v2, hg19/GRCh37). These scores were all taken from this [wepage](https://gnomad.broadinstitute.org/downloads). gnomAD used [LDSC](https://github.com/bulik/ldsc) to compute LD scores for each gnomAD population grouping. 

Each subfolder name refers to the gnomAD population its files and LD scores were calculated in, and each subfolder contains the following files:

| File extension | Description |
| --- | --- | 
| .M_5_50 | The total number of SNPs in this dataset (population grouping) with a minor allele frequency > 0.05 |
| .M | The total number of SNPs in this dataset (population grouping) | 
| .ldscore.bgz | Raw, compressed LD score table taken directly from https://gnomad.broadinstitute.org/downloads. | 
| .ldscore file. | The uncompressed LD score table, Columns: CHR (Chromosome hg19/GRCh37), BP (Base Pair position hg19/GRCh37), ref (Reference allele), alt (Alternative allele), AC, AF (allele frequency), homozyote_count and L2 (LD score). The exact process of unzipping the .bgz file to form this file is listed below | 

For more information on these file types and how they are created see [here](https://github.com/bulik/ldsc/wiki/LD-Score-Estimation-Tutorial). 

**How did we unzip .ldscore.bgz to .ldscore**

Below uses the African American population files as an example: 

```
# rename.ldscore.bgz as .ldscore.gz - .bgz is not recognised by gunzip

cd gnomad.genomes.r2.1.1.afr.ld_scores.ldscore.bgz gnomad.genomes.r2.1.1.afr.ld_scores.ldscore.gz 

gunzip gnomad.genomes.r2.1.1.afr.ld_scores.ldscore.gz 

```
