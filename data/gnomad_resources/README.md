

# gnomAD resources

This folder contains resources taken from gnomAD, specifically from this [webpage](https://gnomad.broadinstitute.org/downloads). Importantly, these resources were taken from gnomAD v2, as gnomAD v2 resources were produced by mapping to the hg19/GRCh37 reference genome, the same reference genome utilised by our feature extraction pipeline. 


| Subfolder | Description | 
| --- | --- | 
| ld_scores | Folder containing LD scores and related information, segregated by gnomAD population / grouping. |
| constraint | Folder containing tables providing information / scores on how constrained particular genes or transcripts are calculated using gnomAD v2. This includes, for e.g. gene-wise LOEUF scores. |

For more information about the specific files in these subfolders (for example when they were downloaded) see the README.md files in their respective subfolders. 
