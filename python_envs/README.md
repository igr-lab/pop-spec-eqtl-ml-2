
## **Python_envs**

This folder contains conda environments - and tracks / stores the .yml files exported from these conda environments so they can be shared. These environments are designed to be run with the machine learning steps of the pipeline (which are also the python script steps of the pipeline / analysis). 

## .yml files:

.yml files can be used to create conda environments (instructions [here]( https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file)). This folder contains .yml files which can be used to create conda environments which match those used for the machine learning steps of the project:

**ml_pipeline_env.yml** : The most up-to-date .yml file export of the conda environment used with the machine learning pipeline.

**cba_ml_pipeline/ml_pipeline.yml**: Christina's original .yml file export of the conda environment used with the machine learning pipeline.