#!/usr/bin/env Rscript --vanilla

suppressPackageStartupMessages(require(optparse)) 

###############################################################################

# Outparser options

option_list = list(
  make_option(c("-b", "--beta_matrix"), type="character", default=NULL, 
              help="Matrix of betas", metavar="character"),
  make_option(c("-e", "--se_matrix"), type="character", default=NULL, 
              help="Matrix of Standard Errors", metavar="character"),
  make_option(c("-s", "--save_result"), type = "character", default=NULL, 
              help = "What to save the mashr results"),
  make_option(c("-o", "--output_dir"), type = "character", default=".",
              help = "What directory to save the produced figures, defaults to current directory"),
  make_option(c("-i", "--input_dir"), type = "character", default=".",
              help = "What directory to save the produced figures, defaults to current directory"),
  make_option(c("-p", "--percent"), type = "numeric", default=0.1,
              help = "What random percentage of tests to use to fit the mashr mixture proportions"),
  make_option(c("-l", "--lfsr_threshold"), type = "numeric", default=0.05,
              help = "What lfsr threshold to set for selecting strong tests to learn patterns of sharing"),
  make_option(c("-c", "--pca_components"), type = "numeric", default=NULL,
              help = "How many components in the PCA 
              (i.e. How many PCA components to use as data drive covariance matrices for mashr). 
              By default - uses the max possible number."),
  make_option(c("--sep"), type = "character", default=",",
              help = "sep for reading in beta and se matrices"),
  make_option(c("-a", "--all"), type = "logical", default=TRUE,
              help = "Should we perform posterior estimates on the entire dataset? Or just on a random sample?")
  );


opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

################################################################################


# Step 0: Set up + load data 

suppressMessages(library(dplyr))
library(mashr)
library(data.table)
library(tidyr)
library(tictoc)
library(flashr)


bhat = data.table::fread(paste0(opt$input_dir, "/", opt$beta_matrix), sep = opt$sep) %>% 
       tibble::column_to_rownames("gene_snp") %>% 
       as.matrix()

shat = data.table::fread(paste0(opt$input_dir, "/", opt$se_matrix), sep = opt$sep) %>% 
       tibble::column_to_rownames("gene_snp") %>% 
       as.matrix() 



# Step 0.1: Minor checks on the provided matrices

# Are the sizes of the two matrices the same?

if(dim(bhat) != dim(shat)){stop("The dimensions of the provided effect size and standard error matrices are not the same!!!")}

# Are the column names - and rownames the same for the matrices?

if(colnames(bhat) != colnames(shat)){stop("The column names are different between the provided effect size and standard error matrices")}



# Step 0.2: How many PCA components

# If the - use the maximum number of PCA components 

if(is.null(opt$pca_components)){
    
    opt$pca_components = ncol(bhat)
    
    }

message("shape of beta matrix")
print(head(bhat))

message("shape of se matrix")
print(head(shat))



################################################################################


# Step 1. Run mashr 1v1 on the entire dataset


# Full m.1by1 - no correction


mashr_data = mashr::mash_set_data(bhat,shat)

tic("Mashr full dataset running 1by1")

full_m.1by1 = mashr::mash_1by1(mashr_data) # Full dataset

toc()


################################################################################



# Step 2: 

# Identify a random subset of tests to get variance matrices

# Learn correlation structure among null tests using random test.


tic("Random subset selection")

set.seed(105)

random_rows = sample.int(nrow(bhat), size = opt$percent*nrow(bhat))

data.temp = mashr::mash_set_data(bhat[random_rows,],shat[random_rows,])

Vhat = estimate_null_correlation_simple(data.temp)

rm(data.temp)

data.random = mashr::mash_set_data(bhat[random_rows,], 
                                   shat[random_rows,], 
                                   V = Vhat)
toc()


################################################################################


# Step 3. 

# Learn data-driven covariance matrices using strong tests.

tic("Learn data-driven covariance matrices")

strong.subset = get_significant_results(full_m.1by1,
                                        opt$lfsr_threshold)


data.strong = mashr::mash_set_data(bhat[strong.subset,], 
                                   shat[strong.subset,], 
                                   V=Vhat)

# - don't include flashr matrices
# flashr is fun - but sometimes doesn't converge - which is a real pain in 
# an automated pipelne 
# U.f = cov_flash(data.strong,
#                 factors="nonneg",
#                 tag="non_neg",
#                 var_type="constant")

U.pca = mashr::cov_pca(data.strong, 
                       opt$pca_components)

U.ed = mashr::cov_ed(data.strong, U.pca)

toc()

################################################################################


# Step 4.

# Add canonical matrices - complete list of U matrices for mashr

tic("Finalise set of U matrices for mashr")

U.c = mashr::cov_canonical(data.random)

U = c(ED = U.ed, 
      Can = U.c)

toc()


################################################################################


# Step 5. 

# Fit the mashr model on a random subset of data


tic("Fit mash model (estimate mixture proportions)")

mash_model = mashr::mash(data.random, Ulist = U)

toc()


################################################################################


# Check pairwise sharing is following expected patterns:

print("Pairwise Sharing")

print(mashr::get_pairwise_sharing(mash_model))


################################################################################


# Step 6. Use the fitted model to estimate posterior matrices of the rest of the dataset

tic("Estimate Posterior Means rest of the dataset")


# extract matrices of the mashr input of the result of the gene snp pairs

bhat_rest = bhat[-random_rows,]
shat_rest = shat[-random_rows,]


# Split these gene-snp pairs into parts of ~100,000

id_vector = 1:nrow(bhat_rest)
id_vector_split = split(id_vector, ceiling(seq_along(id_vector)/100000))


# Create lists to contain results / computed posterior matrices 

mashr_posterior_mean = list()
mashr_posterior_sd = list()
mashr_posterior_lfsr = list()


mashr_posterior_mean[[1]] = mash_model$result$PosteriorMean
mashr_posterior_sd[[1]] = mash_model$result$PosteriorSD
mashr_posterior_lfsr[[1]] = mash_model$result$lfsr


#should paralellise this later
if(opt$all == TRUE){

for(i in 1:length(id_vector_split)){

  data = mashr::mash_set_data(bhat_rest[id_vector_split[[i]],],
                              shat_rest[id_vector_split[[i]],],
                              V = Vhat)

  mashr_posterior = mashr::mash_compute_posterior_matrices(data = data,
                                                           g=mash_model)
  
  mashr_posterior_mean[[i+1]] = mashr_posterior$PosteriorMean
  mashr_posterior_sd[[i+1]] = mashr_posterior$PosteriorSD
  mashr_posterior_lfsr[[i+1]] = mashr_posterior$lfsr
  

}
  
}

toc()

################################################################################


# Step 7. Combine results

# Save fitted model as '...mashr_model.rds'

# And estimated posterior matrices (entire dataset) as '...mashr_results.rds'

print("Combining results")

mashr_posterior_mean = Reduce('rbind', mashr_posterior_mean)
mashr_posterior_sd = Reduce('rbind', mashr_posterior_sd)
mashr_posterior_lfsr = Reduce('rbind', mashr_posterior_lfsr)

mashr_results = list(
                     PosteriorMean = mashr_posterior_mean,
                     PosteriorSD = mashr_posterior_sd,
                     lfsr = mashr_posterior_lfsr
)


print("Saving results")

saveRDS(mash_model, 
        paste0(opt$output_dir, "/", opt$save_result, "_model.rds"))

saveRDS(mashr_results, 
        paste0(opt$output_dir, "/", opt$save_result, "_results.rds"))



################################################################################
