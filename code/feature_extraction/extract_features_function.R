



################# Intention ##############################





# Create functions for extracting features

# from a data frame containing at least two columns of rsids (SNP IDs)and  ensembl id (GENE IDs)




## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 


#'
#'Extract features about both SNPs and genes, using a data frame with SNP and gene pairs 
#' 
#' 
#' @import rlang 0.4.11
#' @import tidyr 1.1.3
#' @import dplyr 1.0.7
#' @import furrr 0.2.3
#' @import future 1.21.0
#' @import GenomicDistributions 1.1.9
#' @import box 1.0.2
#' 
#' @param data Data frame with each row as a SNP / Gene pair
#' @param SNP_column The name of the column of SNP ids / rsids
#' @param GENE_column The name of the column of Gene ids / ensembl ids
#' @param ref_column The name of the column containing the reference allele
#' @param alt_column The name of the column containing the alternative allele 
#'
#' @param snp_features A character vector of features to extract / obtain about SNPs
#' @param gene_features A character vector of features to extract / obtain about Genes / ensembl_ids
#' @param gnomAD_maf_pop A character vector of populations to extract gnomAD minor allele frequency data from
#' @param thous_genome_pop  A character vector of populations to extract 1000 genomes minor allele frequency data from
#' @param window_size A numeric vector of in base pairs, the size of window around the SNP to calculate relevant 
#' features for (e.g. percentage GC). Can be multiple windows, e.g. \code{window_size = c(5, 10, 25, 50)}
#' 
#' @param combined_features A character vector of 
#' features requiring both SNP and Gene information,
#' for e.g. distance between SNP and Gene Transcription Start Site). Please set = NULL. 
#'     
#'@export
extract_features <- function(data,
                             SNP_column,
                             GENE_column,
                             ref_column = NULL,
                             alt_column = NULL,
                             snp_features = c("CADD",  # What features should be extracted / obtained
                                              "linsight", 
                                              "fitCons", 
                                              "gnomAD_maf",
                                              "1000k_genomes_maf",
                                              "genomicdistributions_location",
                                              "ld_score_cross_pop_measures",
                                              "percent_di_nucleotide_win",
                                              "dis_to_TSS", 
                                              "percent_gc_win"),
                             gene_features = c("gene_biotype", 
                                               "gene_duplication_status", 
                                               "percentage_gene_gc_content", 
                                               "kegg_enzyme",
                                               "gnomAD_loeuf",
                                               "gene_length", 
                                               "gnomAD_cds_length",
                                               "oe_syn_upper",
                                               "oe_syn_lower",
                                               "oe_syn",
                                               "oe_mis_upper",
                                               "oe_mis_lower",
                                               "oe_mis",
                                               "oe_mis_pphen",
                                               "oe_lof",
                                               "classic_caf",
                                               "classic_caf_afr",
                                               "classic_caf_amr",
                                               "classic_caf_nfe",
                                               "classic_caf_eas",
                                               "classic_caf_sas",
                                               "p",
                                               "p_afr",
                                               "p_amr",
                                               "p_nfe",
                                               "p_sas",
                                               "pLI",
                                               "pRec",
                                               "pNull",
                                               "di_nucl_percent",
                                               "interpro_terms", 
                                               "% amino acid", 
                                               "% Di-peptide", 
                                               "goslim_goa_accession",
                                               "gtex_tissue_breadth",
                                               "gtex_tissue_breadth_no_brain",
                                               "gtex_expression_measures", 
                                               "immport_annotation",
                                               "innatedb_annotation",
                                               "phastCons",
                                               "phyloP"),
                             gtex_tissue = NULL,
                             go_grouping = "immune",
                             gtex_tissue_exp_pca_components = NULL,
                             gtex_tissue_exp_flash_factors = NULL,
                             encode_dhs_tissue = NULL,
                             transcript_selection_tissue = NULL, #for protein features
                             tissue_feat_type = c("mean", "var"),
                             gnomAD_maf_pop = c("AF"), # default - global AF
                             # "AF_afr", #African American
                             # "AF_amr", # Latino Ancestry
                             # "AF_nfe_est", #Estonian ancestry
                             # "AF_nfe_nwe", #North western europe ancestry
                             # "AF_eas", #East asian 
                             # "AF_fin", #finnish
                             # "AF_popmax", # (Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry
                             # "AF_nfe"), # Non finish european
                             thous_genome_pop = c("AF"),
                             # "EUR_AF", # European population
                             # "AFR_AF", # African population
                             # "AMR_AF", # American population
                             # "EAS_AF", # East Asian
                             # "SAS_AF") # South Asian
                            combined_features = c("dist_esnp_egene"), 
                            window_size = 50){#size of window   
  

  
########## Set up - load simple required packages ################
  
  renv::load(here::here())
  
  options(box.path = here::here())
  
  box::use(dplyr[...],
           rlang[...])
  
  # Set up timing - measure how long things take
  tictoc::tic.clearlog()
  
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
  
  
##### Set up data frame to add features to - and get gene and snp columns ######
  
  
  data_bare_bone_columns = data %>% dplyr::select({{GENE_column}}, 
                                                  {{SNP_column}}) %>% names()
  

  
  df = data %>% 
       dplyr::transmute(
                      SNP_column = {{ SNP_column }},
                      GENE_column = {{ GENE_column }}
                                                     ) %>% 
        dplyr::arrange(GENE_column, SNP_column)
  
  

  SNP_vector = unique(df$SNP_column) #granges really doesn't like duplicates of the same sequence 
  
  GENE_vector = unique(df$GENE_column)
  
  
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
  
  
  
  
########### Extract SNP features #######################
  
  
  # Features you can extract from SNPs
  
  box::use(./id_to_genomic_ranges)
  

  if(any(snp_features %in% c("CADD",
                         "linsight",
                         "fitCons",
                         "gnomAD_maf",
                         "dis_to_TSS",
                         "percent_gc_win",
                         "transcript_selection_tissue", 
                         "genomicdistributions_location",
                         "ld_score_cross_pop_measures",
                         "1000k_genomes_maf"))){
    
    
    tictoc::toc(log = TRUE, quiet = TRUE)
    tictoc::tic("SNP feature extraction")
    
    ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##   
    
    
    message("\n Now ... extracting features from SNP ids")
    
    ref_alt_metadata = data %>% 
                       dplyr::transmute(ref = {{ref_column}},
                                        alt = {{alt_column}},
                                        refsnp_id = {{SNP_column}}) %>% 
                       dplyr::distinct() 
    
    SNP_vector = unique(ref_alt_metadata$refsnp_id) 
     
    # biomaRt package steps
    # Convert SNP ids to genomic ranges objects 
    snp_gr_list = id_to_genomic_ranges$id_to_granges(id_vector = SNP_vector,
                                                     type = "SNP",
                                                     meta_data_columns = ref_alt_metadata, 
                                                     return_mart = TRUE)

    
    snp_gr = snp_gr_list[[1]]
    
    
    ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
    
    
    
    #Set up / create dataframe for which features will be added to 
    
    snp_features_df <- snp_gr_list[[2]] 
    
    # load function to extract information about snps from rsids
    
    box::use(./extract_from_rsid) 
    
    
    ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
    
    snp_features_df = extract_from_rsid$extract_from_rsid(gr = snp_gr, 
                                                          snp_features_df = snp_features_df,
                                                          gnomAD_maf_pop  = gnomAD_maf_pop,
                                                          thous_genome_pop = thous_genome_pop,
                                                          features = snp_features,
                                                          window_size = window_size,
                                                          encode_dhs_tissue = encode_dhs_tissue) # bp size of window around snp for features that require it
  
    
   
    ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
    
    # Renaming snp columns to match the original dataset
  
   snp_feature_df = snp_features_df %>% 
    dplyr::rename(!!quo_name(enquo(SNP_column)) := refsnp_id)
 
  }
  
  
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
  
  
  
  
  
######### Extract gene features ##################  

  if(
      any(
         gene_features %in% c("gene_biotype", 
                             "percentage_gene_gc_content", 
                             #"transcript_length",
                             #"cds_length",
                             "go_id",
                             "kegg_enzyme",
                             "goslim_goa_accession")
                                                     )
                                                      ){
    
    biomart_gene_features = gene_features[gene_features %in% c("gene_biotype", 
                                                               "percentage_gene_gc_content", 
                                                              # "transcript_length",
                                                              # "cds_length",
                                                               "go_id",
                                                              "kegg_enzyme",
                                                               "goslim_goa_accession")]
    
  }
  
  else{
    
    biomart_gene_features = NULL
    
      }
  
  
  tictoc::toc(log = TRUE, quiet = TRUE)
  tictoc::tic("Gene feature extraction")
  
  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
  
  
  gene_gr_list = id_to_genomic_ranges$id_to_granges(id_vector = GENE_vector,
                                                    type = "Gene",
                                                    return_mart = TRUE,
                                                    go_grouping = go_grouping,
                                                    biomart_features = biomart_gene_features)
  
  
  gene_gr = gene_gr_list[[1]]
  
  gene_features_df = gene_gr_list[[2]]
  
  
  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
  
  if(any(gene_features %in% c("gene_biotype", 
                              "gene_duplication_status", 
                              "percentage_gene_gc_content", 
                              "gene_length", 
                              "kegg_enzyme", 
                            # "transcript_length",
                            # "cds_length",
                            # "go_id",
                            "di_nucl_percent",
                            "gnomAD_cds_length",
                            "gnomAD_loeuf",
                            "oe_syn_upper",
                            "oe_syn_lower",
                            "oe_syn",
                            "oe_mis_upper",
                            "oe_mis_upper",
                            "oe_mis",
                            "oe_mis_pphen",
                            "oe_lof",
                            "classic_caf",
                            "classic_caf_afr",
                            "classic_caf_amr",
                            "classic_caf_nfe",
                            "classic_caf_eas",
                            "classic_caf_sas",
                            "p",
                            "p_afr",
                            "p_amr",
                            "p_nfe",
                            "p_sas",
                            "pLI",
                            "pRec",
                            "pNull",
                            "interpro_annotation", 
                            "% amino acid",
                            "goslim_goa_accession",
                            "phastCons",
                            "phyloP",
                            "immport_annotation",
                            "innatedb_annotation",
                            "gtex_tissue_breadth_no_brain",
                            "gtex_tissue_breadth"))|
                             is.numeric(gtex_tissue_exp_flash_factors)|
                             is.numeric(gtex_tissue_exp_pca_components)|
                             !is.null(transcript_selection_tissue)|
                             !is.null(gtex_tissue)){
    
    
  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
  
      
  # Features you can extract from Genes
  message("\n Now ... extracting features from ensembl gene ids")
  
      
  box::use(./extract_from_ensembl)  
  
  gene_features_df = extract_from_ensembl$extract_from_ensembl_id(gr = gene_gr,
                                                                  gene_features_df = gene_features_df,
                                                                  features = gene_features,
                                                                  gtex_tissue = gtex_tissue,
                                                                  tissue_feat_type = tissue_feat_type,
                                                                  transcript_selection_tissue = transcript_selection_tissue, 
                                                                  gtex_tissue_exp_pca_components = gtex_tissue_exp_pca_components,
                                                                  gtex_tissue_exp_flash_factors = gtex_tissue_exp_flash_factors)
  

  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
  
  
  # Renaming gene and snp columns to match the original dataset
  
  gene_feature_df = gene_features_df %>% 
    dplyr::rename(!!quo_name(enquo(GENE_column)) := ensembl_gene_id)
  
  
  
  }

  
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
  
  
  
########## Combined features ########################  

 if(!is.null(combined_features)) {
   
   tictoc::toc(log = TRUE, quiet = TRUE)
   tictoc::tic("Combined feature extraction")
   
  if (combined_features %in% "dist_esnp_egene"){
  
    
    message("...Distance from SNP to Gene")
    
    
    ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
    
    
    
    # distance from snp to gene in eQTL pair
    
    gene_gr_to_df = data.frame(gene_start = GenomicRanges::start(gene_gr),
                               gene_end = GenomicRanges::end(gene_gr), 
                               ensembl_gene_id = GenomicRanges::mcols(gene_gr)$ensembl_gene_id)
    
    snp_gr_to_df = data.frame(snp_location = GenomicRanges::start(snp_gr),
                              refsnp_id = GenomicRanges::mcols(snp_gr)$refsnp_id)
    
    df_snp_gene_pair = data.frame(refsnp_id = df$SNP_column,
                                  ensembl_gene_id = df$GENE_column)
    
    add_snp_gene_pair = dplyr::left_join(df_snp_gene_pair,
                                         snp_gr_to_df)
    
    snp_gene_pair_all = dplyr::left_join(add_snp_gene_pair,
                                         gene_gr_to_df)
    
    combined_features_df = snp_gene_pair_all %>% 
      dplyr::rowwise() %>% 
      dplyr::mutate(dist_snp_gene = min(
        abs(gene_end - snp_location),
        abs(gene_start - snp_location))
      ) %>% 
      dplyr::select(refsnp_id,
                    ensembl_gene_id,
                    dist_snp_gene)
  
    
  }
   
   
   ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
   
   
   
   # Renaming gene and snp columns to match the original dataset
   combined_features_df = combined_features_df %>% 
     dplyr::rename(!!quo_name(enquo(SNP_column)) := refsnp_id, 
                   !!quo_name(enquo(GENE_column)) := ensembl_gene_id)
   
  
  }   
  

  
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
  
  
  
  tictoc::toc(log = TRUE, quiet = TRUE)
  message("\n Time taken for each feature extraction main step:")
  message(paste0(tictoc::tic.log(format = TRUE), sep = "\n"))
  tictoc::tic.clearlog()

    
  
##### Add extracted features to input data frame - return this frame ######          
  
  
  
  
if(exists("gene_feature_df")&exists("snp_feature_df")&exists("combined_features_df")){
  
 
  data = data %>% 
    dplyr::select(
                  -dplyr::any_of(
                                  names(snp_feature_df)[!(names(snp_feature_df) %in% data_bare_bone_columns)]
                                                                                                              ),
                  -dplyr::any_of(
                                 names(gene_feature_df)[!(names(gene_feature_df) %in% data_bare_bone_columns)]
                                                                                                                                         ),
                  -dplyr::any_of(
                                  names(combined_features_df)[!(names(combined_features_df) %in% data_bare_bone_columns)]
                                                                                                                                                        )
                 )
  
  
  return(Reduce("full_join", 
                list(data, 
                     snp_feature_df, 
                     gene_feature_df, 
                     combined_features_df
                ))
  ) 
}
  
else if(exists("gene_feature_df")&exists("snp_feature_df")){
  
  data = data %>% 
    dplyr::select(
      -dplyr::any_of(
        names(snp_feature_df)[!(names(snp_feature_df) %in% data_bare_bone_columns)]
      ),
      -dplyr::any_of(
        names(gene_feature_df)[!(names(gene_feature_df) %in% data_bare_bone_columns)]
      )
      )
  
  
  
  return(Reduce("full_join", 
                list(data, 
                     snp_feature_df, 
                     gene_feature_df
                ))
  ) 
}  

else if(exists("gene_feature_df")){
  
  data = data %>% 
    dplyr::select(
                 -dplyr::any_of(names(gene_feature_df)[!(names(gene_feature_df) %in% data_bare_bone_columns)]
                                                                                                                                       )
                 )
  
  return(dplyr::left_join(data,
                   gene_feature_df))

  
}
  
else if(exists("snp_feature_df")){
  
  data = data %>% 
         dplyr::select(
                   -dplyr::any_of(
                                  names(snp_feature_df)[!(names(snp_feature_df) %in% data_bare_bone_columns)]
                                                                                                              )
                                                                                                               )
  
  return(Reduce("full_join", 
                list(data, 
                     snp_feature_df
                ))
  ) 
}
  


 
}





