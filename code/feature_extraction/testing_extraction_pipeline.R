
options(box.path = ".")

box::use(code/feature_extraction)

library(dplyr)

shang_df = data.table::fread("data/eqtl_summary_stats/Shang_2020/combined/combined_shang_summary.csv")

# test timing for sample size of eQTLs of 20,000
test_df = shang_df %>% sample_n(25)

library(tictoc)
# for 20000 eQTLs
tic()
shang_test = feature_extraction$extract_features(test_df,
                                                 snp,
                                                 gene,
                             snp_features = c("CADD", # What features should be extracted / obtained
                                              "linsight", 
                                              "fitCons", 
                                              "gnomAD_maf",
                                              "1000k_genomes_maf",
                                              "genomicdistributions_location",
                                              "win_di_nucl_content",
                                              "dis_to_TSS", 
                                              "win_GC_content"),
                             gene_features = c("gene_biotype", 
                                               "percentage_gene_gc_content", 
                                               "kegg_enzyme",
                                               "gnomAD_loeuf",
                                               "gnomAD_cds_length",
                                               "oe_syn_upper",
                                               "oe_syn_lower",
                                               "oe_syn",
                                               "oe_mis_upper",
                                               "oe_mis_upper",
                                               "di_nucl_percent",
                                               "oe_mis",
                                               "oe_mis_pphen", 
                                               "goslim_goa_accession",
                                               "phastCons",
                                               "phyloP"),
                             gtex_tissue = "lcl",
                             gtex_tissue_exp_pca_components = 1:10,
                             gtex_tissue_exp_flash_factors = 1:10,
                             encode_dhs_tissue = "lcl",
                             gnomAD_maf_pop = c("AF", "AF_afr", "AF_nfe", "AF_popmax"), # default - global AF
                             thous_genome_pop = c("AF", "EUR_AF", "AFR_AF"),
                             combined_features = c("dist_esnp_egene"), 
                             window_size = c(10,50))
toc()



natri_features_df = feature_extraction$extract_features(natri_df,
                                    snp,
                                    gene,
                                    snp_features = c("CADD", # What features should be extracted / obtained
                                                     "linsight", 
                                                     "fitCons", 
                                                     "gnomAD_maf",
                                                     "1000k_genomes_maf",
                                                     "genomicdistributions_location",
                                                     "win_di_nucl_content",
                                                     "dis_to_TSS", 
                                                     "win_GC_content"),
                                    gene_features = c("gene_biotype", 
                                                      "percentage_gene_gc_content", 
                                                      "kegg_enzyme",
                                                      "gnomAD_loeuf",
                                                      "gnomAD_cds_length",
                                                      "oe_syn_upper",
                                                      "oe_syn_lower",
                                                      "oe_syn",
                                                      "oe_mis_upper",
                                                      "oe_mis_upper",
                                                      "di_nucl_percent",
                                                      "oe_mis",
                                                      "oe_mis_pphen", 
                                                      "goslim_goa_accession",
                                                      "phastCons",
                                                      "phyloP"),
                                    gtex_tissue = "wb",
                                    gtex_tissue_exp_pca_components = 1:20,
                                    gtex_tissue_exp_flash_factors = 1:20,
                                    encode_dhs_tissue = "wb",
                                    gnomAD_maf_pop = c("AF", "AF_nfe", "AF_popmax"), # default - global AF
                                    thous_genome_pop = c("AF", "EUR_AF"),
                                    combined_features = c("dist_esnp_egene"), 
                                    window_size = c(10,50, 250, 1000))
