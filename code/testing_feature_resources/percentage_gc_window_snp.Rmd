---
title: "% GC Content Around SNP (to determine what window size to pick)"
author: "Isobel Beasley"
date: "24/06/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message=FALSE,warning = FALSE)
```

## Set up

```{r}
# requires box package 

box::use(dplyr[...],
         ggpubr[...],
         tidyr[...],
         data.table[...],
         magrittr[...],
         ggplot2[...],
         dtplyr[...],
         utils[...],
         biomaRt[...],
         patchwork[...])


setwd("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/scripts")

# Load functions from feature_extraction subfolder using box
options(box.path = setwd("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/scripts"))  

# Functions to extract %GC from genomic ranges object of snps
box::use(feature_extraction/ID_to_GenomicRanges)

# Functions to convert rsids to 
box::use(feature_extraction/extract_features_function)

# Set up custom theme for ggplots
custom_theme <-
  list(
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        text = element_text(size = 9),
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
      )
  )


```

## Extract a random sample of rsids from Natri et al. 2020

```{r}

# Load mashr results from a random sample of 100,000 gene snp pairs

mashr_canoncovs_datacovs <- readRDS("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/data/Indo_testing_data/mashr_canoncovs_datacovs.rds")

# Create a vector of rsids / snps ids
snp_vector = mashr_canoncovs_datacovs$result$PosteriorMean %>% 
             as.data.frame() %>% 
             tibble::rownames_to_column(var = "gene_snp") %>% 
             tidyr::separate(col = "gene_snp", into = c("gene", "snp")) %$% 
              snp 

# Keep only the distinct snp ids
snp_vector = base::unique(snp_vector)

# Take a random sample of 10^4 snps - for which to calculate %GC in a window around
random_sample_snps = sample(snp_vector, 
                            size = 10000)


```

### Calculate percentage GC for many random snps, across a wide range of windows

```{r results='hide'}


# Convert snp ids to a genomic ranges object
gr = ID_to_GenomicRanges$id_to_granges(random_sample_snps, type = "SNP")

# What windows (in bp) should we test
win_size = seq(from = 5, 
                        to = 250, 
                                    by = 5)
# Calculate %GC for a window around each snp 
#(this produces a list of dfs, each row being a different snp
# and each df being produced with different window sizes)
g = lapply(win_size, 
           function(y) {extract_features_function$extract_from_rsid(
                                                                    gr[[1]], 
                                                                    gr[[2]], 
                                                                    features = "win_GC_content", 
                                                                      window_size = y) 
                                                                                       } 
)
# Combine the df into a single df of many columns (other than the snp id col,
# each col is a %GC from a different window size around the snp),
# making sure to label each column so the window size is listed in the column 
for(i in 1:length(win_size)) {
  
  g[[i]] = rename_with(g[[i]],
                       .fn = ~ (gsub("GC", win_size[i], .x, fixed = TRUE)),
                       .cols = starts_with("win"))
  
} 


```

# Plot how %GC calculation varies as this window increases

```{r}

# Create ploting function to plot a random sample of these snps (50 per plot)
# y axis being percentage %GC and x-axis window size
plot_n_sample = function(n){
    Reduce("inner_join", g) %>% 
  tidyr::pivot_longer(!refsnp_id, 
                      names_to = 
                        "win_GC", 
                      values_to = "percent") %>% 
  tidyr::separate(col = win_GC,
                  sep = "_" ,
                  into = c("A", "win_size")) %>% 
  tidyr::nest(!refsnp_id) %>% 
  sample_n(n) %>% 
  tidyr::unnest() %>% 
  rowwise() %>% 
  mutate(percent = as.numeric(percent)) %>% 
  mutate(win_size = as.numeric(win_size)) %>% 
  ggplot(aes(x=win_size, 
             y=percent, 
             group = refsnp_id, 
             col = refsnp_id)) + 
  geom_line() + 
  custom_theme + 
  theme(legend.position = "none")
} 

# Plot 6 of window size vs %GC plots

(plot_n_sample(25) | plot_n_sample(25) | plot_n_sample(25)) / 
(plot_n_sample(25) | plot_n_sample(25) | plot_n_sample(25))


(plot_n_sample(25) + xlim(0, 50) | plot_n_sample(25) + xlim(0, 50) | plot_n_sample(25) + xlim(0, 50)) / 
(plot_n_sample(25) + xlim(0, 50) | plot_n_sample(25) + xlim(0,50) | plot_n_sample(25) + xlim(0,50))



```

## What is the correlation between %GC at 250 bp and ... 

Basically, we want a high correlation from as small as window size as possible. 

```{r}

data_gc =  Reduce("inner_join", g)

#  What is the correlation between %GC at 250 bp and % GC at... 
cors = c(
cor(data_gc$win_5, data_gc$win_250), # 5 bp
cor(data_gc$win_15, data_gc$win_250), # 15 bp
cor(data_gc$win_25, data_gc$win_250), # 25 bp
cor(data_gc$win_35, data_gc$win_250), # 35 bp
cor(data_gc$win_40, data_gc$win_250), # 40 bp
cor(data_gc$win_45, data_gc$win_250), # 45 bp
cor(data_gc$win_50, data_gc$win_250), # 50 bp
cor(data_gc$win_75, data_gc$win_250), # 75 bp
cor(data_gc$win_100, data_gc$win_250)) # 100 bp

win_size = c(5,15,25,35,40,45, 50,75,100)

data.frame(win_size, cors) %>% 
  ggplot(aes(x=win_size, y= cors)) + 
  geom_line() + 
  custom_theme + 
  labs(x = "Window size around SNP",
       y = "Correlation between %GC in 250 bp window around snp, and %GC at this window size")
```

## Conclusions: 

(1) At this stage, I will use a window size of 50 bp (%GC at this window size has a relatively high correlation > 0.7 with %GC at 250 bp, but is still a relatively small window)
## Session Info

```{r}

sessionInfo()

```