
################################################################################

# Getting opposite effect eQTLs for Irene

library(magrittr)

################################################################################

# Natri 2020

natri_mashr_results = readRDS("output/mashr_results/natri_mashr_results.rds")


# which eQTLs 
sig_res_natri = natri_mashr_results$lfsr %>% 
  as.data.frame() %>% 
  filter(across(everything(), ~.x<0.05)) %>% 
  rownames()

# natri_mashr_results$lfsr %>% 
#   as.data.frame() %>% 
#   filter(indo < 0.05 & twinsuk < 0.05 & lepik < 0.05 & gtex < 0.05) %>% 
#   dim()

sig_res_all = natri_mashr_results$lfsr %>% 
              as.data.frame() %>% 
              filter(across(everything(), ~.x<0.05)) %>% 
              tibble::rownames_to_column("gene_snp") %$%
              gene_snp

ops_eqtls = natri_mashr_results$PosteriorMean[sig_res_natri, ] %>% 
  as.data.frame() %>% 
  mutate(is_opps = ifelse(sign(indo) != sign(gtex) &
                          sign(indo) != sign(lepik) &
                          sign(indo) != sign(twinsuk),
                          TRUE,
                          FALSE)) %>% 
  filter(is_opps == TRUE) 

opps_eff_indo_eqtl_vector = rownames(ops_eqtls)

save(opps_eff_indo_eqtl_vector, file = "opps_effect_indo_euro_eqtl.Rdata")

################################################################################

mogil_mashr_results = readRDS("output/mashr_results/mogil_only_mashr_results.rds")

sig_res = mogil_mashr_results$lfsr %>% 
  as.data.frame() %>% 
  filter(across(everything(), ~.x<0.05)) %>% 
  rownames()


ops_eqtls = mogil_mashr_results$PosteriorMean[sig_res, ] %>% 
  as.data.frame() %>% 
  mutate(is_opps_effect = ifelse(sign(ea_beta) == sign(aa_beta) & sign(aa_beta) == sign(his_beta),
                          FALSE,
                          TRUE)) %>% 
  group_by(is_opps_effect) %>% 
  summarise(n = n(),
            mean_abs_african_american = mean(abs(aa_beta)), 
            mean_abs_hispanic = mean(abs(his_beta)), 
            mean_abs_european_american = mean(abs(ea_beta)))

ops_eqtls_2 = mogil_mashr_results$PosteriorMean[sig_res, ] %>% 
  as.data.frame() %>% 
  mutate(is_opps = ifelse(sign(ea_beta) == sign(aa_beta) & sign(aa_beta) == sign(his_beta),
                          FALSE,
                          TRUE)) %>% 
  filter(is_opps == TRUE) 

ops_eqtls %>% 
  mutate(opps_effect_population = ifelse(sign(ea_beta) != sign(aa_beta) & sign(aa_beta) == sign(his_beta), "euro",
                            ifelse(sign(aa_beta) != sign(ea_beta) & sign(ea_beta) == sign(his_beta), "african-american", "hispanic"  ))) %>% 
  group_by(opps_effect_population) %>% 
  filter(is_opps == TRUE) %>% 
  summarise(n = n(), 
            mean_abs_african_american = mean(abs(aa_beta)), 
            mean_abs_hispanic = mean(abs(his_beta)), 
            mean_abs_european_american = mean(abs(ea_beta)))

ops_eqtls = ops_eqtls %>% 
  mutate(opps_effect_population = ifelse(sign(ea_beta) != sign(aa_beta) & sign(aa_beta) == sign(his_beta), "european",
                            ifelse(sign(aa_beta) != sign(ea_beta) & sign(ea_beta) == sign(his_beta), "african-american", "hispanic"  ))) %>% 
  select(opps_effect_population) %>% 
  tibble::rownames_to_column("ensembl_rsid") 

ops_eqtls %>% 
  filter(opps_effect_population == "european")


#%>% 
#  data.table::fwrite(. , "mogil_opposite_effect_eqtls.csv")

ops_eqtls 

sig_res_some = natri_mashr_results$lfsr %>% 
  as.data.frame() %>% 
  tibble::rownames_to_column("gene_snp")  %>% 
  filter(indo<0.05) %>% 
  rowwise() %>% 
  mutate(Class = ifelse(gtex<0.1|lepik<0.1|twinsuk<0.1, "Shared", "Indo-Specific"))


sig_res_some %>% 
  group_by(Class) %>% 
  summarise(n  = n())

natri_df = sig_res_some %>% 
           select(-indo, -gtex, -twinsuk, -lepik) %>% 
          tidyr::separate("gene_snp", into = c("gene", "snp"), sep = "_") 
  
  
  
