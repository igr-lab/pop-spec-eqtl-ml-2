
# Code subfolders:

| Subfolder | Description |
| --- | --- |
|**calling_pop_spec_eqtl**| Scripts to take combined eQTL summary statistics data (especially after mashr is applied) and call each eQTL population specifc or shared|
| **combine_summary_stats** | Scripts to combine summary eQTL data from multiple populations into a single data.frame (unfortunately, given different eQTL summary statistics have their own set of problems / ex  these are currently at least on script per dataset with multiple populations) |
| **feature_extraction** | Scripts with functions that take ensembl ids and rsids to extract features for the ML to train on (these functions are dataset agonistic, you just need to give them a data.frame and tell them which column contains the ensembl ids and/or the rsids) |
| **illumina_array_testing** | Scripts for testing illumina array probes and their location, and whether this matches with what gene ids they were assigned | 
| **mashr** | Scripts for applying mashr to combined summary statistics |
|**ML-Pipeline**| Scripts for machine learning pipeline taken from [https://github.com/azodichr/ML-Pipeline](https://github.com/azodichr/ML-Pipeline) |
| **presentation_and_poster_figures** | Scripts used to create figures for various presentations, posters (incl. conference presentations)  
| **testing_population_specific_methods** | Scripts for messing around with and testing different software / measures for declaring an eQTL population specific |
| **testing_feature_resources** | Scripts for messing around with different resources and software for extracting features |
| **updated_ml_pipeline** | Scripts for the updated ml pipeline (current updated versions), including scripts for plotting results |
| **utils** | Scripts containing utility functions. Includes workflowr wrapper functions, slurmR Rmarkdown submission scripts, and plotting functions.| 
