# Plotting result functions


#' Group machine learning features by type (E.g. Gene ontology, summary statistics, allele frequency etc. )
#' 
#' @param feature_name A vector of strings denoting the feature name 
#' (e.g. a column name of a feature in a data.frame used to train a machine learning model)
#' 
#' @param dont_group A string to grep against the feature name, any features you don't want grouped
#' 
#' @param group_other Logical, should features which don't fit under defined groups be grouped together under
#' "other"?
#' 
#' @export
group_features = function(feature_name, 
                          dont_group = FALSE,
                          group_other = TRUE) {
  
  feature_group = dplyr::case_when(
                   grepl(dont_group, feature_name) ~ feature_name,
                   
                   grepl("GO|go|kegg|Kegg|IPR", feature_name) ~ "GOSlim, Kegg pathway, or Interpro term",
                   
                   grepl("_beta|_se", feature_name) ~ "eQTL summary statistics",
                   
                   grepl("LDScore|LD score|LD Score", feature_name) ~ "LD Score (gnomAD cross-population metrics)",
                   
                   ("dist_snp_gene" != feature_name & ( 
                                                        "phastcons"== feature_name | 
                                                        "phylop" == feature_name | 
                                                         grepl("oe|classic|p_|gene_conservation|Gene Conservation", 
                                                               feature_name) | 
                                                         feature_name %in% c("p","pLI", "pRec","pNull")
                                                        )
                   ) ~ "Gene Conservation or Constraint Metrics",
                   
                   grepl("fitCons|cadd|linsight|SNP_conservation|SNP Conservation",
                         feature_name) ~ "SNP Conservation / Consequence",
                   
                   grepl("maf|AF|Allele Frequency", feature_name) ~ "Allele Frequency",
                   
                   grepl("PC|factor|tpm|brain|non_zero|expression|Gene Expression",
                         feature_name) ~ "Gene Expression Metrics",
                   
                   (grepl("SNP_percent|%_Nucleotide_SNP", feature_name)|
                      "% Nucleotide (SNP)" == feature_name) ~ "% Nucleotide (SNP)",
                   
                   (
                     nchar(feature_name)==1|
                       grepl("amino_acid|Amino|amino",feature_name)
                   ) ~ "% Amino acid",
                   
                   (
                     (grepl("gene_percent",
                            feature_name) & 
                        !grepl("biotype", feature_name)) | 
                       "% Nucleotide (Gene)" == feature_name
                   ) ~ "% Nucleotide (Gene)",
                   
                   group_other ~ "Other",
                   
                   TRUE ~ feature_name
                   
                     
  )
  
  return(feature_group)
}


# if(grepl("GO|go|kegg|Kegg|IPR", feature_name)){
#   
#   return("GOSlim, Kegg pathway, or Interpro term")
#   
# }
# 
# else if(grepl("_beta|_se", feature_name)){
#   
#   return("eQTL summary statistics")
# }
# 
# 
# else if(grepl("LDScore|LD score|LD Score", feature_name)){
#     
#     return("LD Score (gnomAD cross-population metrics)")
#   }
#   
#   
# else if("dist_snp_gene" != feature_name &(
#         "phastcons"== feature_name | 
#         "phylop" == feature_name | 
#         grepl("oe|classic|p_|gene_conservation|Gene Conservation", feature_name) | 
#         feature_name %in% c("p","pLI", "pRec","pNull"))){
#   
#   return("Gene Conservation or Constraint Metrics")
# }
# 
# else if(grepl("fitCons|cadd|linsight|SNP_conservation|SNP Conservation",
#               feature_name)){
# 
#   return("SNP Conservation / Consequence")
# }
# 
# else if(grepl("af|AF|Allele Frequency", feature_name)){
#   
#   return("Allele Frequency")
# }
# 
# else if(grepl("PC|factor|tpm|brain|non_zero|expression|Gene Expression",
#               feature_name)){
#   
#   return("Gene Expression Metrics")
# }
# 
# else if(grepl("SNP_percent|%_Nucleotide_SNP", feature_name)|
#         "% Nucleotide (SNP)" == feature_name){
#   
#   return("% Nucleotide (SNP)")
# }
#   
# else if(
#     nchar(feature_name)==1|
#     grepl("amino_acid|Amino",feature_name)
#   ){
#     
#     return("% Amino acid")
# }
# 
# else if(
#         (grepl("gene_percent",
#               feature_name) & 
#         !grepl("biotype", feature_name)) | 
#         "% Nucleotide (Gene)" == feature_name
#         ){
#   
#   return("% Nucleotide (Gene)")
# }

# else if(grepl("kegg", feature_name)){
#   
#   return("GOSlim or Kegg pathway term")
# }
# 
#   
# else{
#   return("Other")
# }
# 
# }

#' @export
feature_level = function(feature_name){
  
feat_level = dplyr::case_when(
                   (grepl(
                        paste(c("GO","go","kegg","classic",
                                "oe","pLI","pRec","pNull",
                                "phast","phylo","PC","factor","tpm",
                                "brain","immport","innate", 
                                "cds","non_zero","immune",
                                "%_Nucleotide_Gene", "expression_metrics",
                                "Gene", "gene_percent", "duplication", "duplicated",
                                "biotype"),
                               collapse = "|"
                               ),
                        feature_name)|
                    feature_name =="p" ) ~ "eGene",
                   
                   grepl("_beta|_se|dist_snp", feature_name) ~ "eQTL",
                   
                   grepl(
                      paste(c("fitCons", "cadd", "linsight",
                              "maf", "AF", "win_", "dis_to_TSS",
                              "dhs", "UTR", "intron", "exon",
                              "LDScore","intergenic", "p_",
                              "%_Nucleotide_SNP", "genes",
                              "SNP_conservation_or_consequence"),
                            collapse = "|"
                            ),
                       feature_name) ~ "eSNP", 
                  
                  (
                    nchar(feature_name) == 1|
                   grepl("IPR|interpro|amino_acid", feature_name)
                  ) ~ "Protein", 
                  
                
                  TRUE ~ "Unlabelled"
)
    
return(feat_level)
  
}

#' @export
custom_theme <- function(font_size = 11){
  
  box::use(ggplot2[...])
  
  return(
  list(
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        text = element_text(size = font_size),
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
      )
  )
  )
}

#' Plot a rank-rank plot of two importance ranks
#'  @param imp_data_1 The first dataset 
#'
#' @param imp_data_2 
#' @param imp_data_1_name Name of the importance rank 1 (to label on the plot)
#' @param imp_data_2_name 
#'
#' @export
rank_plot_from_imp = function(imp_data_1, 
                              imp_data_2,
                              imp_data_1_name = "Importance Rank 1",
                              imp_data_2_name = "Importance Rank 2"){
  
  box::use(dplyr[...], ggplot2[...])
  
  
  imp_data_1 = data.table::fread(imp_data_1) %>% 
               dplyr::mutate(rank = 1:nrow(.)) %>% 
               dplyr::mutate(feature = gsub("_", " ", feature)) %>% 
               dplyr::mutate(dplyr::across(feature, 
                              ~plotting_results_functions$group_features(.x),
                              .names = "Group"))
  
  imp_data_2 = data.table::fread(imp_data_2) %>%
               dplyr::mutate(rank = 1:nrow(.)) %>%
               dplyr::mutate(feature = gsub("_", " ", feature))
  
  topconfects::rank_rank_plot(vec1 = imp_data_1$feature,
                              vec2 = imp_data_2$feature,
                              label1 = imp_data_1_name,
                              label2 = imp_data_2_name) + 
    labs(y = "Feature Importance Rank") + 
    custom_theme
}
