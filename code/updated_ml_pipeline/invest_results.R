#!/usr/bin/env Rscript --vanilla

suppressPackageStartupMessages(require(optparse)) 

###############################################################################

# Plotting and saving 
# Other result investigation plots 

option_list = list(
  make_option(c("-i", "--imp_table"), type="character", default=NULL, 
              help="Importance Table file", metavar="character"),
  make_option(c("-t", "--scores_table"), type="character", default=NULL, 
              help="Scores Table file", metavar="character"),
  make_option(c("-s", "--save_plots"), type = "logical", default=TRUE, 
              help = "Whether to save the produced figures"),
  make_option(c("-p", "--lofo_imp"), type = "logical", default=TRUE, 
              help = "Is the importance table a lofo importance table?"),
  make_option(c("--test_set"), type = "character", help= "test set file"),
  make_option(c("-d", "--save_plot_dir"), type = "character", default=".",
              help = "What directory to save the produced figures, defaults to current directory"),
  make_option(c("-c", "--class"), type = "character", default=NULL,
              help = "What are the specific (element 1) and shared (element 2) class names. For ROC and PRC plotting, the specific class is 
              considered the positive class."),
  make_option(c("--save_tables"), type = "logical", default = FALSE,
              help = "Should we save the tables used to produce the ROC and PRC plots?"),
  make_option(c("--save_tables_dir"), type = "character", default = "save_plot_dir",
              help = "What directory to save the tables used to produce the ROC and PRC plots, defaults as the same place as where the 
              plots are saved."),
  make_option(c("-l", "--label", type = "character", default = NULL,
                help = "What label to add to the names of saved plot / tables."))
); 


opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

renv::load(here::here())

################################################################################

# for using slurm and plotly (honestly I don't understand why ggplotly works
# if this is included, but it doesn't if it isn't)
options(bitmapType='cairo')


if(!is.null(opt$imp_table)){
  
if(file.exists(opt$imp_table)){
  
imp_table = data.table::fread(paste0(opt$imp_table))

}
  
  
} 
scores = data.table::fread(paste0(opt$scores))

# get the names of the 'specific' and 'shared' classes
class = strsplit(opt$class, ",")[[1]]
specific_class = class[1]
shared_class = class[2]

# Required packages 

suppressMessages(library(dplyr))
library(ggplot2)
library(RColorBrewer)
library(patchwork)
library(stringr)
library(magrittr)


# Load functions from plotting_results_functions.R script using box
options(box.path = getwd())  # wd should just be the pop_spec_eqtl folder

box::use(code/utils/plotting_results_functions)

# ggplot theme

custom_theme = plotting_results_functions$custom_theme(18)



################################################################################



# Feature importance Plot

# Plot of ranking of feature importance score

if(!is.null(opt$imp_table)){
  
  if(file.exists(opt$imp_table)){

message("\n Producing Feature Importance Plots ...")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Function to group features together to label



if(!opt$lofo_imp){

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# Apply this function on importance results - to group them for plotting

imp_table = data.frame(
  imp_table, 
  rank = 1:nrow(imp_table) # give each feature a rank in terms of importance
) %>% 
  rowwise() %>% # group features into categories for plotting (categories above)
  mutate( # Gene conservation + SNP Conservation + Allele Freq and other
    dplyr::across(dplyr::any_of(c("V1", "feature")), 
                         ~plotting_results_functions$group_features(.x),
                         .names = "Group"),
    dplyr::across(dplyr::any_of(c("V1", "feature")), #when lofo is used to produce imp
                         ~plotting_results_functions$feature_level(.x), #file - feature
                         .names = "Feature_level") #is the column name refering to the
         )# feature name - otherwise there is no column name for this column - which
# when it is read in become V1


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# What's the mean importance score of the highest rank feature (to use to scale graphs by)
top_imp = max(abs(imp_table %$% mean_imp))  
  
# plot of the relative importance score of all included features  
# coloured by group
p1 = imp_table %>% 
      rowwise() %>% 
      mutate(scaled_mean_imp = mean_imp/top_imp) %>% 
      ggplot() + 
      geom_col(aes(x=rank, y=scaled_mean_imp, fill = Group)) + 
      custom_theme + 
      scale_fill_brewer(palette="Paired", na.value  = "black") + 
      labs(x = "Features ranked by mean importance",
           y= "Scaled mean importance") 

# plot of the relative importance score of all included features  
# coloured by feature level
p2 = imp_table %>% 
  rowwise() %>% 
  mutate(scaled_mean_imp = mean_imp/top_imp) %>% 
  ungroup() %>% 
  slice_max(., scaled_mean_imp, n= 100) %>% 
  ggplot() + 
  geom_col(aes(x=rank, y=scaled_mean_imp, fill = Feature_level)) + 
  custom_theme + 
  scale_fill_brewer(palette="Dark2", na.value  = "black") + 
  labs(x = "Features ranked by mean importance (top 100)",
       y= "Scaled mean importance") 

# plot of the relative importance scores of the top 20 included features
# coloured by group
p3 = imp_table %>% 
     rowwise() %>% 
     mutate(scaled_mean_imp = mean_imp/top_imp) %>% 
     ungroup() %>% 
     slice_max(., scaled_mean_imp, n= 15) %>% 
     ggplot() + 
     geom_col(aes(x=rank, y=scaled_mean_imp, fill = Group)) + 
     custom_theme + 
     scale_fill_brewer(palette="Paired", na.value = "black") + 
     labs(x = "Top 15 features ranked by mean importance",
       y= "Scaled mean importance") + 
     coord_cartesian(xlim =c(1,15))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

} 
    
else {
  
  
p1 = imp_table %>% # relabel columns for plotting
  rename("feature" := 1,
         "mean_lofo" := 2,
         "sd_lofo" := 3) %>%
  
  # calculate the upper and lower parts of the error bar to plot
  # using sd in lofo scores
  mutate(upper = mean_lofo + sd_lofo,
         lower = mean_lofo - sd_lofo) %>% 
  mutate(rank = 1:n()) %>% 
  
  # remove _ from the names of the features 
  # for nicer plotting names
  mutate(feature = gsub("_", " ", feature)) %>% 
  
  # find the grouping of the feature - 
  # allowing us to plot this by colour
  # needs rowwise as the function for grouping features
  # is not vectorised
  rowwise() %>% 
  mutate(
    dplyr::across(1, 
                  ~plotting_results_functions$group_features(.x),
                  .names = "Group")) %>% 
  
  # Plot the lofo importance
  # including error bars using sd
  ggplot() + 
  geom_col(aes(x=reorder(feature,rank), y=mean_lofo, fill = Group)) +
  scale_fill_brewer(palette="Paired", na.value  = "black") + 
  geom_errorbar(aes(ymin = lower,
                    ymax = upper,
                    x = feature),
                col = "darkgrey") + 
  custom_theme + 
  labs(x = "",
       y = "LOFO Feature Importance") + 
  theme(axis.text.x=element_text(size=12))
  
}

# save these plots of the importance scores
if(opt$save_plots){
  
  message("\n ... Saving feature importance plots as ")
  
  message(opt$save_plot_dir, opt$label, "_feature_importance_plots.pdf", "\n \n")
  
ggsave(plot = p1,
       paste0(opt$save_plot_dir, opt$label, "_feature_importance_plots_group.pdf"),
       width = 400,
       height = 200,
       units = "mm")

if(!opt$lofo_imp){

ggsave(plot = p2,
       paste0(opt$save_plot_dir, opt$label, "_feature_importance_plots_feat_level.pdf"),
       width = 300,
       height = 200,
       units = "mm")
  
  ggsave(plot = p3,
         paste0(opt$save_plot_dir, opt$label, "_top_15_feature_importance_plots.pdf"),
         width = 450,
         height = 200,
         units = "mm")
 }
}

  }
  
}
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


################################################################################


# Plot distribution of scores 

# for specific and shared effects

message("\n  Producing plots of the distribution of scores ... ")
message("\n ... For both ", specific_class, " and ", shared_class, " classified eQTLs \n")



test_set = readLines(opt$test_set)

scores = scores %>% dplyr::filter(ID %in% test_set)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Get data.frames of specific and shared scores

specific = scores %>% filter(Class == specific_class)

shared = scores %>% filter(Class == shared_class)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Calculate the height of the histogram as 5% of the total number
# of shared eQTLs
# and round this to the nearest 1000

n_shared = nrow(shared)

height_histogram = round(0.05*n_shared, -3)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

columns = names(scores)

# what is the score threshold chosen by the pipeline
predicted_column = columns[grepl("Predicted", columns)]

# where should the predicted line be drawn on the ggplot (i.e. numeric value of the )
line = str_remove(predicted_column, "Predicted_") %>% as.numeric()

message("... Score threshold for specificity was chosen to be", " ", line)

# calculate the percentage of specific instances declared specific at this threshold 
anno_spec  = sum(specific[,..predicted_column] == specific_class) / length(specific$Mean) 

# calculate the percentage of shared instances declared shared at this threshold
anno_shared = sum(shared[,..predicted_column] == shared_class) / length(shared$Mean) 


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# plot the distribution of scores
# overlaying the percentage correctly annotated
# and the line of score threshold chosen
scores_plot = scores %>% 
  ggplot() + 
  geom_histogram(aes(x=Mean, 
                     fill = Class), 
                     bins = 100) + 
  facet_wrap(~Class, nrow =2) + 
  labs(x = "Mean Predicted 'Specificity' Score", y = "") + 
  custom_theme +    
  scale_fill_brewer(palette="Dark2") + 
  geom_vline(xintercept = line) + 
  geom_text(data = data.frame(x = line+0.05, 
                              y = height_histogram, 
                              Class = shared_class, 
                              label = paste0(round(100-anno_shared*100, digits = 2), "%")), 
            aes(x = x, y = y, label = label)
                                            )  + 
  geom_text(data = data.frame(x = line+0.05, 
                              y =height_histogram,
                              Class = specific_class, 
                              label = paste0(round(anno_spec*100, digits = 2), "%")), 
            aes(x = x, y = y, label = label)
                                            ) + 
  geom_text(data = data.frame(x = line-0.05, 
                              y = height_histogram, 
                              Class = shared_class, 
                              label = paste0(round(anno_shared*100, digits = 2), "%")), 
            aes(x = x, y = y, label = label)
                                            )  + 
  geom_text(data = data.frame(x = line-0.05, y =height_histogram, 
                              Class = specific_class, 
                              label = paste0(round(100-anno_spec*100, digits = 2), "%")), 
            aes(x = x, y = y, label = label))



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# save the plot of the distribution of scores across specific and shared groups
if(opt$save_plots){
  
message("\n ... Saving plot of scores across specific and shared groups as")
message(opt$save_plot_dir, opt$label, "_scores_plot.pdf", "\n")
  
ggsave(plot = scores_plot,
       paste0(opt$save_plot_dir, opt$label, "_scores_plot.pdf"),
       width = 200,
       height = 300,
       units  = "mm")
}

##############################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Plotting test set ROC

message("\n \n Producting ROC plot ...")

# Positive class is specific
# Negative class is shared 

# Set up thresholds for scores for a 
# particular eQTL being declared specific 
threshold =  rev(seq(from = 0, 
                 to = 1, 
                 by = 0.01))

# set up vectors of true positives (TP), false positives (FP), 
# False Positive Rate (FPR), True Positive Rate (TPR)
# at each threshold level 
TP = vector()
FP = vector()
FPR = vector()
TPR = vector()


# How many eQTLs are in the postive class (specific class)
P = scores %>% 
    filter(Class == specific_class) %>% 
    nrow()

N = scores %>% 
  filter(Class == shared_class) %>% 
  nrow()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# for each threshold
# calculate the number of TP, FP
# and the FPR and TPR

#n = grepl("score", names(scores)) %>% sum()

for(i in 1:length(threshold)){
  
  # at this threshold which eQTLs are declared specific 
  # how many of these are truly specific
  TP[i] = scores %>% 
          filter(Class == specific_class & Mean > threshold[i]) %>% 
          nrow()
  
  # at this threshold which eQTLs are declared specific 
  # how many of these are actually shared   
  FP[i] = scores %>% 
          filter(Class == shared_class & Mean > threshold[i]) %>% 
          nrow()
  
  
  # False positive rate, Falsely declared specific /  Number of shared eQTLs
  FPR[i] = FP[i] / N
  
  # True positive rate, Truly declared specific / Number of specific eQTLs
  TPR[i] = TP[i] / P
   
  
}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# If asked to, save the data points used to produce the ROC plot

if(opt$save_tables){
  
  
  message("... Saving the tables used to produce the ROC plot as")
  
  
  if(opt$save_tables_dir == "save_plot_dir"){
    
    
  message(opt$save_plot_dir, opt$label, "_roc_table.txt", "\n")

    
  roc_table = data.frame(threshold,
                       TPR = TPR,
                        FPR = FPR)
  

  write.table(roc_table, paste0(opt$save_plot_dir,opt$label, "_roc_table.txt"), sep = "\t")

  }
  
  else{
    
    message(opt$save_tables_dir, opt$label, "_roc_table.txt")
    
    roc_table = data.frame(threshold,
                           TPR = TPR,
                           FPR = FPR)
    
    write.table(roc_table, paste0(opt$save_tables_dir, opt$label, "_roc_table.txt"), sep = "\t")
  }

}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 



# Plot ROC curve as TPR vs FPR 

roc_plot = data.frame(TPR = TPR,
                      FPR = FPR) %>% 
  ggplot(aes(x=FPR,y=TPR)) + 
  geom_line(size = 3) + 
  custom_theme + 
  geom_abline(slope = 1, 
              intercept = 0, 
              col = "red", 
              linetype = 2, size = 2) + 
  geom_abline(slope = 0, 
              intercept = 1, 
              col = "grey", 
              linetype = 2, size = 2) + 
  labs(y = "Sensitivity",
       x= "1-Specificity") +
  xlim(0,1) + 
  ylim(0,1)

  
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

  
# save ROC curve plot     
  
if(opt$save_plots){  
  
message("\n ... Saving plot of ROC curve in the testing set as")
message(opt$save_plot_dir,"_roc_plot.pdf", "\n")
  
ggsave(plot = roc_plot,
       paste0(opt$save_plot_dir,"_roc_plot.pdf"),
       width = 300,
       height = 200,
       units  = "mm")
}
  

  
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

################################################################################

# Plotting test set PRC

message("\n \n Producting PRC plot ...")


for(i in 1:length(threshold)){
  
  # at this threshold which eQTLs are declared specific 
  # how many of these are false postives
  FP[i] = scores %>% 
    filter(Class == shared_class & Mean > threshold[i]) %>% 
    nrow()
  
  
} 

Precision = TP / (FP + TP)

Recall = TP / P

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# If asked to, save the data points used to produce the ROC plot

if(opt$save_tables){
  
  
  message("... Saving the tables used to produce the ROC plot as")
  
  
  if(opt$save_tables_dir == "save_plot_dir"){
    
    
    message(opt$save_plot_dir, opt$label, "_prc_table.txt", "\n")
    
    
    prc_table = data.frame(threshold,
                           TPR = TPR,
                           FPR = FPR)
    
    
    write.table(prc_table, paste0(opt$save_plot_dir,opt$label, "_prc_table.txt"), sep = "\t")
    
  }
  
  else{
    
    message(opt$save_tables_dir, opt$label, "_prc_table.txt")
    
    prc_table = data.frame(threshold,
                           TPR = TPR,
                           FPR = FPR)
    
    write.table(prc_table, paste0(opt$save_tables_dir, opt$label, "_prc_table.txt"), sep = "\t")
  }
  
}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 



# Plot ROC curve as TPR vs FPR 

random_line = P / (P + N)

prc_plot = data.frame(Precision = Precision,
                      Recall =  Recall) %>% 
  ggplot(aes(y=Precision,x=Recall)) + 
  geom_line(size = 3) + 
  custom_theme + 
  geom_abline(slope = 0, 
              intercept = 
                random_line, 
              col = "red", 
              linetype = 2,
              size = 2) +
  geom_abline(slope = 0, 
              intercept = 1, 
              col = "grey", 
              linetype = 2) +
 annotate(
  "text",
  x = 0.3,
  y = random_line + 0.1,
  size = 8,
  col = "red", 
  label = paste("Random chance =", round(random_line, digits = 2)) 
) + 
  xlim(0,1) + 
  ylim(0,1)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# save PRC curve plot     

if(opt$save_plots){  
  
  message("\n ... Saving plot of PRC curve in the testing set as")
  message(opt$save_plot_dir,"_prc_plot.pdf", "\n")
  
  ggsave(plot = prc_plot,
         paste0(opt$save_plot_dir,"_prc_plot.pdf"),
         width = 300,
         height = 200,
         units  = "mm")
}



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #   
  
