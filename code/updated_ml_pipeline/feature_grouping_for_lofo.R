#!/usr/bin/env Rscript --vanilla

suppressPackageStartupMessages(require(optparse)) 

###############################################################################


# Overview of script:

# Takes in a feature file (a .txt file where each line corresponds to a feature name)
# as produced using Feature_Selection.py step. Used as input for the -feat argument of
# the ML_classification.py script

# Produces a file - with the same number of rows as the original feature file
# where each line corresponds to the feature group of the feature in the same line. 
# This file is designed to be used as input for the -feat_group argument of the
# ML_classification.py script. 

# The function mapping features to groups is designed to work with the feature names
# of data frames produced from the feature_extraction scripts / steps. 

# For e.g. if you have a feature file with features:

# feature_1
# feature_2
# feature_3

# This script could produce a file like: 

# group_1
# group_1
# group_2

# Demonstrating that feature_1 and feature_2 belong to group_1 and feature_3
# belongs to group_2. 

################################################################################

option_list = list(
  make_option(c("-f", "--feature_file"), type="character",
              help="File path of features (.txt file with each line a features)", 
              metavar="character"),
  make_option(c("-s", "--save_file_name"), type = "character", default = NULL,
              help = "Where to save the produced feature grouping file"),
  make_option(c("-r", "--rm_space"), type="logical", default = TRUE,
              help="Remove white space between words in grouped items", 
              metavar="character")
); 


################################################################################

# Script set up
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

# load project specific R library fron renv
renv::load(here::here())

# read in features as a vector
features = readLines(opt$feature_file)

################################################################################

options(box.path = here::here())

box::use(code/utils/plotting_results_functions[...])

# make function that returns the group name of a feature (output)
# from the feature name (input)
# group_features = function(feature) {
# 
#   if(grepl("GO|go", feature)){
# 
#     return("GOslim_term")
# 
#   }
# 
#   else if(grepl("LD_score", feature)){
# 
#     return("LD_score")
#   }
# 
# 
#   else if("phastcons"== feature |
#           "phylop" == feature |
#           grepl("oe|classic|p_", feature) |
#           feature %in% c("p","pLI", "pRec","pNull")){
# 
#     return("gene_conservation_or_constraint")
#   }
# 
#   else if(grepl("fitCons|cadd|linsight",feature)){
# 
#     return("SNP_conservation_or_consequence")
# 
#   }
# 
# 
#   else if(grepl("PC|factor|tpm|brain|non_zero",feature)&
#           !grepl("wb|lcl",feature)){
# 
#     return("Cross_tissue_gene_expression_metrics")
# 
# 
#   }
# 
#   else if(grepl("wb|lcl",feature)) {
# 
# 
#     return("Within_tissue_gene_expression_metrics")
# 
# 
#   }
#   else if(grepl("win_", feature)){
# 
#     return("%_Nucleotide_SNP")
#   }
# 
#   else if(grepl("gene_%|gene.",feature)
#           &!grepl("biotype", feature)){
# 
#     return("%_Nucleotide_Gene")
#   }
# 
#   else if(grepl("immport|innate",feature)){
# 
#     return("immune_database_annotated")
#   }
# 
#   else if(grepl("kegg", feature)){
# 
#     return("kegg_pathway_term")
#   }
# 
#   else if(nchar(feature)==1){
# 
#     return(" %_amino_acid")
#   }
# 
#   else if(grepl("interpro|IPR",feature)){
# 
#     return("interpro_terms")
#   }
# 
#   else{
#     return(feature)
#   }
# 
# }
# 
# # Apply feature grouping function to entire set
# # of feature names read into the feature vector -
# # to obtain full set of mapping from feature name to feature group name
# 
grouped_features = group_features(features)
# 

if (opt$rm_space){
  
  grouped_features = gsub(" ", "_", grouped_features)
}

################################################################################

# Saving the output file of feature group names


# if the output file name is not provided
# then the input file is assumed to have come from the outputml_data/ml_feature_sets
if(is.null(opt$save_file_name)){

  

  feature_file_name = opt$feature_file

  feat_group_file_name = gsub("ml_feature_sets/feature",
                              "ml_feature_sets/feat_group",
                              feature_file_name)

  # add reference to the fact it is now a grouped feature file:

  # feat_group_file_name = gsub(".txt",
  #                             "_group.txt",
  #                             feat_group_file_name)


} else {

  feat_group_file_name = opt$save_file_name
  
}


message("Saving grouped features file as ",
        feat_group_file_name)

message("\n If this location is not what you want - please set 
        --save_file_name to be the correct path/ name!")

# Save file of grouped features
writeLines(grouped_features, feat_group_file_name)

