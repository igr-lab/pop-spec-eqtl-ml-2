

# Across samp. algo tables: 

extract_results = function(ml_run_path){
  
  # from the relative path, get the subfolder title (this is the 'ID of the run')
  
  ml_run_subfolder = gsub("./ml_runs/", "", ml_run_path)
  
  # read in the results from RESULTS.txt
  
  results_file = list.files(ml_run_path)[
    grep("RESULTS.txt", list.files(ml_run_path))
  ]
  results  = data.table::fread(paste0(ml_run_path, "/", results_file), sep = "\t") 
  
  results = results %>% 
    as_tibble() %>% 
    mutate(ml_run_ID = ml_run_subfolder, 
           .before = 1) # 
  
  return(results)
  
}


cross_alg = c("20221031_RF_gs0_n5_nearmiss1_natri_indo",
              "20221031_RF_gs0_n5_nearmiss2_natri_indo",
              "20221031_RF_gs0_n5_nearmiss2_natri_indo",
              "20221031_RF_gs0_n5_tomeklink_natri_indo",
              "20223110_RF_gs0_n5_onesidedselection_natri_indo",
              "20221031_RF_Randomunder_natri_indo")

lapply(cross_alg, FUN = function(x) extract_results(paste0("ml_runs/",x))) %>% 
  dplyr::bind_rows(.) %>% 
  dplyr::select(auPRC_train = AUCPRc_val,
         auPRC_train_sd = AUCPRc_val_sd,
         auPRC_test = AUCPRc_test,
         AUCPRc_test_sd) %>% 
  write.csv("auPRC_sample_alg_table.csv")
  
  lapply(cross_alg, FUN = function(x) extract_results(paste0("ml_runs/",x))) %>% 
    dplyr::bind_rows(.) %>% 
    dplyr::select(auROC_train = AUCROC_val,
                  auROC_train_sd = AUCROC_val_sd,
                  auROC_test = AUCROC_test,
                  auROC_test_sd = AUCROC_test_sd) %>% 
    write.csv("auROC_sample_alg_table.csv")

