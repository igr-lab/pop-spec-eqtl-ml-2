

# correlation between lofo mean scores 

data.table::fread("ml_runs/20221102_RF_gs0_n1_mogil_ea_his_mode_LOFO_Recall/mogil_ea_lofo_imp_full_df.csv") -> recall_ea_his_lofo

data.table::fread("ml_runs/20221103_RF_gs0_n1_mogil_ea_aa_mode_LOFO_Recall/mogil_ea_lofo_imp_full_df.csv") -> recall_ea_aa_lofo

data.table::fread("ml_runs/20221103_RF_gs0_n1_mogil_aa_ea_mode_LOFO_Recall/mogil_aa_lofo_imp_full_df.csv") -> recall_aa_ea_lofo

data.table::fread("ml_runs/20221103_RF_gs0_n1_mogil_aa_his_mode_LOFO_Recall/mogil_aa_lofo_imp_full_df.csv") -> recall_aa_his_lofo

data.table::fread("ml_runs/20221103_RF_gs0_n1_mogil_his_ea_mode_LOFO_Recall/mogil_his_lofo_imp_full_df.csv") -> recall_his_ea_lofo

data.table::fread("ml_runs/20221103_RF_gs0_n1_mogil_his_aa_mode_LOFO_Recall/mogil_his_lofo_imp_full_df.csv") -> recall_his_aa_lofo

data.table::fread("ml_runs/20221104_RF_gs0_n1_natri_euro_LOFO_recall/natri_indo_lofo_imp_full_df.csv") -> natri_euro_lofo_recall

data.table::fread("ml_runs/20221103_RF_gs0_n1_natri_indo_F1_LOFO_recall/natri_indo_lofo_imp_full_df.csv") -> natri_indo_lofo_recall

options(box.path = here::here())

prepare_for_cor_mat = function(lofo_df, model){
  
 box::use(dplyr[...])
 box::use(code/utils/plotting_results_functions)  
 
 #browser()
 
 fix = lofo_df %>% 
       dplyr::arrange(V1) %>% 
       mutate(V1 = gsub("amino", "Amino", V1)) %>% 
       mutate(V1 = gsub("_", " ", V1))  %>% 
       mutate(V1 = plotting_results_functions$group_features(V1)) %>%
       select(feature_group = V1,
              !!as.symbol(paste0(model)) := V2)  
 
 return(fix)
}


full_join(prepare_for_cor_mat(natri_indo_lofo_recall, "Indo_Euro"),
      prepare_for_cor_mat(natri_euro_lofo_recall, "Euro_Indo")
      )

cor_mat = list(prepare_for_cor_mat(natri_indo_lofo_recall, "Indo_Euro"), 
     prepare_for_cor_mat(natri_euro_lofo_recall, "Euro_Indo"), 
     prepare_for_cor_mat(recall_ea_his_lofo, "EA_HIS"),
     prepare_for_cor_mat(recall_ea_aa_lofo, "EA_AA"),
     prepare_for_cor_mat(recall_aa_his_lofo, "AA_HIS"),
     prepare_for_cor_mat(recall_aa_ea_lofo, "AA_EA"),
     prepare_for_cor_mat(recall_his_aa_lofo, "HIS_AA"),
     prepare_for_cor_mat(recall_his_ea_lofo, "HIS_EA")) %>% 
     Reduce(full_join, .) %>% 
    tibble::column_to_rownames("feature_group") %>% 
    cor(.,use = "pairwise.complete.obs", method = "spearman")

{
  mm = 80 
  wmm = 100
  
  pdf("correlation_between_recall_lofo.pdf",
      height = 0.0393701*mm,
      width = 0.0393701*wmm)
  
  
  library(circlize)
  library(viridis)
  library(ComplexHeatmap)
  
  col_fun = colorRamp2(c(-1,0,1),
                       c("firebrick", "white", "dodgerblue"))
  
  
  ht = draw(Heatmap(mat = cor_mat,  
                    #col = col_fun,
                    show_column_names  = TRUE,
                    show_row_names = TRUE,
                    show_column_dend = TRUE,
                    show_row_dend = TRUE,
                    #row_order = order(1:105),
                    #column_order = order(1:105),
                    #clustering_distance_rows = "spearman",
                    #column_title_gp = grid::gpar(fontsize = 8,fontfamily="Helvetica"),
                    #row_title_gp = grid::gpar(fontsize = 8,fontfamily="Helvetica"),
                    #column_names_gp = grid::gpar(fontsize = 8,fontfamily="Helvetica"),
                    #row_names_gp = grid::gpar(fontsize = 8, fontfamily="Helvetica"),
                    name = "Spearman's\nCorrelation",
                    heatmap_legend_param = list(labels_gp = gpar(fontsize = 8)),
                    #column_title = "Correlation between non-binary features (Indonesian whole blood eQTLs)",
                    #clustering_distance_rows = function(m) dist(m)
  ),
  legend_title_gp = grid::gpar(fontsize = 8, fontface = "bold")
  #legend_label_gp = grid::gpar(fontsize = 8)
  )
  
  set.seed(123)
  
  dev.off()
  
    }
