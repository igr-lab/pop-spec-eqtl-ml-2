

# GTEx population biased eqtls

readxl::read_excel("./data/gtex_resources/GTEx_v8_pb_eQTLs.fdr_0.25.high_confidence_set.xlsx") -> pb_eqtls

pb_eqtls %>% View(.)

genes = pb_eqtls %>% 
  filter(tissue == "Whole_Blood") %>%
  dplyr::select(gene = gene_id, snp = rs_id_dbSNP151_GRCh38p7) %$%
  gene %>% 
  gsub("\\..*", "",.)


data.table::fread("./output/classified_eqtls/natri_euro_spec_v4_lfsr_0.01_majority_rules.txt") -> euro_spec

gene_overlap = euro_spec %>% filter(gene %in% genes)

pb_gene_snp = pb_eqtls %>% 
  filter(tissue == "Whole_Blood") %>%
  dplyr::select(gene = gene_id, snp = rs_id_dbSNP151_GRCh38p7) %>% 
  mutate(gene = gsub("\\..*", "",gene)) %>% 
  tidyr::unite("gene_snp",gene:snp)

left_join(pb_gene_snp,
          gene_overlap %>% 
          tidyr::unite("gene_snp",gene:snp))

data.table::fread("./output/classified_eqtls/natri_indo_spec_v4_lfsr_0.01_majority_rules.txt") -> indo_spec

gene_overlap = indo_spec %>% filter(gene %in% genes)

left_join(pb_gene_snp,
          gene_overlap %>% 
            tidyr::unite("gene_snp",gene:snp))

# mashr_results 

readRDS("output/mashr_results/natri_mashr_em_results.rds") -> natri_mashr_res

natri_mashr_res$lfsr[rownames(natri_mashr_res$lfsr) %in% c(pb_gene_snp$gene_snp),]



############# 

readRDS("output/mashr_results/mogil_only_mashr_em_results.rds") -> mogil_mashr_res

rownames(mogil_mashr_res$lfsr) %in% c(pb_gene_snp$gene_snp) %>% sum(.)

mogil_mashr_res$lfsr[rownames(mogil_mashr_res$lfsr) %in% c(pb_gene_snp$gene_snp),]


################### Manhattan plots ###################


# Function to make a data frame to plot a manhattan type plot (x axis = SNP position, y axis = Effect size)
# For a given eGene
make_plot_df = function(gene_name,
                        mashr_results,
                        ml_df){
  
  box::use(magrittr[...],
           dplyr[...])
  
#  browser()
  
  # Find effect sizes for all gene-snp pairs for this gene
  effect_size = mashr_results$PosteriorMean[grepl(gene_name,rownames(mashr_results$PosteriorMean)),]
  
  effect_size  = effect_size %>% 
    as.data.frame() %>% 
    tibble::rownames_to_column("V1") %>% 
    tidyr::separate(V1, into = c("gene", "snp"))
  
  # Get snp names- use this to find positions / location of snp
  
  snp_names = effect_size %$% snp
  
  # Find snp positions
  
  options(box.path = getwd())
  box::use(code/feature_extraction)
  
  snp_positions = suppressWarnings(
    feature_extraction$id_to_granges(snp_names, 
                                     type = "SNP",
                                     ensembl_version = 108)
  )
  
  snp_positions = snp_positions[1] %>% 
    as.data.frame() %>% 
    select(snp = refsnp_id, position = start)
  
  effect_size_pos = inner_join(effect_size, snp_positions, by = "snp")
  
  # class label
  plot_df = left_join(effect_size_pos,
                      ml_df %>% 
                        #tidyr::separate(V1, into = c("gene", "snp")) %>% 
                        select(gene, snp, Class) %>% 
                        as.data.frame(),
                      by = c("gene", "snp")) %>% 
  tidyr::replace_na(list(Class = "Non-significant"))
  
  return(plot_df)
  
}

{
  
  library(dplyr)
  library(magrittr)
  library(ggplot2)
  library(plotgardener)
  
  custom_theme <-
    list(
      theme_bw() +
        theme(
          panel.border = element_blank(),
          axis.line = element_line(),
          panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          text = element_text(size = 8),
          legend.position = "right",
          legend.box = "vertical",
          strip.background = element_blank(),
          legend.text=element_text(size=8),
          legend.title = element_text(size=8),
          legend.margin=margin(),
          axis.text = element_text(size = 8),
          axis.text.x = element_text(size = 8, angle = 45, vjust = 0.5)
        )
    )
  
  # Plotting colours
  hispanic_col = c("#009988")
  shared_col = c("#000000")
  european_col = c("#0077BB")
  indonesian_col = c("#CC3311")
  african_american_col = c("#EE3377")
  na_col = c("#888888")
  
}

####################### Portable eQTL Plot #####################

make_plot_df("ENSG00000197728", 
             natri_mashr_res, 
             indo_spec) -> indo_plot_df

make_plot_df("ENSG00000197728", 
             natri_mashr_res, 
             euro_spec) -> euro_plot_df

SNP_position = indo_plot_df %>% filter(snp == "rs1131017") %$% position

SNP_indo_eff = indo_plot_df %>% filter(snp == "rs1131017") %$% indo_beta

SNP_gtex_eff = euro_plot_df %>% filter(snp == "rs1131017") %$% gtex_beta

(euro_plot_df %>% 
    ggplot(aes(x=position,
               y=gtex_beta,
               col = Class)) + 
    geom_point() +
    labs(x = "Chromosome Position",
         y = "GTEx Effect Size") + 
    scale_color_manual(values = c(na_col,shared_col,european_col)) +
    annotate("text", 
             hjust = 0,
             x= SNP_position + 100000, 
             y= SNP_gtex_eff, label = "rs1131017",
             size= 8/.pt) + 
    custom_theme +
    ylim(-1.5,1.5) + 
    theme(legend.position="none")
) + (
  indo_plot_df %>% 
    ggplot(aes(x=position,
               y=indo_beta,
               col = Class)) + 
    geom_point() +
    labs(x = "Chromosome Position",
         y = "Indonesian Effect Size") + 
    scale_color_manual(values = c(na_col,shared_col,indonesian_col)) +
    custom_theme + 
    theme(legend.position="none") + 
    annotate("text", 
             hjust = 0,
             x= SNP_position + 100000, 
             y= SNP_indo_eff, label = "rs1131017",
             size= 8/.pt) + 
    ylim(-1.5,1.5)
) + 
  plot_annotation("ENSG00000197728")

ggsave("test.pdf",
       width = 150,
       height = 100,
       units = "mm")


###################### Non-portable eQTL ##############

make_plot_df("ENSG00000160221", 
             natri_mashr_res, 
             indo_spec) -> indo_plot_df

make_plot_df("ENSG00000160221", 
             natri_mashr_res, 
             euro_spec) -> euro_plot_df


SNP_position = indo_plot_df %>% filter(snp == "rs2299818") %$% position

SNP_indo_eff = indo_plot_df %>% filter(snp == "rs2299818") %$% indo_beta

SNP_gtex_eff = euro_plot_df %>% filter(snp == "rs2299818") %$% gtex_beta

library(ggplot2)
library(patchwork)

(euro_plot_df %>% 
    ggplot(aes(x=position,
               y=gtex_beta,
               col = Class)) + 
    geom_point() +
    labs(x = "Chromosome Position",
         y = "GTEx Effect Size") + 
    scale_color_manual(values = c(na_col,shared_col,european_col)) +
    annotate("text", 
             hjust = 0,
             x= SNP_position + 50000, 
             y= SNP_gtex_eff, label = "rs2299818",
             size= 8/.pt) + 
    custom_theme +
    ylim(-2,2) + 
    theme(legend.position="none")
) + (
  indo_plot_df %>% 
    ggplot(aes(x=position,
               y=indo_beta,
               col = Class)) + 
    geom_point() +
    labs(x = "Chromosome Position",
         y = "Indonesian Effect Size") + 
    scale_color_manual(values = c(indonesian_col,na_col,shared_col)) +
    custom_theme +
    theme(legend.position="none") + 
    annotate("text", 
             hjust = 0,
             x= SNP_position + 50000, 
             y= SNP_indo_eff + 0.1, label = "rs2299818",
             size= 8/.pt) + 
    ylim(-2,2)
) + 
  plot_annotation("ENSG00000160221")

ggsave("test.pdf",
       width = 150,
       height = 100,
       units = "mm")
