

# ven diagram immune vs immport

(
natri_indo %>% 
  rowwise() %>% 
  mutate(immune_any = sum(c_across(c(contains("Immport"),
                                     contains("innate")))))  %>% 
  ungroup() %>% 
  mutate(immune_any = as.logical(immune_any),
         has_immune_go_term = as.logical(has_immune_go_term)) %>% 
  ggvenn(., c("test" = "immune_any","has_immune_go_term"),
         fill_color = c("#66c2a5","#fc8d62"),
         text_size = 8/.pt,
         set_name_size = 12/.pt) + 
  theme_void() + coord_fixed()
) / (
  ea_aa %>% 
    rowwise() %>% 
    mutate(immune_any = sum(c_across(c(contains("Immport"),
                                       contains("innate")))))  %>% 
    ungroup() %>% 
    mutate(immune_any = as.logical(immune_any),
           has_immune_go_term = as.logical(has_immune_go_term)) %>% 
    ggvenn(., c("test" = "immune_any","has_immune_go_term"),
           fill_color = c("#66c2a5","#fc8d62"),
           text_size = 8/.pt,
           set_name_size = 12/.pt) + 
    theme_void() + coord_fixed()
)

ggsave("test_venn.pdf", height = 120, width = 150, units = "mm")
