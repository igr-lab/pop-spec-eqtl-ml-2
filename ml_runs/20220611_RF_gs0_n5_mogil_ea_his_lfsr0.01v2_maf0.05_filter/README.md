

Steps /code for Filtering MAF in gnomad populations relevant to Mogil

### Relevant code:

```

ml_filtered_df = data.table::fread("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/output/ml_data/ml_full_sets/mogil_ea_spec_his_lfsr_v2_0.01_majority_rules.txt") %>% 
filter(gnomAD_AF_afr > 0.05 & gnomAD_AF_amr > 0.05 & gnomAD_AF_nfe_nwe > 0.05)


write.table(ml_filtered_df %>% 
            tibble::column_to_rownames("V1"), 
            sep ="\t", "/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/output/ml_data/ml_full_sets/mogil_ea_spec_his_lfsr_v2_0.01_majority_rules_filtered_maf0.05.txt")

```