
# Run details

```
# run python script

echo "Preprocessing"
echo " "
python3 ./code/updated_ml_pipeline/ML_preprocess.py \
        -df ./output/ml_data/natri_ml_test_v1.1_beta.txt \
        -na_method median \
        -save ./ml_runs/TBC_RF_gs0_n10_natri_ml_test_lsfr0.05_no_feature_selection/natri_ml_test_mod.txt

echo "Feature engineering"
echo " "
python3 ./code/updated_ml_pipeline/test_set.py \
         -df ./ml_runs/TBC_RF_gs0_n10_natri_ml_test_lsfr0.05_no_feature_selection/natri_ml_test_mod.txt \
         -use Indo-Specific,Shared \
         -type c  \
         -p 0.1 \
         -drop_na T \
         -save ./ml_runs/TBC_RF_gs0_n10_natri_ml_test_lsfr0.05_no_feature_selection/test_eqtls.txt

                            
echo "Classification"
echo " "
python3 ./code/updated_ml_pipeline/ML_classification.py \
    -df ./ml_runs/TBC_RF_gs0_n10_natri_ml_test_lsfr0.05_no_feature_selection/natri_ml_test_mod.txt \
    -test ./ml_runs/TBC_RF_gs0_n10_natri_ml_test_lsfr0.05_no_feature_selection/test_eqtls.txt \
    -cl_train Indo-Specific,Shared \
    -alg RF \
    -cv 10 \
    -gs F \
    -max_depth 10 \
    -n_estimators 1000 \
    -max_features 0.5 \
    -n 10 \
    -n_jobs 72 \
    -plots T \
    -cv_num 10 \
    -save ./ml_runs/TBC_RF_gs0_n10_natri_ml_test_lsfr0.05_no_feature_selection/metab_RF

echo "Feature Importance and Scores Plots" 
echo " "

Rscript --vanilla ./code/updated_ml_pipeline/invest_results.R -i ./ml_runs/TBC_RF_gs0_n10_natri_ml_test_lsfr0.05_no_feature_selection/metab_RF_imp \
                                                              -t ./ml_runs/TBC_RF_gs0_n10_natri_ml_test_lsfr0.05_no_feature_selection/metab_RF_scores.txt \
                                                              -d ./ml_runs/TBC_RF_gs0_n10_natri_ml_test_lsfr0.05_no_feature_selection \
                                                              -c Indo-Specific,Shared
```