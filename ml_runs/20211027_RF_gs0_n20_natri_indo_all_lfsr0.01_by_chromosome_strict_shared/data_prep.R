

# Data Creation

#Rscript --vanilla ./code/calling_pop_spec_eqtl/call_pop_spec_mashr_sig.R -r ./output/mashr_results/natri_mashr_results.rds \
#--lfsr_p1 0.01 \
#--lfsr_p2 0.1 \
#--p1 indo \
#--p2 lepik,gtex,twinsuk \
#--class shared,indo-specific \
#-i ./output/preclassified_eqtls/natri_indo_preclass.csv \
#--save ./output/classified_eqtls/indo_spec_eqtl_natri_lfsr0.01_strict_shared.csv

ml_all = read.table("output/ml_data/natri_indo_lfsr0.01_features_df.txt")

new_classification = data.table::fread("output/classified_eqtls/indo_spec_eqtl_natri_lfsr0.01_strict_shared.csv")

new_Class_df = inner_join(ml_all %>% tibble::rownames_to_column("gene_snp"),
           new_classification %>% 
            tidyr::unite("gene_snp", gene:snp) %>% 
           select(gene_snp,New_Class = Class))

new_Class_df %>% 
  select(-Class) %>% 
  rename(Class = New_Class) %>% 
  filter(Class != "Unclassified") %>% 
  tibble::column_to_rownames("gene_snp") %>% 
  write.table(., "output/ml_data/natri_indo_lfsr0.01_strict_shared_features_df.txt", sep = "\t")
