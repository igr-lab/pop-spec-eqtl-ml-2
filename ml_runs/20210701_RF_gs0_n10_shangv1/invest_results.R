

###############################################################################

# Other result investigation plots 

args = commandArgs(TRUE)
imp_table = args[1]
scores = args[2]

################################################################################
################################################################################

imp_table = data.table::fread(paste0(imp_table))
scores = data.table::fread(paste0(scores))


# Required packages 

library(dplyr)
library(ggplot2)
library(RColorBrewer)
library(patchwork)
library(stringr)
library(magrittr)

# ggplot theme

custom_theme <-
  list(
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        text = element_text(size = 9),
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
      )
  )



################################################################################



# Feature importance Plot

# Plot of ranking of feature importance score

print("Feature Importance Plot")

imp_table = data.frame(
  imp_table, 
  rank = 1:nrow(imp_table) # give each feature a rank in terms of importance
) %>% 
  rowwise() %>% # group features into categories for plotting (these categories are GO terms + mashr output
  mutate( # Gene conservation + SNP Conservation + Allele Freq and other
    Group = ifelse(grepl("GO", V1), "GOSlim Term", 
                   ifelse(grepl("beta", V1) | grepl("se", V1), 
                          "mashr output (effect size / se)",
                          ifelse( "phastcons"== V1 | "phylop" == V1 | grepl("oe", V1),
                                  "Gene Conservation / Constraint",
                                  ifelse("fitCons" == V1 | grepl("cadd", V1) | "linsight" == V1, 
                                         "SNP Conservation / Consequence",
                                         ifelse(grepl("af", V1) | grepl("AF", V1), "Allele Freq", 
                                                "Other")
                                  )
                          )
                   )
    )
  )
# What's the mean importance score of the highest rank feature (to use to scale graphs by)
top_imp = max(imp_table %$% mean_imp)  


p1 = imp_table %>% 
  rowwise() %>% 
  mutate(scaled_mean_imp = mean_imp/top_imp) %>% 
  ggplot() + 
  geom_col(aes(x=rank, y=scaled_mean_imp, fill = Group)) + 
  custom_theme + 
  scale_fill_brewer(palette="Set2") + 
  labs(x = "Features ranked by mean importance",
       y= "Scaled mean importance") 


p2 = imp_table %>% 
  rowwise() %>% 
  mutate(scaled_mean_imp = mean_imp/top_imp) %>% 
  ggplot() + 
  geom_col(aes(x=rank, y=scaled_mean_imp, fill = Group)) + 
  custom_theme + 
  scale_fill_brewer(palette="Set2") + 
  labs(x = "Top 15 features ranked by mean importance",
       y= "Scaled mean importance") + 
  xlim(0,15)


p3 = imp_table %>% 
  rowwise() %>% 
  mutate(scaled_mean_imp = mean_imp/top_imp) %>% 
  ggplot() + 
  geom_col(aes(x=rank, y=scaled_mean_imp, fill = Group)) + 
  custom_theme + 
  scale_fill_brewer(palette="Set2") + 
  labs(x = "Top 20 features ranked by mean importance",
       y= "Scaled mean importance") + 
  xlim(0,20)

# theme(fill = brewer.pal(unique(imp_table$Group) %>% length(), "Dark2"))

feature_importance_plot = p1 + p2

ggsave(plot = feature_importance_plot,
       "./feature_importance_plots.pdf",
       width = 400,
       height = 200,
       units  = "mm")

ggsave(plot = p3,
       "./top_feature_importance_plots.png",
       width = 400,
       height = 200,
       units  = "mm")

################################################################################


# Distribution of scores 

print("Scores Plots")

specific = scores %>% filter(Class == "specific")

shared = scores %>% filter(Class == "shared")

head(specific)

columns = names(scores)

predicted_column = columns[grepl("Predicted", columns)]

print(predicted_column)

anno_spec  = sum(specific[,..predicted_column] == "specific") / length(specific$Mean) 
anno_shared = sum(shared[,..predicted_column] == "shared") / length(shared$Mean) 


line = str_remove(predicted_column, "Predicted_") %>% as.numeric()


scores_plot = scores %>% 
  ggplot() + 
  geom_histogram(aes(x=Mean, fill = Class), bins = 100) + 
  facet_wrap(~Class, nrow =2) + labs(x = "Mean Predicted 'Specificity' Score", y = "") + 
  custom_theme +    
  scale_fill_brewer(palette="Dark2") + 
  geom_vline(xintercept = line) + 
  geom_text(data = data.frame(x = line+0.05, y = 20000, Class = "shared", label = paste0(round(100-anno_shared*100, digits = 2), "%")), 
            aes(x = x, y = y, label = label))  + 
  geom_text(data = data.frame(x = line+0.05, y =20000, Class = "specific", label = paste0(round(anno_spec*100, digits = 2), "%")), 
            aes(x = x, y = y, label = label)) + 
  geom_text(data = data.frame(x = line-0.05, y = 20000, Class = "shared", label = paste0(round(anno_shared*100, digits = 2), "%")), 
            aes(x = x, y = y, label = label))  + 
  geom_text(data = data.frame(x = line-0.05, y =20000, Class = "specific", label = paste0(round(100-anno_spec*100, digits = 2), "%")), 
            aes(x = x, y = y, label = label))

ggsave(plot = scores_plot,
       "./scores_plot.pdf",
       width = 200,
       height = 300,
       units  = "mm")

ggsave(plot = scores_plot,
       "./scores_plot.png",
       width = 200,
       height = 300,
       units  = "mm")


##############################################################################