

# Details of Run: 

Basics:

- Run on the 1/7/2021
- No grid search (to save time)
- CV validation of only n = 10
- Shang and Smith et al. 2020 combined dataset
- From only two populations (european american and african american), so from applying mashr 1v1 
- Declare snp-gene pair as eqtl at lsfr threshold of 0.05 in at least one population 
- Declared shared if at least lsfr < 0.5 in secondary population

## Thoughts to follow up:

- standard error of effect sizes is an important feature for the model, maybe consider using a measure of significance? 
- standard error of effect sizes was included in the model, which is obviously related to significance, which means I may have accidentally provided the model with something that correlates nicely with significance in both populations (bad bad thing) - check by removing standard error from one population
- does grid search improve things?
- what about other algorithms?
- interesting to see how far down cadd is in the feature importance (weird considering cadd and fitCons are correlated)
- this doesn't include maf from global populations - should add (especially since maf is an important feature)

- Weird error / concern in step one of ML Pipeline: 
===>  ML Pipeline started  <===
  Round 1 of 10
/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/scripts/ML-Pipeline/no_grid_search/ML_functions.py:712: RuntimeWarning: invalid value encountered in long_scalars


## Slurm details

- 72 cores (used all - but was not an efficient use of resources - only 20% efficiency)
- 100 GB memory (only used 12%)


## Features

- GO Slim (GOA Accession)
- Mashr effect size (one population, european american); mashr standard error (both populations)
- fitCons, cadd, phyloP, phastCons, linsight
- Dataset specific allele frequency 


## Basic results: 

===>  ML Results  <===

Validation Set Scores
Accuracy: 0.630022 (+/- stdev 0.002706)
F1: 0.691701 (+/- stdev 0.000526)
AUC-ROC: 0.716732 (+/- stdev 0.003795)
AUC-PRC: 0.700005 (+/- stdev 0.004185)


Test Set Scores:
Precision: 0.587838
Accuracy: 0.625596
F1: 0.691830
AUC-ROC: 0.724464 (+/- stdev 0.006149)
AUC-PRC: 0.614855 (+/- stdev 0.007466)
finished!



