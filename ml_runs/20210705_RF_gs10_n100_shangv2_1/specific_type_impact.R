

###############################################################################

# Given the high performance of this model, 

# is this caused by 

# Simple check; are eQTLs in the test set, which their eGenes is also in the training set

# in another eQTL

# better / differently scored than those without their eGenes in the training set?


###########################################################################

library(dplyr)
library(ggplot2)
library(patchwork)
library(RColorBrewer)
library(magrittr)

# ggplot theme

custom_theme <-
  list(
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        text = element_text(size = 9),
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
      )
  )

################################################################################

# Read in scores talbe

scores = read.table("./ml_runs/05072021_RF_gs10_n100_shangv2_1/metab_RF_scores.txt",
                    sep = "\t",
                    header = TRUE)


# Result of :

full_m.1by1 <- readRDS("./output/mashr_results/Shang_et_al_2020/full_m.1by1.rds")


EA_spec = intersect(scores %>% filter(Class == "specific") %$% ID, 
                        mashr::get_significant_results(full_m.1by1, thresh = 0.01, conditions = 1) %>% names())


AA_spec = intersect(scores %>% filter(Class == "specific") %$% ID, 
                        mashr::get_significant_results(full_m.1by1, thresh = 0.01, conditions = 2) %>% names())


p1 = scores %>% 
  filter((ID %in% EA_spec)) %>% 
  ggplot() + 
  geom_density(aes(x=Mean, fill = Class)) + 
  custom_theme +    
  ylim(0,6) + 
  scale_fill_brewer(palette="Dark2") + 
  labs(title = "European Specific")

p2 = scores %>% 
  filter((ID %in% AA_spec)) %>% 
  ggplot() + 
  geom_density(aes(x=Mean, fill = Class)) + 
  custom_theme +    
  ylim(0,6) + 
  scale_fill_brewer(palette="Dark2") + 
  labs(title = "African American Specific")

p1 + p2
