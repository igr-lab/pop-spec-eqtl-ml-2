
# Second test

- difference is lsfr thresholds 0.05 / 0.5
-
- inclusion of more gnomAD constraint scores
- 

`r

shang_df_pop_spec = call_pop_specific(shang_df,
                                      snp,
                                      gene,
                                      method = "mashr_significance",
                                      sig_thresh = 0.01,
                                      sig_thresh_pop_2 = 0.4,
                                      mashr_obj = full_m.1by1)


# Then extracted features on a subset of gene / snp pairs using function 

shang_test = extract_features(shang_df_pop_spec, 
                                     snp, 
                                     snp_features = c("CADD",  # What features should be extracted / obtained
                                                      "linsight", 
                                                      "fitCons", 
                                                      "dis_to_TSS", 
                                                      "win_GC_content"),
                                     gene, 
                                     window_size = c(5,20, 50, 100, 250, 1000),
                                     combined_features = NULL)



`