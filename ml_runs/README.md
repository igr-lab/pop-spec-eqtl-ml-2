
# ml_runs folder

Folder subfolders with models, testing results, plots and information on how each model was produced (.slurm / README.md). A summary table of each run can be found [here](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/summary_details_ml_runs.html). Data used to develop models is found in the ```output/ml_data``` folder (of the main directory). 


## General rules

Below lists the naming structure and explains some parts of subfolder names in the ml_runs folder. This should help you deduce how each machine learning training and testing run was conducted. For more information see the **Folder descriptions** table below, and the contents of each subfolder. 

**Naming structure**: 

*<- <span style="color:grey"> Date Completed or Abandoned in the case of failed run </span> ->* _ *<- <span style="color:grey"> Algorithm </span> ->* _ **gs** *<- <span style="color:grey"> Number of grid search iterations </span>->* _ **n** *<- <span style="color:grey"> Number of replicates of balanced cross-validation </span> ->* _ *<- <span style="color:grey"> Dataset </span>  ->* _ *<- <span style="color:grey"> Any other relevant / important information </span> ->*

For example: **20210601_RF_gs0_n10_shangv2** contains information about a model which finished building on the 01/06/2021 using the Random Forest Algorithm, 0 grid searches (used the default parameters), 10 replicates of balanced cross validation and was built using the shangv2 dataset (shang_ml_test_v2.txt, created from the Shang and Smith 2020 dataset).

**Some parts of folder names explaination**: 



## Folder descriptions

The following table is designed to explain the details of each run (listed by subfolder). This table was started on 2022/01/16 so is less complete for runs completed before this date. 

| Subfolder name: | Description: | Reason for conducting this run: | Any known issues / mistakes: | 
| --- | --- | --- | --- | 
| 20210701_RF_gs0_n19_shangv1 | | | | 
| 29219703_RF_gs0_n19_shangv2_1 | |
| 20210704_RF_gs10_n100_shangv2_1 | | 
| 2021
