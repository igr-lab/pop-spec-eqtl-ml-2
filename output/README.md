
# output

This folder contains data processed in some way. Raw data is in the data folder. 


# Sketch of data pipeline (illustrates how data moves through the different subfolders)

![Outline of data pipeline](data_pipeline_figure.png)

For full pipeline see [workflowr site](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/pipeline_overview.html)

# Subfolders: 

| Subfolder | Description |
| --- | --- |
| classified_eqtls | Datasets of eQTLs  and summary statistics from a single population. Contains a 'Class' column which identifies the eQTL as being shared or specific. Essentially, datasets from preclassified_eqtls which have filtered significant results, and split these into shared or specific classifications using relevant mashr results. |
| combined_eqtl_summary_stats | Datasets which are a result of combining multiple populations and /or multiple studies in the same tissue into a single dataframe. | 
| filtered_encode_dnase_motif | .bed files  of ENCODE DNAse I footprint, filtered to have hint score > 200 (recommended threshold by paper, [ENCODE, Funk et al 2020](https://www.cell.com/cell-reports/pdf/S2211-1247(20)31014-7.pdf)) | 
| gene_function_or_ontology_lists | User curated lists of GO terms or genes with particular functions used to annotate datasets. |
| gtex_expression | Summary statistics of gene expression in GTEx - created from raw files in /data/gtex_resources | 
| gtex_pb_eqtls | Sets of 'population biased' eQTLs, motified into a useful format for combining with features in the current pipeline.| 
| liftover_encode_dnase_motif | .bed files of ENCODE DNAse I footprints - converted to hg19 from hg38. | 
|lsfr_threshold | Datasets contain information about the number of eQTLs called population specific or shared at different lsfr thresholds. | 
| mashr_input | Matrices of standard errors and betas to provide mashr. | 
| mashr_results | Results (created models, and posterior distributions of effect sizes) from applying mashr. | 
| ml_data | Folder of datasets ready to be used to either train and test machine learning models on, or to use with tested machine learning models. These should be .txt, and sep  = " &#92; t " | 
| preclassified_eqtls | Datasets of eQTLs and required summary statistics, taken from combined_eqtl_summary_stats.These should be datasets which are ready to have features added to them using the feature extraction step. At a minimum they should have a column identifying the snp, and another column identifying the gene. Ideally they should also have column for the effect size and standard error from a single population. Essentially these are the bare bones of the datasets that will eventually be used to train the machine learning models. | 
