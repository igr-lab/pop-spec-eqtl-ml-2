---
title: "Initial mashr testing"
output:
  workflowr::wflow_html:
    toc: true
editor_options:
  chunk_output_type: console
---

## Intention : Mess around with mashr object for the eQTL dataset / mashr package

This mashr object is from Natri et al. 2020 (preprint) / ? 2021, it is the result of performing mashr on a random subset of 100,000 SNP / Gene pairs 

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Set up

```{r setup_true, message=FALSE,warning=FALSE}

# Required packages
library(mashr)
library(dplyr)
library(tidyr)
library(ggplot2)
library(tibble)
library(ggpubr)
library(circlize)
library(ComplexHeatmap)
library(viridis)

# load mashr R object for Indonesian eQTL dataset 

mashr_indo = readRDS("data/indo_testing_data/mashr_canoncovs_datacovs.rds")

# Set up custom theme for ggplot

custom_theme <-
  list(
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        text = element_text(size = 9),
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
      )
  )


```

# 1. Local false sign rate

"analogous to a false discovery rate, but more stringent because it requires true discoveries to be not only nonzero, but also correctly signed. " Urbut et al 2019 mashr paper

```{r lsfr}
lfsr = get_lfsr(mashr_indo) 

lfsr %>% 
  head

# Look at the distribution of local false sign rate (lfsr)

# Convert matrix to tidy tibble format to plot

lfsr_df  = lfsr %>% 
  as_tibble(rownames = "SNP") %>%
  pivot_longer(!SNP, 
               names_to = "Dataset",
              values_to = "lfsr") 


# Calculate quartiles / percentiles of lfsr for each dataset

lfsr_df %>% 
  group_by(Dataset) %>% 
  summarise(mean  = mean(lfsr),
            quartile_1 = quantile(lfsr, probs = 0.25), #1st quartile of local false sign rate (lfsr)
            median = median(lfsr), # median
            quartile_3 = quantile(lfsr, probs = 0.75), #3rd quartile of lfsr
            percentile_95 = quantile(lfsr, probs = 0.95),  #  95% percentile of lfsr
            percentile_95 = quantile(lfsr, probs = 0.99)) #99% percentile of lfsr

# Note: 
# All distributions are left skewed, (i.e. median > mean)
# gtex, indonesian and twinsuk have similar means, medians, percentiles etc.
# for example median: gtex (0.773), indonesian (0.783), and twinsuk (0.781)
# lepik dataset has lower percentile / and mean lfsr values

# Create boxplot of lfsr to compare each dataset 

lfsr_df %>% 
  ggplot() + 
  geom_boxplot(aes(x=lfsr),
               fill = "dodgerblue") + 
  facet_wrap(~Dataset, ncol = 1) + 
  custom_theme + 
  labs(x = "Local False Sign Rate")


# Plot the distribution of false sign rate, for all datasets

  lfsr_df %>% 
  ggplot() + 
  geom_histogram(aes(x=lfsr),
                 colour = "black",
                 fill = "dodgerblue") + 
  custom_theme + 
    labs(x = "Local False Sign Rate")

  
# Compare distribution of local false sign rate between different datasets  
# Each plot is a different dataset
    
  lfsr_df %>% 
    ggplot() + 
    geom_histogram(aes(x=lfsr),
                   colour = "black",
                   fill = "dodgerblue") + 
    custom_theme + 
    facet_wrap(~Dataset, ncol = 1) + 
    labs(x = "Local False Sign Rate")

# Compare distribution of local false sign rate <0.05 different datasets      
  
  lfsr_df %>% 
    filter(lfsr<0.05) %>% 
    ggplot() + 
    geom_histogram(aes(x=lfsr),
                   bins = 50,
                   colour = "black",
                   fill = "dodgerblue") + 
    custom_theme + 
    facet_wrap(~Dataset, ncol =1) + 
    labs(x = "Local False Sign Rate")
  
  # Number in each datset with an lfsr < 0.05
  
  lfsr_df %>% 
    filter(lfsr<0.05) %>% 
           group_by(Dataset) %>%  
            summarise(n = n())

  
  # Number in each datset with an lfsr < 0.25 
  
  lfsr_df %>% 
    filter(lfsr<0.25) %>% 
    group_by(Dataset) %>%  
    summarise(n = n())
  
  # Number in each datset with an lfsr < 0.01 
  
  lfsr_df %>% 
    filter(lfsr<0.01) %>% 
    group_by(Dataset) %>%  
    summarise(n = n())

  # Number in each datset with an lfsr = 0
  
  lfsr_df %>% 
    filter(lfsr== 0) %>% 
    group_by(Dataset) %>%  
    summarise(n = n())
  
  # Note: gtex and twinsuk have comparable numbers
  # lepik has more (explaining the lower mean / median / percentiles)
  # indonesian dataset has fewer 

  
  
  
# # # # # # # # # # # # # # # # # # #
  


```


# 2. Local false discovery rate

```{r lfdr}

lfdr = get_lfdr(mashr_indo)


lfdr_df = lfdr %>% 
  as_tibble(rownames = "SNP") %>%
  pivot_longer(!SNP, 
               names_to = "Dataset",
               values_to = "lfdr") 

  lfdr_df %>% 
  ggplot() + 
  geom_histogram(aes(x=lfdr),
                 colour = "black",
                 fill = "dodgerblue") + 
  custom_theme 

  
# Compare the distribution of local false discovery rate with local false sign rate
  
ggarrange(lfdr_df %>% 
            ggplot() + 
            geom_histogram(aes(x=lfdr),
                           colour = "black",
                           fill = "dodgerblue") + 
            custom_theme + 
            ylim(0, 80000) + 
            labs(x = "Local False Discovery Rate"),
          lfsr_df %>% 
            ggplot() + 
            geom_histogram(aes(x=lfsr),
                           colour = "black",
                           fill = "dodgerblue") + 
            ylim(0, 80000) + 
            labs(x = "Local False Sign Rate") + 
            custom_theme,
            nrow= 2)

# Note: Local False Sign Rate is more stringent than 
# local fdr - so the fact that the distribution of lfsr is more left skewed 
# is to be expected


# Plot all combinations of dataset and either local false discovery rate, or local false sign rate
# To compare the distribution of local false discovery rate with local false sign rate


ggarrange(lfdr_df %>% 
            ggplot() + 
            geom_histogram(aes(x=lfdr),
                           colour = "black",
                           fill = "dodgerblue") + 
            custom_theme + 
            ylim(0, 30000) + # keep y limits the same for comparasion 
            labs(x = "Local False Discovery Rate") + 
            facet_wrap(~Dataset, nrow = 1),
          lfsr_df %>% 
            ggplot() + 
            geom_histogram(aes(x=lfsr),
                           colour = "black",
                           fill = "dodgerblue") + 
            ylim(0, 30000) + 
            labs(x = "Local False Sign Rate") + 
            custom_theme + 
            facet_wrap(~Dataset, nrow = 1),
          nrow= 2)


# Similar plot as above, but
# Keep lfdr and lfsr on same plot, but different colours
# To compare the distribution of local false discovery rate with local false sign rate


inner_join(lfdr_df, 
           lfsr_df) %>% 
  ggplot() + 
  geom_histogram(aes(x=lfdr), fill = "dodgerblue", colour = "black") + 
  geom_histogram(aes(x=lfsr), fill = "red", colour = "black") + 
  facet_wrap(~Dataset, nrow = 1) + 
  custom_theme + 
  labs(x = "Blue = lfdr, Red = lfsr")


# Compare at specific regions of the distribution

inner_join(lfdr_df, 
           lfsr_df) %>% 
  filter(lfdr > 0.75 & lfsr > 0.75) %>% 
  ggplot() + 
  geom_histogram(aes(x=lfdr), fill = "dodgerblue", colour = "black") + 
  geom_histogram(aes(x=lfsr), fill = "red", colour = "black") + 
  custom_theme


inner_join(lfdr_df, 
           lfsr_df) %>% 
  filter(lfdr > 0.8 & lfsr > 0.8) %>% 
  ggplot() + 
  geom_histogram(aes(x=lfdr), fill = "dodgerblue", colour = "black") + 
  geom_histogram(aes(x=lfsr), fill = "red", colour = "black") + 
  custom_theme

inner_join(lfdr_df, 
           lfsr_df) %>% 
  filter(lfdr < 0.10 & lfsr > 0.10) %>% 
  ggplot() + 
  geom_histogram(aes(x=lfdr), fill = "dodgerblue", colour = "black") + 
  geom_histogram(aes(x=lfsr), fill = "red", colour = "black") + 
  custom_theme



# # # # # # # # # # # # # # # # # # # #
```

# 3. Posterior Mean 

```{r pd}


  pm = get_pm(mashr_indo) 

  # Convert matrix to tidy tibble format to plot
  # so instead of having a 
  
  pm_df  = get_pm(mashr_indo) %>% 
  as_tibble(rownames = "SNP") %>% 
  pivot_longer(!SNP,
               names_to = "dataset", 
               values_to = "posterior_mean")
    

  pm_df %>% 
  ggplot(
         aes(x=posterior_mean) 
                               ) + 
  geom_histogram(bins = 100, 
                 fill = "dodgerblue",
                 colour = "black") + 
  custom_theme
    

    pm_df %>% 
    filter(posterior_mean<1*-1) %>%  
    ggplot(
            aes(x=posterior_mean)
                                  ) + 
                        geom_histogram(bins = 100, 
                                       fill = "dodgerblue",
                                       colour = "black") + 
                                                  custom_theme

# Plot progressively tighter intervals around the center (0)
# to investigate the distribution of posterior means around the centre
        
    ggarrange(
    pm_df %>% 
      filter(posterior_mean>1*-1&posterior_mean<1) %>% 
      ggplot(
        aes(x=posterior_mean)
      ) + 
      geom_histogram(bins = 100, 
                     fill = "dodgerblue",
                     colour = "black") + 
      custom_theme,
    pm_df %>% 
      filter(posterior_mean>0.25*-1&posterior_mean<0.25) %>% 
      ggplot(
        aes(x=posterior_mean)
      ) + 
      geom_histogram(bins = 100, 
                     fill = "dodgerblue",
                     colour = "black") + 
      custom_theme, 
    pm_df %>% 
      filter(posterior_mean>0.1*-1&posterior_mean<0.1) %>% 
      ggplot(
        aes(x=posterior_mean)
      ) + 
      geom_histogram(bins = 100, 
                     fill = "dodgerblue",
                     colour = "black") + 
      custom_theme,
    pm_df %>% 
      filter(posterior_mean>0.025*-1&posterior_mean<0.025) %>% 
      ggplot(
        aes(x=posterior_mean)
      ) + 
      geom_histogram(bins = 100, 
                     fill = "dodgerblue",
                     colour = "black") + 
      custom_theme,
     nrow = 2,
     ncol = 2) 
```

# 4. Posterior standard deviaton

```{r posterior_sd}

psd = get_psd(mashr_indo)

```

# 5. Negative probability

```{r neg_prob}



# Negative probability 

get_np(mashr_indo)  %>% 
  as_tibble(rownames = "SNP") %>% 
  pivot_longer(!SNP,
               names_to = "dataset", 
               values_to = "negative_prob") %>% 
  ggplot(aes(x=negative_prob)) + 
  geom_histogram(bins = 100) + 
  custom_theme

```

# 6.log likelihood

```{r log_like}

get_loglik(mashr_indo)


```

# 7. Get significant results

```{r sig_results}
# Significant results


# Number of significant results
get_significant_results(mashr_indo) %>% 
                                        length()


get_significant_results(mashr_indo, conditions = 1)[1:10]

# the significance function used to extract significance from mash object; 
#eg could be ashr::get_lfsr or ashr::get_lfdr. (Small values must indicate significant.)

# Find number of significant results for each condiditon (dataset)
# indo = 1, gtex = 2, lepik = 3, twinsuk = 4

# indo          gtex         lepik       twinsuk

get_significant_results(mashr_indo, 
                           conditions=1) %>% 
  length()



# # # # # # # # # # # # # # # # # # # #


```

# 8. Pairwise sharing

```{r pairwise_sharing, fig.width=12}

#  Sharing of significant signals among each pair of conditions

# # # # # # # # # # # # # # # # # # # #



# default definition of shared is "the same sign and within a factor 0.5 of each other".
get_pairwise_sharing(mashr_indo)

col_fun = colorRamp2(seq(from = 0.5, to = 1, by = 0.125), 
                                             plasma(n = 5, direction = 1))

Heatmap(mat = get_pairwise_sharing(mashr_indo), 
        col = col_fun,
              clustering_distance_rows = function(m) dist(m))

# by setting the factor to be 0 you assess only if they are the same sign

get_pairwise_sharing(mashr_indo, factor = 0) 

# Note: gtex, lepik, twinsuk share ~98-99% of the same sign
# indo shares ~93% with other datasets


# Ignore sign - just consider effect size
get_pairwise_sharing(mashr_indo, factor = 0.5, FUN = abs) 


get_pairwise_sharing(mashr_indo, factor = 1)  # Everything is zero


get_pairwise_sharing(mashr_indo, factor = 0.9)  # Lower level of sharing



# Plot heatmaps for different levels of sharing 

# Set all heatmaps to have the same colour mapping legend
col_fun = colorRamp2(seq(from = 0, to = 1, by = 0.1), plasma(n = 11, direction = -1))


heat_map_list = Heatmap(mat = get_pairwise_sharing(mashr_indo, 
                                              factor = 0.9),  # ? the same sign and within a factor of 0.9 of each other?
        col = col_fun,
        name = "Pairwise Sharing",
        column_title = "Factor = 0.9",
        clustering_distance_rows = function(m) dist(m)) + 
  Heatmap(mat = get_pairwise_sharing(mashr_indo, 
                                     factor = 0.5),  # the same sign and within a factor 0.5 of each other
          col = col_fun,
          show_heatmap_legend = FALSE,
          column_title = "Factor = 0.5",
          clustering_distance_rows = function(m) dist(m)) + 
  Heatmap(mat = get_pairwise_sharing(mashr_indo, 
                                     factor = 0.25),  # ? the same sign and within a factor 0.25 of each other
          col = col_fun,
          show_heatmap_legend = FALSE,
          column_title = "Factor = 0.25",
          clustering_distance_rows = function(m) dist(m)) + 
  Heatmap(mat = get_pairwise_sharing(mashr_indo,  # asses only if they are the same sign
                                     factor = 0),  
          col = col_fun,
          show_heatmap_legend = FALSE,
          column_title = "Factor = 0 (Testing same sign only)",
          clustering_distance_rows = function(m) dist(m))   

draw(heat_map_list, column_title = "Pairwise sharing (same size and within a factor of ...")








```

# 9. Mixture proportions 

```{r mixture_prop}

# Mixture proportions

# # # # # # # # # # # # # # # # # # # #

# Get the estimated mixture proportions 

get_estimated_pi(mashr_indo) %>% 
  sort(decreasing = F)

# As expected null results make up the largest mixture proportion ~45%

# Followed by equal effects ~24%

# 

# Indonesian ~9%


# Plot the mixture proportions 

get_estimated_pi(mashr_indo) %>% 
  as_tibble(rownames = "mix_name") %>% 
  rename(pi = value) %>% 
  ggplot(aes(x=mix_name, y= pi)) + 
  geom_col(fill = "dodgerblue", colour = "black") + 
  custom_theme





```