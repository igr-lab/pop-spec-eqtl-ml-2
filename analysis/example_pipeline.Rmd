---
title: "Example of running the pipeline on sample data - demonstrating the steps in the population-specific eQTL machine learning pipeline"
site: workflowr::wflow_site
output:
    workflowr::wflow_html:
editor_options:
  chunk_output_type: console
---

This notebook runs through the general steps in our pipeline using sample data from the ```test_data``` folder. The pipeline illustrated here starts from 3 files built after combining summary statistics from multiple studies (described under the requirements subheading below) and ends with a producing graphs on built and tested machine learning model (models designed to predict whether or not an eQTL from one population is transferable to another population). 

The steps in this pipeline include: running mashr, calling eQTLs population specific or shared, extracting eQTL features, splitting eQTLs into training and testing sets, training machine learning models, testing machine learning models - and plotting of results from machine learning models. 

The point of this notebook is to provide an example of how the pipeline works on a sample dataset so it can be used and understood by others. Ideally, it should explain each step and help demystify the command line scripts in the pipeline. Additionally, this notebook should explain the usage of each of the command line scripts. 

For a visual overview of the way data moves through the pipeline and the folders in this repository see this figure [here](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/pipeline_overview.html). 

## Requirements

** This section (requirements) needs updating and expanding. It's missing many of the important packages!!

For a full set of package requirements, see the relevant scripts (they should all list the required packages at the top of the scripts, or if it is a script of function at the top of each function in the script). As an general overview the pipeline relies on:

**Python requirements:**

- python (pipeline tested using 3.7.4)
- sys, os, time, argparse
- pandas
- numpy
- sklearn
- joblib, random, math
- lofo-importance

**R requirements:**

- r (tested using 4.0.0)
- mashr
- data.table
- dplyr, tidyr, tibble, dtplyr, tictoc
- biomaRt, GenomicDistributions, GenomicRanges

**Data / file inputs:**

- Matrix of eQTL effect sizes 
- Matrix of eQTL standard errors 
- Data table with at a minimum a single column identifying the rsid and ensembl gene id in the form rsid_ensmeblID. Having other columns in this data table is fine - but keep in mind that if you follow the below pipeline exactly this table will form the basis of the data table used to build the machine learning table - and each column here (that's not the ID column) will be interpreted as a feature! 

**Other**

For the feature extraction step (see step 3 below), the pipeline additionally requires many of the subfolders in the ```data``` and the ```output``` folder. 

# Step 0. Combine summary statistics from mulitiple eQTL studies

The column and row labels **and order** should be consistent between the two matrices (essentially these . The row IDs in the matrices should be in the same form 

```{r prepared_data, eval = FALSE}

# Load example matrices
beta_matrix = read.table("test_data/natri_example_beta_matrix.csv")
se_matrix = read.table("test_data/natri_example_beta_matrix.csv")

# See structure of example matrices
head(beta_matrix)
head(se_matrix)

dim(beta_matrix)
dim(se_matrix) 

```

# Step 1. Run mashr

```{bash, eval = FALSE}

echo "Natri mashr"

Rscript --vanilla ./code/mashr/mashr_pipeline.R -b natri_example_beta_matrix.csv \ #the matrix of effect sizes
                                                -e natri_example_se_matrix.csv \
                                                -s natri_example_mashr \
                                                -o ./test_data \ #./output/mashr_results \
                                                -i ./test_data \ #./output/mashr_input \
                                                -p 0.01 \ # what lsfr threshold should we use to call eQTLs
                                                -l 0.05 \ 
                                                -c 4 \ #how many PCA components - default is just the maximum possible
                                                -a TRUE #run for the entire dataset - not just a sample
```

# Step 2. Call eQTL population-specific or shared

```{bash, eval = FALSE}

Rscript --vanilla ./code/calling_pop_spec_eqtl/call_pop_spec_mashr_sig.R -r ./test_data/natri_mashr_results.rds \
                                                                                        --lfsr_p1 0.01 \
                                                                                        --lfsr_p2 0.1 \
                                                                                        --p1 indo \
                                                                                        --p2 lepik,gtex,twinsuk \
                                                                                        --class shared,indo-specific \
                                                                                        -i ./test_data/natri_indo_example_preclass.csv \ #output/preclassified_eqtls
                                                                                        --save ./test_data/indo_spec_eqtl_natri_example_lfsr0.01.csv #output/classified_eqtls/



```


# Step 3. Extract Features of eQTLs 

```{bash, eval = FALSE}

Rscript --vanilla code/feature_extraction/feature_extraction_optparser.R -t wb \
                                                                         --thous_genome_pops AF,EUR_AF,AFR_AF,AMR_AF \
                                                                         --gnomad_pops AF,AF_afr,AF_amr,AF_nfe,AF_popmax \
                                                                         -d ./test_data/indo_spec_eqtl_natri_example_lfsr0.01.csv  \
                                                                         --save natri_indo_lfsr0.01   
                                               
```


# Step 4. Prepare eQTLs for machine learning pipeline

## 4.0 Preprocessing / one-hot-encoding

```{bash, eval = FALSE}

echo "Preprocessing"

python3 ./code/updated_ml_pipeline/ML_preprocess.py \
        -df ./output/ml_data/natri_indo_lfsr0.01_strict_shared_features_df.txt \
        -na_method median \
        -save ./ml_runs/TBC_RF_gs0_n1_natri_indo_all_lfsr0.01_by_chr_ss_lofo_imp/natri_ml_test_mod.txt

```

##  4.1 Filter out features which are too rare 

```{bash, eval = FALSE}

echo "Filtering out rare features"

Rscript --vanilla ./code/updated_ml_pipeline/ML_preprocess.py \
        -df ./output/ml_data/natri_indo_lfsr0.01_strict_shared_features_df.txt \
        -na_method median \
        -save ./ml_runs/TBC_RF_gs0_n1_natri_indo_all_lfsr0.01_by_chr_ss_lofo_imp/natri_ml_test_mod.txt

```

##  4.2 Split eQTLs into train and test sets 

```{bash, eval = FALSE}
        
echo "Sampling by chromosome"

Rscript --vanilla ./code/updated_ml_pipeline/split_test_train_chromosome.R \
                  -d /natri_ml_test_mod.txt  \ #./ml_runs/TBC_RF_gs0_n1_natri_indo_all_lfsr0.01_by_chr_ss_lofo_imp
                  -n 2  \
                  -s ./ml_runs/TBC_RF_gs0_n1_natri_indo_all_lfsr0.01_by_chr_ss_lofo_imp/test_eqtls.txt

```

##  4.3 Feature selection  

## 4.4 Train and test machine learning model

```{bash, eval = FALSE}

python3 ./code/updated_ml_pipeline/Feature_Selection.py \
         -df  ./code/ML-Pipeline/Workshop/data_mod.txt \
         -test ./code/ML-Pipeline/Workshop/test_genes.txt \
         -cl_train special,gen \
         -type c  \
         -alg lasso  \
         -p 0.02 \
         -save ./code/ML-Pipeline/Workshop/top_feat_lasso.txt
         
python3  ./code/updated_ml_pipeline/ML_classification.py \
        -df ./code/ML-Pipeline/Workshop/data_mod.txt \
        -test ./code/ML-Pipeline/Workshop/test_genes.txt \
        -cl_train special,gen \
        -alg RF \
        -gs F\
        -max_depth 10 \
        -n_estimators 1000 \
        -max_features 0.5 \
        -n 1 \
        -n_jobs 1 \
        -plots T \
        -cv_num 5 \
        -feat  ./code/ML-Pipeline/Workshop/top_feat_lasso.txt \
        -save ./code/ML-Pipeline/Workshop/test_data_lofo \
        -imp_method lofo         
```