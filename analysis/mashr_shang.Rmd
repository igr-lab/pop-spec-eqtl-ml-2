---
title: "Shang mashr analysis"
author: "Isobel Beasley"
date: "23/08/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Running mashr on Shang and Smith et al. 2020

# Set up

```{r, message=FALSE, warning=FALSE}

library(dplyr)
library(mashr)
library(data.table)
library(tidyr)
library(tictoc)
library(flashr)

# Combined dataset of shang et al. 
# includes eQTLs which were avaliable in summary stats from both populations 
# (aa = african american, ea = european american)
# (meaning MAF >0.01 in both populations)

# shang and smith combined - african american + european american

shang_only_df = data.table::fread("output/combined_eqtl_summary_stats/shang_combined_only.csv")


##################################################################

# shang with combined studies 

shang_lcl_df = data.table::fread("output/combined_eqtl_summary_stats/shang_gtex_geuvadis_twinsuk_combined.csv")

```

# Running mashr on Shang and Smith et al. 2020 (excluding relevant datasets, e.g. GTEx, )

## No correction (basically the equivalent of running ashr)


```{r}

# for only shang and smith et al 2020
# Complete dataset of effect sizes (betas), standard errors (shats)

bhat = shang_only_df %>%
  select(gene, snp, ea_beta, aa_beta) %>%
  tidyr::unite("gene_snp", gene:snp) %>%
  arrange(gene_snp) %>%
  tibble::column_to_rownames(var = "gene_snp") %>%
  base::as.matrix()

shat = shang_only_df %>%
  select(gene, snp, ea_se, aa_se) %>%
  tidyr::unite("gene_snp", gene:snp) %>%
  arrange(gene_snp) %>%
  tibble::column_to_rownames(var = "gene_snp") %>%
  base::as.matrix()

# Full m.1by1 - no correction

mashr_data = mash_set_data(bhat,shat)

tic("Mashr full dataset m.1by1")
full_m.1by1 = mash_1by1(mashr_data) # Full dataset
toc()

saveRDS(full_m.1by1,
        "output/mashr_results/shang_only_no_corr_full_m.1by1.rds")


```

## Adding corrections

```{r adding_cor}

# Identify a strong subset of tests

strong.subset = get_significant_results(full_m.1by1,0.05)

# Identify a random subset of tests

set.seed(105)

random.subset = sample.int(nrow(bhat), size = 0.1*nrow(bhat))

################################################################################

# Using random sample of 10% of gene pairs to get variance matrices


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# take random sample of snp / gene pairs

rows = sample.int(nrow(bhat), size = 0.1*nrow(bhat))

data.temp = mash_set_data(bhat[rows,],shat[rows,])

Vhat = estimate_null_correlation_simple(data.temp)

rm(data.temp)



```

```{r test_1}

data.strong = mash_set_data(bhat[strong.subset,], shat[strong.subset,], V=Vhat)

```


```{r test}

data.random = mash_set_data(bhat[random.subset,], shat[random.subset,], V = Vhat)

```

````{r flashr}
################################################################################

# Data driven Empirical Bayes matrix factorization for data driven prior
# following proceedure as per https://stephenslab.github.io/mashr/articles/mash_sampling.html

tic("Finalise Covariances")

#
U.f = cov_flash(data.strong,
                factors="nonneg",
                tag="non_neg",
                var_type="constant")

U.pca = cov_pca(data.strong, 2)

U.ed = cov_ed(data.strong,  c(U.f, U.pca))

U.c = cov_canonical(data.random)

U = list(PCA = U.pca, ED = U.ed, Can = U.c)

toc()

# Fit mash model (estimate mixture proportions)

```

```{r fit_mashr_model}
tic("Fit mash model (estimate mixture proportions)")

mash_model = mash(data.random, Ulist = c(U.ed,U.c), outputlevel = 1)

toc()

saveRDS(mash_model,
        "output/mashr_results/shang_only_mashr_fit_model.rds")

```

```{r posterior_summary}
# Compute posterior summaries (for strong subset)

tic("Compute posterior summaries (for strong subset)")

m2 = mash(data.strong, 
          g=get_fitted_g(mash_model), 
          fixg=TRUE)

tic()

saveRDS(m2,
        "output/mashr_results/shang_only_strong_mashr.rds")
```

```{r posterior_summarise_all}
#  Compute posterior summaries full dataset

# tic("Compute posterior summaries full dataset")
# 
# mashr_data = mash_set_data(bhat,shat,Vhat)
# 
# m3 = mash(mashr_data,
#           g=get_fitted_g(mash_model),
#           fixg=TRUE)
# 
# tic()
# 
# saveRDS(m3,
#         "output/mashr_results/shang_only_all_mashr.rds")
# 
# # Compute posterior summaries for mostly strong subset (relax threshold a little to get more results)
# 
# tic("Compute posterior summaries mostly strong subset")
# 
# 
# mostly.strong.subset = get_significant_results(full_m.1by1, 
#                                                     thresh = 0.20)
# 
# mostly.data.strong = mash_set_data(bhat[mostly.strong.subset,],
#                                    shat[mostly.strong.subset,],
#                                    V=Vhat)
# 
# m4 = mash(mostly.data.strong, 
#           g=get_fitted_g(m), 
#           algorithm.version = 'R', 
#           posterior_samples = 100,
#           fixg=TRUE)
# 
# toc()
# 
# saveRDS(m4,
#         "output/mashr_results/shang_only_mostly_strong_mashr_posterior_samples.rds")
# 
# ##############################################################################



```

# Including other studies (GTEx, GEUVADIS, TwinsUK)

```{r}

full_bhat = shang_lcl_df %>%
  select(gene,
         snp, 
         ea_beta, 
         aa_beta,
         gtex_beta,
         twinsuk_beta,
         geuvadis_beta) %>%
  tidyr::unite("gene_snp", gene:snp) %>%
  group_by(gene_snp) %>% 
  dplyr::sample_n(1) %>% 
  ungroup() %>% 
  arrange(gene_snp) %>%
  tibble::column_to_rownames(var = "gene_snp") %>%
  base::as.matrix()

full_shat = shang_lcl_df %>%
  select(gene, 
         snp, 
         ea_se, 
         aa_se,
         gtex_se,
         twinsuk_se,
         geuvadis_se) %>%
  tidyr::unite("gene_snp", gene:snp) %>%
  group_by(gene_snp) %>% 
  dplyr::sample_n(1) %>% 
  ungroup() %>% 
  arrange(gene_snp) %>%
  tibble::column_to_rownames(var = "gene_snp") %>%
  base::as.matrix()

mashr_data = mash_set_data(full_bhat,
                           full_shat)


tic("Mashr full dataset m.1by1")
full_combined_m.1by1 = mash_1by1(mashr_data) # Full dataset
toc()


strong.subset = get_significant_results(full_combined_m.1by1,0.1)

# Identify a random subset of tests

set.seed(105)

random.subset = sample.int(nrow(full_bhat), size = 0.1*nrow(full_bhat))

################################################################################

# Using random sample of 10% of gene pairs to get variance matrices


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# take random sample of snp / gene pairs

rows = sample.int(nrow(full_bhat), size = 0.1*nrow(full_bhat))

data.temp = mash_set_data(full_bhat[rows,],
                          full_shat[rows,])

Vhat = estimate_null_correlation_simple(data.temp)

rm(data.temp)



data.strong = mash_set_data(full_bhat[strong.subset,], 
                            full_shat[strong.subset,], 
                            V=Vhat)


data.random = mash_set_data(full_bhat[random.subset,], 
                            full_shat[random.subset,], 
                            V = Vhat)

tic("Finalise Covariances")

#
U.f = cov_flash(data.strong,
                factors="nonneg",
                tag="non_neg",
                var_type="constant")

U.pca = cov_pca(data.strong, 5)

U.ed = cov_ed(data.strong,  c(U.f, U.pca))

U.c = cov_canonical(data.random)

U = list(PCA = U.pca, ED = U.ed, Can = U.c)

toc()

tic("Fit mash model (estimate mixture proportions)")

mash_model = mash(data.random, Ulist = c(U.ed,U.c), outputlevel = 1)

toc()

saveRDS(mash_model,
        "output/mashr_results/shang_combined_mashr_fit_model.rds")



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# take random sample of snp / gene pairs

set.seed(406)

rows = sample.int(nrow(full_bhat), 
                  size = 0.1*nrow(full_bhat))

sam_bhat = bhat[rows, ]

sam_shat = shat[rows,]

data.random = mash_set_data(sam_bhat, 
                                     sam_shat)

Vhat = estimate_null_correlation_simple(data.random)

data.random = mash_set_data(sam_bhat, 
                            sam_shat,
                            V=Vhat)

strong.subset = get_significant_results(full_m.1by1, 
                                                    0.05)

data.strong = mash_set_data(bhat[strong.subset,],
                            shat[strong.subset,],
                            V=Vhat)
#
U.f = cov_flash(data.strong,
                factors="nonneg",
                tag="non_neg",
                var_type="constant")

U.pca = cov_pca(data.strong, 
                4)

U.ed = cov_ed(data.strong, 
              c(U.f, U.pca))



U.c = cov_canonical(data.random)

U = list(PCA = U.pca, ED = U.ed, Can = U.c)

tic("Fit mash model (estimate mixture proportions)")

m = mash(data.random, 
         Ulist = c(U.ed,U.c), 
         outputlevel = 1)

toc()

# Compute posterior summaries (for strong subset)

tic("Compute posterior summaries (for strong subset)")

m2 = mash(data.strong, 
          g=get_fitted_g(m), 
          fixg=TRUE)

tic()

saveRDS(m2,
        "output/mashr_results/shang_all_strong_mashr.rds")
```