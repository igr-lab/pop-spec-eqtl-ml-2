---
title: "Combine Mogil with other microarray eQTL studies"
output:
  workflowr::wflow_html:
    toc: true
    code_folding: show
editor_options:
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

<div class="toggle"> 
<details><summary> Required packages </summary>

```{r setup_2, message=FALSE, warning=FALSE, class.source = 'fold-hide'}

# set up

library(data.table)
library(dplyr)
library(dtplyr)
library(tidyr)
library(tictoc)
library(magrittr)
library(ggplot2)
library(patchwork)
library(reactable)

# Setggplot theme

custom_theme <-
  list(
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        text = element_text(size = 9),
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
      )
  )

# function to swap strands of an allele
swap_strand = function(allele){


  s_allele = dplyr::case_when(allele == "A" ~ "T",
                              allele == "T" ~ "A",
                              allele == "G" ~ "C",
                              allele == "C" ~ "G")
  
  return(s_allele)
}

```

</details>
</div>

<div class="toggle"> 
<details><summary> Load Mogil data </summary>


```{r load_mogil_data}

mogil_combined = data.table::fread("output/combined_eqtl_summary_stats/mogil_combined_only.csv") %>% 
                 lazy_dt(.)

```

</details>
</div>

# Part 5: Adding datasets from the eQTL catalogue

We may want to consider adding a 3rd and / or 4 population dataset for two reasons:

i. Improve mashr estimates
ii. Investigate the impact of microarray vs RNAseq studies on sharing of eQTLs (mashr analysis of Shang and Smith et al 2020 data shared more eQTLs within dataset (i.e. between European American and African American), than between other eQTL studies with more closely related ancestries - which we think could be caused by differences in microarray vs RNAseq)
iii. Provide an additional replication population/s to assess the accuracy of trained population specific machine learning models.

## 5.1 Adding Fairfax 2014 from the eQTL catalogue

```{r fairfax_load}

# load fairfax data for the same tissue
fairfax = data.table::fread("data/eqtl_summary_stats/Mogil_2018/similar_eqtl_studies/Fairfax_2014_microarray_monocyte_naive.all.tsv")

```

### Problems with adding Fairfax (or any microarray from the eQTL catalogue)

There are multiple probes (and therefore multiple estimated effect sizes and standard errors) per gene! How do I select an effect size?

```{r fairfax_issues}

fairfax = lazy_dt(fairfax, immutable = FALSE) %>% 
                  filter(type == "SNP", 
                         !is.na(rsid),
                         !is.na(beta),
                         !is.na(se),
                         !is.na(ref),
                         !is.na(alt)) %>% 
          compute()

fairfax %>% 
  group_by(gene_id, rsid) %>% 
  summarise(n = n()) %>% 
  filter(n > 1) %>% 
  head() %>% 
  as.data.frame() #some rsids are NAs - need to remove these NAs before adding to Mogil
# importantly though - why are gene snp pairs being tested twice - 

# example/s of gene snp pairs being tested twice:
fairfax %>% 
  filter(molecular_trait_object_id == "ENSG00000000419" & rsid == "rs12479559") %>% 
  as.data.frame()

fairfax %>% 
  filter(molecular_trait_object_id == "ENSG00000000419" & rsid == "rs13037515") %>% 
  as.data.frame()

# Conclusion:
# there's multiple illumina probe ids per gene - which is leading to multiple sets
# of eQTLs statistics (multiple effect sizes) per gene
# in this case  - (Christina suggested selecting the illumina probe id with the lowest standard
# error per gene - and seeing if this works 6/10/2021)

```

## Adding Fairfax to Mogil dataset

```{r fairfax_setup}

# rename fairfax columns to identify they are from fairfax (important for combining datasets later)
# + remove unnecessary columns for this analysis 

# set up columns for adding to mogil
fairfax = fairfax %>%  
          select(snp = rsid, 
              gene = gene_id,
              #fairfax_median_tpm = median_tpm, fairfax is a microarray experiment
              fairfax_beta = beta, #so no tpm avaliable
              fairfax_se = se,
              fairfax_maf = maf,
              fairfax_ref = ref, 
              fairfax_alt = alt) 


fairfax_mogil = inner_join(mogil_combined,
                          fairfax) %>% 
                compute()
 
```
 
 
## Select the illumina probe with the lowest standard error per gene:

```{r fairfax_filter_by_se}

fairfax_mogil = fairfax_mogil %>% 
                group_by(snp, gene) %>% 
                slice_max(fairfax_beta,
                          n = 1,
                          with_ties = FALSE) %>% 
                ungroup() %>% 
                compute()

```
 
```{r, eval = FALSE}

# original number of mogil gene snp pairs
n_mogil = combine_mogil %>% 
          nrow()

print(n_mogil)

# gene snp pairs in both mogil and fairfax
# i.e. how many are left after for filtering for what is also found in fairfax
n_fairfax_mogil = fairfax_mogil %>% 
                  collect() %>% 
                  nrow()

print(n_fairfax_mogil)

# check that each row is a snp gene pair
n_fairfax_mogil = fairfax_mogil %>% 
                  select(gene,snp) %>% 
                  distinct() %>% 
                  collect() %>% 
                  nrow()

print(n_fairfax_mogil)

```

### Are reference alleles the same between Mogil 2018 and Fairfax 2014?

```{r fairfax_ref_alleles}

fairfax_testing = fairfax_mogil %>% 
                  # which snps have the same reference allele in mogil and fairfax? 
                  mutate(same_ref = ifelse(ea_ref == fairfax_ref, TRUE, FALSE)) %>% 
                  # which snps have the same reference and alternative alleles in mogil and fairfax? 
                  mutate(same_ref_alt_fairfax = ifelse(ea_ref == fairfax_ref & ea_alt == fairfax_alt, TRUE, FALSE)) %>% 
                  # which snps have swapped reference and alternative alleles between mogil and fairfax?
                  mutate(ref_alt_swapped_fairfax = ifelse(ea_ref == fairfax_alt & ea_alt == fairfax_ref, TRUE, FALSE)) %>% 
                  compute()

```


```{r, eval = FALSE}

# how many snps fit these classes: 
fairfax_testing  %>% 
  group_by(same_ref, 
           same_ref_alt_fairfax, 
           ref_alt_swapped_fairfax) %>% 
  summarise(n = n()) %>% 
  collect()

```

### Total number of snp-gene pairs with different reference / alternative alleles to Mogil 2018? 

```{r fairfax_n_diff_ref_alt, eval = FALSE}

# total number of snp-gene pairs after combining mogil with fairfax
total_fairfax = fairfax_testing %>% collect() %>% nrow()

# total number of snps gene pairs where the
# reference and alternative allele are not the same
# across fairfax and mogil
dif_mogil_fairfax = fairfax_testing %>% 
                    filter(same_ref_alt_fairfax == FALSE) %>% 
                    collect() %>% 
                    nrow()

# percentage of all snps:
round((dif_mogil_fairfax / total_fairfax)* 100, digits = 2)

```

### What do these snps with different reference / alternative alleles to Mogil 2018 look like (other than simple swapage)?

```{r fairfaix_different_ref_alt_look_like, eval = FALSE}

fairfax_test_2 = fairfax_testing %>% 
                 filter(same_ref == TRUE & same_ref_alt_fairfax == FALSE) %>% 
                 select(-gene) %>% 
                 distinct() %>% 
                 compute()


fairfax_test_2 %>% 
head() %>% 
as.data.frame()

fairfax_test_2 %>% 
  group_by(ea_alt, fairfax_alt) %>% 
  summarise(n = n()) %>% 
  as.data.frame()

```

```{r}
fairfax_testing = fairfax_testing %>% 
  mutate(strand_swapped_fairfax_ref = swap_strand(fairfax_ref)) %>% 
  mutate(strand_swapped_fairfax_alt = swap_strand(fairfax_alt)) %>% 
  mutate(strand_swapped_fairfax = ifelse(ea_ref == strand_swapped_fairfax_ref & ea_alt == strand_swapped_fairfax_alt, TRUE, FALSE)) %>% 
  mutate(strand_swapped_revs_alt_fairfax = ifelse(ea_alt == strand_swapped_fairfax_ref & ea_alt == strand_swapped_fairfax_alt, TRUE, FALSE)) %>% 
  compute()


```
  
```{r eval = FALSE}  
fairfax_testing %>% 
  group_by(same_ref, 
           same_ref_alt_fairfax, 
           ref_alt_swapped_fairfax,
           strand_swapped_fairfax,
           strand_swapped_revs_alt_fairfax) %>% 
  summarise(n = n()) %>% 
  collect()

fairfax_testing %>% 
  filter(same_ref == FALSE & same_ref_alt_fairfax == FALSE & ref_alt_swapped_fairfax == FALSE) %>% 
  head() %>% 
  collect()
  
```

### Steps to combine

1. Filter out any snps where the reference and alternative alleles do not match (either the same, or flipped)
2. For snps with swapped reference and alternative alleles - flip effect directions and adjust minor allele frequency so they match Mogil. 

```{r fairfax_combine}

fairfax_final = fairfax_testing %>% 
                filter(same_ref_alt_fairfax == TRUE | ref_alt_swapped_fairfax == TRUE) %>% 
                mutate(fairfax_beta = ifelse(ref_alt_swapped_fairfax == TRUE, -fairfax_beta, fairfax_beta)) %>% 
   mutate(fairfax_maf = ifelse(ref_alt_swapped_fairfax == TRUE, 1-fairfax_maf, fairfax_maf)) %>% 
                compute()

```

### Check the distribution of effect sizes + significance in Mogil after adding Fairfax

```{r distribution_mogil_after_fairfax, eval = FALSE}

# Effect size in Mogil African American Sample

plot_fairfax_sample = fairfax_final %>% 
                      slice_sample(n = length(multiple_alt_allele)) %>% 
                      collect()

plot_combine_sample = combine_mogil %>% 
                      slice_sample(n = length(multiple_alt_allele)) %>% 
                      collect()

( plot_fairfax_sample %>% 
  ggplot(aes(x=aa_beta)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
   xlim(-2,2) + 
    labs(x = "Effect size",
       subtitle = "After adding Fairfax")
 ) /
(
  plot_combine_sample %>% 
  ggplot(aes(x=aa_beta)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  xlim(-2,2) + 
  labs(x = "Effect size",
       subtitle = "Before adding Fairfax")
) + 
  plot_annotation(title = "Effect size in Mogil African American Sample")


###############################################################################

# Effect size in European American (Mogil)

(plot_fairfax_sample %>% 
  ggplot(aes(x=ea_beta)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
   xlim(-2,2) + 
    labs(x = "Effect size",
       subtitle = "After adding Fairfax")
 ) /
(
  plot_combine_sample %>% 
  ggplot(aes(x=ea_beta)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  xlim(-2,2) + 
  labs(x = "Effect size",
       subtitle = "Before adding Fairfax")
) + 
  plot_annotation(title = "Effect size in Mogil European American Sample")




# False discovery rate in Mogil African American Population

(plot_fairfax_sample %>% 
  ggplot(aes(x=aa_fdr)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
   xlim(0,1) + 
    labs(x = "False Discovery Rate",
       subtitle = "After adding Fairfax")
 ) /
(
  plot_combine_sample %>% 
  ggplot(aes(x=aa_fdr)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  xlim(0,1) + 
  labs(x = "False Discovery Rate",
       subtitle = "Before adding Fairfax")
) + 
  plot_annotation(title = "FDR in Mogil African American Sample")


################################################################################

# False discovery rate in Mogil European American

(plot_fairfax_sample %>% 
  ggplot(aes(x=ea_fdr)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
   xlim(0,1) + 
    labs(x = "False Discovery Rate",
       subtitle = "After adding Fairfax")
 ) /
(
  plot_combine_sample %>% 
  ggplot(aes(x=ea_fdr)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  xlim(0,1) + 
  labs(x = "False Discovery Rate",
       subtitle = "After adding Fairfax")
) + 
  plot_annotation(title = "FDR in Mogil European American Sample")

```


## 5.2 Adding CEDAR from the eQTL catalogue

```{r cedar} 

CEDAR = data.table::fread("data/eqtl_summary_stats/Mogil_2018/similar_eqtl_studies/CEDAR_microarray_monocyte_CD14.all.tsv")


# select the probe with lowest se
CEDAR = lazy_dt(CEDAR,immutable = FALSE) %>% 
                  filter(type == "SNP", 
                         !is.na(rsid),
                         !is.na(beta),
                         !is.na(se),
                         !is.na(ref),
                         !is.na(alt)) %>% 
        compute()

CEDAR = CEDAR %>% 
        group_by(gene_id, rsid) %>% 
        slice_max(beta,
                    n = 1,
                    with_ties = FALSE) %>% 
        ungroup() %>% 
        compute()

# set up columns for adding to mogil
CEDAR = CEDAR %>%  
          select(snp = rsid, 
              gene = gene_id,
              cedar_beta = beta, 
              cedar_se = se,
              cedar_maf = maf,
              cedar_ref = ref, 
              cedar_alt = alt) %>% 
        compute()


```


```{r combine_with_mogil_and_fairfax}

# combine with mogil and fairfax 
CEDAR_fairfax_mogil = inner_join(fairfax_final,
                                 CEDAR) 

CEDAR_fairfax_mogil = CEDAR_fairfax_mogil %>% 
                      # which snps have the same reference and alternative alleles in mogil and fairfax? 
                      mutate(same_ref_alt_cedar = ifelse(ea_ref == cedar_ref & ea_alt == cedar_alt, TRUE, FALSE)) %>%
                      # which snps have swapped reference and alternative alleles between mogil and fairfax?
                      mutate(ref_alt_swapped_cedar = ifelse(ea_ref == cedar_alt & ea_alt == cedar_ref, TRUE, FALSE)) %>% 
                      compute()


```

```{r cedar_add, eval = FALSE}

CEDAR_fairfax_mogil %>% 
  group_by(ref_alt_swapped_cedar, same_ref_alt_cedar) %>% 
  summarise(cor_beta = cor(ea_beta,cedar_beta)) %>% 
  collect()

```

```{r}

CEDAR_fairfax_mogil = CEDAR_fairfax_mogil %>% 
                      filter(ref_alt_swapped_cedar == TRUE | same_ref_alt_cedar == TRUE) %>% 
                      mutate(cedar_beta = ifelse(ref_alt_swapped_cedar == TRUE, -cedar_beta, cedar_beta)) %>% 
                      mutate(cedar_maf = ifelse(ref_alt_swapped_cedar == TRUE, 1-cedar_maf, cedar_maf)) %>% 
                      compute()

```

```{r eval = FALSE}
# check that this improves the correlation between effect size in Mogil european
# and cedar dataset: 

CEDAR_fairfax_mogil %>% 
  group_by(ref_alt_swapped_cedar, same_ref_alt_cedar) %>% 
  summarise(cor_beta = cor(ea_beta,cedar_beta)) %>% 
  collect()
  
```


## Filter maf < 0.05

```{r}
# eQTL catalogue studies have maf provided:
CEDAR_fairfax_mogil = CEDAR_fairfax_mogil %>% 
                      filter(fairfax_maf > 0.05, cedar_maf > 0.05) %>% 
                      as.data.frame()
```

```{r}

# All datasets in this analysis 

CEDAR_fairfax_mogil %>% 
  as.data.frame() %>% 
  data.table::fwrite("output/combined_eqtl_summary_stats/mogil_combined_microarray.csv")

```

## Matrices For mashr Analysis 

### Beta Matrices 

```{r}

microarray_beta_table = CEDAR_fairfax_mogil %>%  
                        select(contains("beta"), snp, gene) %>% 
                        as.data.frame() %>% 
                        tidyr::unite("gene_snp", gene:snp) 


data.table::fwrite(microarray_beta_table,
                   "output/mashr_input/mogil_microarray_beta_table.csv")

```

### Standard Error Matrices

```{r}

microarray_se_table = CEDAR_fairfax_mogil %>%  
                        select(contains("se"), snp, gene) %>% 
                        as.data.frame() %>% 
                        tidyr::unite("gene_snp", gene:snp) 


data.table::fwrite(microarray_se_table,
                   "output/mashr_input/mogil_microarray_se_table.csv")

```

