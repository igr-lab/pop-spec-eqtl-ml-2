---
title: "gnomAD LD scores - analysis across populations"
output:
  workflowr::wflow_html:
    toc: true
editor_options:
  chunk_output_type: console
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

**Introduction** One explanation for why eQTLs are not found across populations is that linkage disequilibrium patterns are different across populations, resulting in different variants being tagged to the causal variant and thus different variants being statically associated with changes to gene expression. For this reason, linkage disequilibrium (and in particular linkage disequilibrium differences across populations) should be a useful predictor of whether an eQTL is shared across human populations. 

However, it is not entirely clear (to me - any ways) how include linkage disequilibrium into the machine learning model for predicting whether a given eQTL is population-specific or shared. In this script we investigate LD scores from gnomAD v2 populations. LD scores are measured per genetic variants, and represent the sum of calculated linkage disequilibrium (r^2) measured with all other variants in the genome. If the LD score of a particular SNP varies across populations, this could suggest this SNP tags different loci in the genome across human populations, and thus why we observed population-specific eQTLs. 

**Aims: **
i. Investigate LD score across gnomAD populations and double check that their distribution matches expectations across populations
ii. Curate measures of LD score across population for the model to use to predict whether or not a particular eQTL is specific to a particular population, or whether it is shared across populations. 

## Set up

<div class="toggle"> 
<details><summary> Required packages </summary>

```{r message = FALSE, warning = FALSE, eval= TRUE}

renv::load()

library(dplyr)
library(data.table)
library(ggplot2)
library(patchwork)
library(dtplyr)
library(stringi)

# for normalisation
library(preprocessCore)

# packages and functions for producing correlation heatmap/s
library(circlize)
library(viridis)
library(ComplexHeatmap)

```

</details>
</div>

<div class="toggle"> 
<details><summary> Load gnomAD .ldscore datasets (for each gnomAD population ) </summary>

```{r gnomad_load}

# Load gnomAD ld scores data tables

# African American population LD scores
afr_ld = data.table::fread("data/gnomad_resources/ld_scores/african_american/gnomad.genomes.r2.1.1.afr.adj.ld_scores.ldscore")

# Ashkenazi Jewish population LD scores
ash_ld = data.table::fread("data/gnomad_resources/ld_scores/ashkenazi_jewish/gnomad.genomes.r2.1.1.asj.adj.ld_scores.ldscore")

# East Asian population LD scores
eas_ld = data.table::fread("data/gnomad_resources/ld_scores/east_asian/gnomad.genomes.r2.1.1.eas.adj.ld_scores.ldscore")

# Estonian population LD scores
est_ld = data.table::fread("data/gnomad_resources/ld_scores/estonian/gnomad.genomes.r2.1.1.est.adj.ld_scores.ldscore")

# Finnish European population LD scores
fin_ld = data.table::fread("data/gnomad_resources/ld_scores/finnish_european/gnomad.genomes.r2.1.1.fin.adj.ld_scores.ldscore")

# Latino / Admixed American Ancestry LD scores
amr_ld = data.table::fread("data/gnomad_resources/ld_scores/latino_admixed_american/gnomad.genomes.r2.1.1.amr.adj.ld_scores.ldscore")

# Non-Finnish European LD Scores population LD scores
nfe_ld =  data.table::fread("data/gnomad_resources/ld_scores/non_finnish_european/gnomad.genomes.r2.1.1.nfe.adj.ld_scores.ldscore")

# North Western European population LD scores
nwe_ld = data.table::fread("data/gnomad_resources/ld_scores/north_western_european/gnomad.genomes.r2.1.1.nwe.adj.ld_scores.ldscore")

# Southern European population LD scores
se_ld = data.table::fread("data/gnomad_resources/ld_scores/southern_european/gnomad.genomes.r2.1.1.seu.adj.ld_scores.ldscore")

```


</details>
</div>

<div class="toggle"> 
<details><summary> Load / create plotting functions + settings  </summary>

```{r plotting_functions}

# Load functions from plotting_results_functions.R script using box
options(box.path = getwd())  # wd should just be the pop_spec_eqtl folder

box::use(code/utils/plotting_results_functions)

# ggplot theme
custom_theme = plotting_results_functions$custom_theme(20)

# set up heatmap colour functions
col_fun = colorRamp2(seq(from = 0.5, to = 1, by = 0.1), plasma(n =6, direction = 1))

col_fun_2 = colorRamp2(seq(from = 0, to = 1, by = 0.1), plasma(n =11, direction = 1))
```

</details>
</div>

# What does the distribution of LD scores look like within each population? 

### What does the raw LD score distribution plots look like within each population?

**Aims:** Here we consider four gnomAD populations; (African Ancestry, Non-finnish European, Latino + Admixed, East Asian) to investigate the properties of the distribution of LD scores. 

```{r ld_score_each_pop, fig.height=15, fig.width=10}


# Plot histogram of LD scores in each gnomAD population
(
  afr_ld %>% 
  ggplot(aes(x=L2)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  labs(title = "African Ancestry gnomAD Population LD Score Distribution")
               ) / (
nfe_ld %>% 
  ggplot(aes(x=L2)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  labs(title = "Non-finnish European gnomAD Population LD Score Distribution")
) + (
amr_ld %>% 
  ggplot(aes(x=L2)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  labs(title = "Latino + Admixed gnomAD Population LD Score Distribution")
) + (
eas_ld %>% 
  ggplot(aes(x=L2)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  labs(title = "East Asian gnomAD Population LD Score Distribution")
) 


```

```{r}
# Get summary statistics on the distribution of LD scores 

# African Ancestry pop
summary(afr_ld$L2)

# North European - non-Finnish
summary(nfe_ld$L2)

# Latino / Admixed American 
summary(amr_ld$L2)

# East Asian
summary(eas_ld$L2)

```

**Notes / Conclusions:**

- The distribution of LD scores within a population are highly skewed (median < mean) - they are not symmetric!
- LD score can be negative (and this negative sign is meaningful - i.e. an LD score of -15 means something different to +15), so taking a log transformatios of the absolute value to reduce this skew are not appropriate
- Variance in, and mean LD score can be quite different across populations. We may want to consider normalising LD scores across populations to make them more comparable. 
- In general, populations where we would expect higher average LD (larger linkage blocks, e.g. European populations) seem to have distributions of LD scores than those for which we would (further explored below - yay things behaving as we would expect!)

# How does the distribution of LD scores vary between populations? 

*Expectations*: In general, Linkage Disequilibrium Blocks should be larger, and therefore there should be average higher Linkage Disequilibrium (and therefore average higher LD score) in European population datasets, than there should be in African ancestry population. Additionally, the correlation between LD score should be higher between European population datasets and Latino population dataset, than between European population datasets and the African American dataset. 

### Mean LD score in each population

```{r mean_ld_cross_pop, fig.height=8, fig.width=10, eval = TRUE}

all_ld_score_tables = tibble::lst(
                                   African_American = afr_ld,
                                   Ashkenazi_Jewish = ash_ld,
                                   East_Asian = eas_ld,
                                   Estonian = est_ld,
                                   Finnish = fin_ld,
                                   Latino_Admixed_American = amr_ld,
                                   North_Western_European = nwe_ld,
                                   Non_Finnish_European = nfe_ld,
                                   Southern_European = se_ld)

# unlike above - filter out non-snps 
# the eQTLs we are considering are only snps
all_ld_score_tables = all_ld_score_tables %>%
                      lapply(.,
                               function(y) {
                                 y %>%
                                   dplyr::filter(!dplyr::if_any(c("ref", "alt"), ~nchar(.x)>1)) %>% 
                                   as.data.frame()})

# Mean LD score - for each population 
all_ld_score_tables %>% sapply(.,function(x) mean(x$L2))

# Mean absolute LD score - for each population
all_ld_score_tables %>% sapply(.,function(x) mean(abs(x$L2)))

# Median LD score - for each population
all_ld_score_tables %>% sapply(., function(x) median(x$L2))

# Variance in LD score - for each population
all_ld_score_tables %>% sapply(.,function(x) var(x$L2))


(
  se_ld %>% 
  ggplot(aes(x=L2)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  labs(title = "Southern European gnomAD Population LD Score Distribution")
  
) / (
  
  ash_ld %>% 
  ggplot(aes(x=L2)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  labs(title = "Ashkenazi Jewish gnomAD Population LD Score Distribution")
  
) / (
  
  afr_ld %>% 
  ggplot(aes(x=L2)) + 
  geom_histogram(bins = 100) + 
  custom_theme + 
  labs(title = "Ashkenazi Jewish gnomAD Population LD Score Distribution")
  
)
  

```

*Conclusion* Mean LD score across populations seems to match expectations. For example, mean LD score is highest in European population - and lowest in population with high African ancestry (i.e. the African American population). However, there are some interesting uninuitive notes: For example, the Southern European population dataset has a negative mean LD score and Ashkenazi Jewish Population dataset has one of the lowest mean LD score.  


### Correlation of LD Score across populations

```{r ld_score_cor_cross_pop, fig.height=15, fig.width=15, eval = TRUE}

join_LD_score_cross_pop = Reduce("inner_join", 
                                    lapply(1:length(all_ld_score_tables), 
                                                             function(x) {
                                                               population = names(all_ld_score_tables)[x];
                                                               var_quo = deparse(substitute(population));
                                                                           return(
                                                                           all_ld_score_tables[[x]] %>% 
                                                                           select(
                                                                                  CHR,
                                                                                  BP,
                                                                                  ref,
                                                                                  alt,
                                                                                !!paste0(var_quo,"_AF") := AF,  
                                                                                !!(names(all_ld_score_tables)[x]) := L2) %>% 
                                                                           lazy_dt (.))
                                                                                         }
         )
)


draw(
     Heatmap(
             mat = join_LD_score_cross_pop %>% 
                   select(-CHR,
                          -BP,
                          -ref,
                          -alt,
                          -contains("AF")) %>% 
                   as.data.frame() %>% 
                   cor(., method = "spearman"),  
        col = col_fun,
        name = "LD Score Correlation)",
        column_title = "Correlation between LD score",
        column_names_gp = grid::gpar(fontsize = 12),
        row_names_gp = grid::gpar(fontsize = 12),
        column_title_gp = grid::gpar(fontsize = 18),
        clustering_distance_rows = function(m) dist(m)),
heatmap_legend_side = "left")

```

*Conclusion* In general, the pattern of correlation in LD score across populations matches expectations. European population LD scores are more highly correlated with the LD scores of other European population LD scores, than they are to non-European population LD scores (e.g. African American, East Asian). 

It is interesting to note that the Southern Europe population LD scores has relatively (compared to other European population LD scores) lower correlation with non-European population LD scores - in particular African American and East Asian population LD scores.

It is also nice to note that the LD scores of the non-Finnish European population - which is primarily comprised of North Western European population individuals - is most highly correlated with North Western population LD scores. 

# Are Reference and Alternative alleles consistent across population? 

If we want to use cross-population measurements of LD across populations, we should make sure that (1) ALT / REF alleles are consistent across gnomAD population datasets - and ideally that these REF / ALT alleles are consistent with the REF and ALT allele we have used in our study. Below we test whether this is true:

```{r multi_alleles}

# Just for example - let's consider combining LD scores from the African ancestry gnomAD pop
# and the Latino + Admixed American gnomAD pop

test_alt_ref = inner_join(
                          afr_ld %>% select(BP, CHR, 
                                            afr_ref = ref, afr_alt = alt, afr_ld = L2),
                          ash_ld %>% select(BP, CHR, 
                                            ash_ref = ref, ash_alt = alt, ash_ld = L2)
)

```

```{r multi_alleles_2}
# Checks:
# How many ref / alt alleles are the same between these populations (i.e. same_ref_alt)
# How many ref / alt alleles are just swapped between these populations? 

test_alt_ref %>% 
  mutate(same_ref_alt = ifelse(ash_alt == afr_alt & ash_ref == afr_ref, TRUE, FALSE)) %>% 
  mutate(swapped = ifelse(ash_alt == afr_ref & ash_ref == afr_alt, TRUE, FALSE)) %>% 
  group_by(same_ref_alt, swapped) %>% 
  as.data.frame() %>% 
  summarise(n = n())


test_alt_ref %>% 
  mutate(same_ref_alt = ifelse(ash_alt == afr_alt & ash_ref == afr_ref, "same reference and alt allele", "different reference and alt allele")) %>% 
  mutate(swapped = ifelse(ash_alt == afr_ref & ash_ref == afr_alt, "swapped", "not-swapped")) %>%   
  sample_n(2000) %>% 
  as.data.frame() %>% 
  ggplot(aes(x=ash_ld, y=afr_ld)) + 
  geom_point(alpha = 0.5) + 
  facet_grid(swapped~same_ref_alt) + custom_theme + 
  labs(x= "Ashkenazi Jewish population LD scores",
       y = "African American population LD scores")

```

```{r example_multi_alleles}
# Sometimes there are multiple alternative alleles per location:
 afr_ld %>% 
   group_by(CHR,BP) %>% 
   summarise(n = n()) %>% 
   filter(n > 1) %>%  
   head()
 
afr_ld %>% 
   mutate(not_snp = ifelse(if_any(c("ref", "alt"), ~nchar(.x)>1), TRUE, FALSE)) %>% 
   filter(not_snp == FALSE) %>% 
    group_by(CHR,BP) %>% 
   summarise(n = n()) %>% 
   filter(n > 1) 

afr_ld %>% filter(CHR == 1 & BP == 768116)

afr_ld %>% filter(CHR == 2, 
                  BP == 202689521)

```

### Considering SNPs only

```{r consider_snp_only}

test_alt_ref %>% 
  filter(!if_any(contains("ref")|contains("alt"), ~nchar(.x)>1)) %>% 
  mutate(same_ref_alt = ifelse(ash_alt == afr_alt & ash_ref == afr_ref, TRUE, FALSE)) %>% 
  mutate(swapped = ifelse(ash_alt == afr_ref & ash_ref == afr_alt, TRUE, FALSE)) %>% 
  group_by(same_ref_alt, swapped) %>% 
  as.data.frame() %>% 
  summarise(n = n())

test_alt_ref %>% 
  filter(!if_any(c("afr_ref", "afr_alt", "ash_ref", "ash_alt"), ~nchar(.x)>1)) %>% 
  mutate(same_ref_alt = ifelse(ash_alt == afr_alt & ash_ref == afr_ref, "same reference and alt allele", "different reference and alt allele")) %>% 
  mutate(swapped = ifelse(ash_alt == afr_ref & ash_ref == afr_alt, "swapped", "not-swapped")) %>%  
  sample_n(2000) %>% 
  as.data.frame() %>% 
  ggplot(aes(x=ash_ld, y=afr_ld)) + 
  geom_point(alpha = 0.5) + 
  facet_grid(swapped~same_ref_alt) + 
  custom_theme + 
  labs(x= "Ashkenazi Jewish population LD scores",
       y = "African American population LD scores")

```

*Notes / Conclusions*: 

I'm not sure how to (or if it is even possible) to adjust LD scores when the reference and alternative alleles are different, hence, to ensure that reference and alternative alleles are consistent cross gnomAD populations we have restrict LD score information to alleles for which reference and alternative alleles are consistent across populations ... (this is obviously something to investigate!) (See for example, below we join datasets by 'inner_join' using ref and alt). 

Additionally, I have noticed there are scenarios where there are multiple alternative alleles at the same position (see above), and without comparing with each of the datasets it is not clear which allele we should select. So, what I will do is: (1) join ensuring reference and alternative alleles are the same, and then where there is still multiple alternative alleles per locus (2) choose the LD score corresponding to the allele with the highest min allele frequency across gnomAD populations (small allele frequencies can distort LD scores). 

# Creating features for machine learning models

From the above investigations we have decided to consider two type of features for each eQTL;

(1) LD scores for each gnomAD population

(2) Measures of variation and center in LD score across populations

### Combine separate LD score datasets (separated by population) into a single dataset

```{r combined_ld_datasets}

# Join all into a single dataset 
join_LD_score_cross_pop = Reduce("inner_join", 
                                    lapply(1:length(all_ld_score_tables), 
                                                             function(x) {
                                                               population = names(all_ld_score_tables)[x];
                                                               var_quo = deparse(substitute(population));
                                                                           return(
                                                                           all_ld_score_tables[[x]] %>% 
                                                                           select(
                                                                                  CHR,
                                                                                  BP,
                                                                                  ref,
                                                                                  alt,
                                                                                #!!paste0(var_quo,"_AF") := AF,  
                                                                                !!paste0(names(all_ld_score_tables)[x], "_LDScore") := L2) %>% 
                                                                           lazy_dt (.))
                                                                                         }
         )
)

join_LD_score_cross_pop %>% sample_n(10) %>% as.data.frame()

# check that non-SNPs were correctly filtered out earlier: 
join_LD_score_cross_pop %>% filter(if_any(contains("ref")|contains("alt"), ~nchar(.x)>1)) %>% head() %>% as.data.frame()

```

## Include normalised

```{r normalise, eval = FALSE, include=FALSE}

normalised_ldscores = join_LD_score_cross_pop %>% 
                      select(contains("_LDScore")) %>% 
                      as.data.frame() 


normalised_ldscores = preprocessCore::normalize.quantiles(x= as.matrix(normalised_ldscores), 
                                                          keep.names = TRUE) %>% 
                      as.data.frame() %>% 
                      rename_with(., ~paste0("norm_",.x))

normalised_ldscores = dplyr::bind_cols(join_LD_score_cross_pop, normalised_ldscores)

normalised_ldscores %>% head()

```

# Measures of variation across populations

Since we have observed that the pattern of , we are now interested in developing location-wise 'scores' / variables which measure how LD varies across population at this location. Population-specific eQTLs should be more likely to be found in regions where LD is different across populations, so measurements of how variable LD scores are across populations should aid the model in predicting whether or not an eQTL is population-specific or shared. Below we calculate measures of variation across populations, and investigate which metrics to include in our model. 

## Measures of centre - across populations

```{r measures_centre_cross_pop, fig.height=15, fig.width=15}

join_LD_score_cross_pop = join_LD_score_cross_pop %>% as.data.frame()

# Mean LD score, per site, cross population

# calculate the differences between all possible pairwise comparisons 

mean_dist = function(x){
  
  require(dplyr)
  
  dist_mat = outer(x,x,"-")
  
  return(dist_mat[upper.tri(dist_mat)] %>% 
  abs() %>% 
  mean())
  
}


median_dist = function(x){
  
  require(dplyr)
  
    dist_mat = outer(x,x,"-")
  
  return(dist_mat[upper.tri(dist_mat)] %>% 
  abs() %>% 
  median())
    
}

#join_LD_score_cross_pop = join_LD_score_cross_pop %>% 
#  as.data.frame() %>% 
#  rowwise() %>% 
#  mutate(min_AF = min(c_across(contains("AF")))) %>% 
#  ungroup() %>% 
#  group_by(CHR,BP) %>% 
#  slice_max(min_AF, with_ties = FALSE) %>% 
#  select(-contains("AF")) %>% 
#  ungroup()

# check that there is now only one LD score per locus

join_LD_score_cross_pop %>% 
  select(BP, CHR) %>% 
  distinct() %>% 
  nrow()

join_LD_score_cross_pop %>% 
  nrow()

box::use(matrixStats[...])

# create dataset of cross-population measurements of LD 


all_pop_sets = join_LD_score_cross_pop %>% 
               select(contains("LDScore"), 
                      -"Non_Finnish_European_LDScore") %>% 
               names()

reduced_pop_sets = join_LD_score_cross_pop %>% 
                   select(contains("East_Asian"),
                          contains("Non_Finnish_European"),
                          contains("Latino_Admixed_American"),
                          contains("African_American"),
                          contains("Ashkenazi_Jewish"),
                          contains("Finnish")) %>% 
                    names()

gnomad_ld_scores_cross_pop = join_LD_score_cross_pop %>% 
                             mutate(
                               mean_LDScore_all = rowMeans(across(all_pop_sets)),
                             median_LDScore_all = rowMedians(as.matrix(across(all_pop_sets))),
                             var_LDScore_all =  rowVars(as.matrix(across(all_pop_sets))),
                             MAD_LDScore_all = rowMads(as.matrix(across(all_pop_sets))),
                             mean_LDScore_reduced = rowMeans(across(reduced_pop_sets)),
                             median_LDScore_reduced = rowMedians(as.matrix(across(reduced_pop_sets))),
                             var_LDScore_reduced =  rowVars(as.matrix(across(reduced_pop_sets))),
                             MAD_LDScore_reduced = rowMads(as.matrix(across(reduced_pop_sets)))
                             ) %>% 
                             rowwise() %>% 
                             mutate(
                             mean_dist_LDScore_all = mean_dist(c_across(all_of(all_pop_sets))),
                             median_dist_LDScore_all = median_dist(c_across(all_of(all_pop_sets)))
                             ) 


head(gnomad_ld_scores_cross_pop %>% as.data.frame())


cor_mat = gnomad_ld_scores_cross_pop %>% 
          select(contains("LDScore")) %>% 
          cor(., method = "spearman")


print(cor_mat)


draw(
     Heatmap(
             mat = cor_mat,  
        col = col_fun_2,
        name = "LD Score Correlation",
        column_title = "Correlation between LD score measurements",
        column_names_gp = grid::gpar(fontsize = 12),
        row_names_gp = grid::gpar(fontsize = 12),
        column_title_gp = grid::gpar(fontsize = 18),
        clustering_distance_rows = function(m) dist(m)),
heatmap_legend_side = "left")



```

## Plot measurements of cross population variability 

```{r plot_difference_cross_pop, fig.height=25, fig.width=15}

(gnomad_ld_scores_cross_pop %>% 
  as.data.frame() %>% 
  ggplot(aes(x=mean_LDScore_all)) + 
  geom_histogram(bins = 100)+ 
  custom_theme
) / (gnomad_ld_scores_cross_pop %>% 
  as.data.frame() %>% 
  ggplot(aes(x=mean_LDScore_reduced)) + 
  geom_histogram(bins = 100)+ 
  custom_theme
) / (gnomad_ld_scores_cross_pop %>% 
  as.data.frame() %>% 
  ggplot(aes(x=median_LDScore_reduced)) + 
  geom_histogram(bins = 100)+ 
  custom_theme
) / (gnomad_ld_scores_cross_pop %>% 
  as.data.frame() %>% 
  ggplot(aes(x=median_LDScore_all)) + 
  geom_histogram(bins = 100)+ 
  custom_theme
)/ (gnomad_ld_scores_cross_pop %>% 
  as.data.frame() %>% 
  ggplot(aes(x=mean_dist_LDScore_all)) + 
  geom_histogram(bins = 100)+ 
  custom_theme
) /  (gnomad_ld_scores_cross_pop %>% 
  as.data.frame() %>% 
  ggplot(aes(x=var_LDScore_all)) + 
  geom_histogram(bins = 100)+ 
  custom_theme
) / 
  (gnomad_ld_scores_cross_pop %>% 
  as.data.frame() %>% 
  ggplot(aes(x=MAD_LDScore_all)) + 
  geom_histogram(bins = 100)+ 
  custom_theme
)

```

## Saving results

```{r save_results}

# for cross population measuresments - keep only mean and var for the reduced set

final_ld_measurements_df = join_LD_score_cross_pop %>% 
                           mutate(
                                   mean_LDScore_reduced = rowMeans(across(reduced_pop_sets)),
                                   var_LDScore_reduced =  rowVars(as.matrix(across(reduced_pop_sets))),
                                  ) %>% 
                           rename(chromosome = CHR,
                                  start = BP) %>% 
                           mutate(end = start) %>% 
                           select(chromosome,
                                  start,
                                  end,
                                  ref,
                                  alt,
                                  contains("LDScore")
                                  )

data.table::fwrite(final_ld_measurements_df,
                   "output/gnomad_ld_scores/ld_score_cross_pop.csv")

```
