

# eGenes - before and after mashr

{
  library(dplyr)
  library(magrittr)
  library(ggplot2)
  library(dtplyr)
  
  custom_theme <-
    list(
      theme_bw() +
        theme(
          panel.border = element_blank(),
          axis.line = element_line(),
          panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          text = element_text(size = 8),
          legend.position = "right",
          legend.box = "vertical",
          strip.background = element_blank(),
          legend.text=element_text(size=8),
          legend.title = element_text(size=8),
          legend.margin=margin(),
          axis.text = element_text(size = 8),
          axis.text.x = element_text(size = 8, angle = 0, vjust = 0.5)
        )
    )
  
  # Plotting colours
  hispanic_col = c("#009988")
  shared_col = c("#000000")
  european_col = c("#0077BB")
  indonesian_col = c("#CC3311")
  african_american_col = c("#EE3377")
  
} 

############# Data load #####################
{
  
  # Calculating effective sample size 
  # Needs mashr results and standard error input to mashr
  mogil_mashr_res = readRDS("output/mashr_results/mogil_only_mashr_em_results.rds")
  
  mogil_mashr_1by1 = readRDS("output/mashr_results/mogil_only_mashr_em_model_full_1v1.rds")
  
  
  natri_mashr_res = readRDS("output/mashr_results/natri_mashr_em_results.rds")
  
  natri_mashr_1by1 = readRDS("output/mashr_results/natri_mashr_em_model_full_1v1.rds")
  
} 


############## Plot function for converting study names to colours #######


col_study = function(study_vector){
  
  cols = case_when(grepl("euro|ea|gtex|twinsuk|biobank|gtex|lepik",tolower(study_vector)) ~ european_col,
                   grepl("natri|indo", tolower(study_vector)) ~ indonesian_col,
                   grepl("african|aa", tolower(study_vector))~ african_american_col,
                   TRUE ~ hispanic_col)
  
  return(cols)
}

#### Function which calculates number of eGenes + eQTLs ##########

egenes = function(eqtl_df){
  
  library(tidyr)
  library(dplyr)
  library(magrittr)
  
  return(eqtl_df %>% 
           tidyr::separate("gene_snp", into = c("gene", "snp")) %>% 
           group_by(gene) %>% 
           slice_sample(n = 1) %>% 
           ungroup() %$%
           gene)
  
}


n_egenes_eqtls = function(mashr_lfsr_results, 
                          sig_thresh = 0.01){
  
  
  groups = colnames(mashr_lfsr_results)
  
  n_eqtls = vector()
  n_egenes = vector()
  egenes = vector()
  venn_df = list()
  
  for (g in 1:length(groups)){
    
    # for this group  / dataset
    group = groups[g]
    
    #  find the eqtls 
    eqtls = lazy_dt(as.data.frame(mashr_lfsr_results) %>% 
                      tibble::rownames_to_column("gene_snp")
    ) %>% 
      filter(!!as.symbol(group) < sig_thresh) %>% 
      as.data.frame()
    
    # the number of eqtls
    n_eqtls[g] = nrow(eqtls)
    
    
    # label egenes 
    egenes = egenes(eqtls)
    
    n_egenes[g] = length(egenes)
    
    venn_df[[g]] = data.frame(egenes = egenes,
                              study = group)
    
  }
  
  final_venn_df = data.table::rbindlist(venn_df) %>%  
    mutate(seen = 1) %>% 
    pivot_wider(id_cols = egenes, 
                names_from = study, 
                values_from = seen, 
                values_fill = 0)
  
  df = data.frame(study = groups,
                  n_eqtls = n_eqtls,
                  n_egenes = n_egenes)
  
  return(list(n_eqtl_egene = df,
              venn_df = final_venn_df))
  
}


################# Make eGenes and eQTLs plotting tables ###################### 

make_plot_table = function(before_plot_list,
                           after_plot_list){
  
  
  before_table = before_plot_list %$%
    n_eqtl_egene %>% 
    mutate(time = "Before mashr analysis") %>% 
    as_tibble() 
  
  after_table = after_plot_list %$%
    n_eqtl_egene %>% 
    mutate(time = "After mashr analysis") %>% 
    as_tibble() 
  
  plot_table = rbind(before_table, after_table) %>% 
    mutate(cols = col_study(study)) %>% 
    arrange(desc(time))
  
  
  return(plot_table)
}

natri_before_list = n_egenes_eqtls(natri_mashr_1by1$result$lfsr)  

natri_after_list = n_egenes_eqtls(natri_mashr_res$lfsr) 

natri_egenes_eqtls  = make_plot_table(natri_before_list,
                                      natri_after_list) %>% 
  mutate(study_name = case_when(grepl("gtex", tolower(study)) ~ "GTEx",
                                grepl("twinsuk", tolower(study)) ~ "TwinsUK",
                                grepl("lepik", tolower(study)) ~ "Estoninan\nBiobank",
                                TRUE ~ "Natri. 2022"
  )
  )


mogil_before_list = n_egenes_eqtls(mogil_mashr_1by1$result$lfsr)

mogil_after_list = n_egenes_eqtls(mogil_mashr_res$lfsr)

mogil_egenes_eqtls = make_plot_table(mogil_before_list,
                                     mogil_after_list) %>% 
  mutate(study_name = case_when(grepl("aa", tolower(study)) ~ "African\nAmerican",
                                grepl("ea", tolower(study)) ~ "European\nAmerican",
                                grepl("his", tolower(study)) ~ "Hispanic")
  )


############### Plot ###################

{
  
  library(ggpubr)
  library(plotgardener)
  
  pdf(file = "thesis_figures/output_pdfs/eQTLs_before_and_after.pdf",
      width = 5.91, 
      height = 3.93,
      family = "Helvetica")
  
  pageCreate(width = 150, 
             height = 100, 
             xgrid = 5,
             ygrid = 5,
             showGuides = FALSE,
             default.units = "mm")
  

############### Part A ##########
  
A = ggpaired(natri_egenes_eqtls %>% 
               arrange(desc(time)),
             x = "time",
             y = "n_eqtls",
             color = "study",
             palette = natri_egenes_eqtls %>% 
               filter(time == "After mashr analysis") %>% 
               arrange(study) %$% 
               cols,
             line.color = "study",
             width = 0)  + 
  labs(y = "Number of eQTLs", x= "") + 
  annotate("text",
           x = 2.05,
           y = natri_egenes_eqtls %>% 
             filter(time == "After mashr analysis") %$% 
             n_eqtls,
           label = unique(natri_egenes_eqtls$study_name),
           lineheight = 0.75,
           hjust = 0,
           vjust = c("top", "top", "bottom","top"),
           size = 8/.pt) +
  ylim(0,8*10^5) + 
  scale_x_discrete(labels= c("Before\nmashr analysis",
                             "After\nmashr analysis")) +
  custom_theme +
  theme(legend.position="none")


plotGG(plot = A,
       x = 1,
       y = 3.5,
       width =  74,
       height = 90,
       just = c("left", "top"),
       default.units = "mm")

plotText(label = "A", 
         fontsize = 12,
         x = 0.5, 
         y = 2.5, 
         just = "left",
         default.units = "mm",
         fontface = "bold",
         fontfamily="Helvetica")

############## Part B ################

B = ggpaired(mogil_egenes_eqtls %>% 
               arrange(desc(time)),
             x = "time",
             y = "n_eqtls",
             color = "study_name",
             palette = mogil_egenes_eqtls %>% 
               filter(time == "After mashr analysis") %>% 
               arrange(study) %$% 
               cols,
             line.color = "study_name",
             width = 0)  + 
  labs(y = "Number of eQTLs", x= "") + 
  annotate("text",
           x = 2.05,
           y = mogil_egenes_eqtls %>% 
             filter(time == "After mashr analysis") %$% 
             n_eqtls,
           label = unique(mogil_egenes_eqtls$study_name),
           #position = replicate(3, position_jitter()),
           check_overlap = TRUE,
           hjust = 0,
           lineheight = 0.75,
           vjust = c("top", "top", "bottom"),
           size = 8/.pt) +
  ylim(0,8*10^5) + 
  scale_x_discrete(labels= c("Before\nmashr analysis",
                             "After\nmashr analysis")) +
  custom_theme +
  theme(legend.position="none")


plotGG(plot = B,
       x = 75 + 1,
       y = 3.5,
       width = 74,
       height = 90,
       just = c("left", "top"),
       default.units = "mm")

plotText(label = "B", 
         fontsize = 12,
         x = 74 + 0.5 + 0.5, 
         y = 2.5, 
         just = "left",
         default.units = "mm",
         fontface = "bold")

dev.off()

}



# {
# natri_before_list$venn_df %>% 
#   rename(GTEx = gtex_beta,
#          "Natri 2022" := indo_beta,
#          "Estonian Biobank" := lepik_beta,
#          "TwinsUK" := twinsuk_beta) %>% 
#   mutate(across(-contains("egenes"),~as.logical(.))) %>% 
#   as_tibble() %>% 
#   ggplot(aes(A = GTEx, 
#              B = `Estonian Biobank`, 
#              C = `TwinsUK`,
#              D = `Natri 2022`)) +
#   geom_venn(fill_color = c(rep(european_col,3), indonesian_col),
#             stroke_color  =  c(rep(european_col,3), indonesian_col),
#             stroke_size = 0.5,
#             text_size = 12/.pt,
#             set_name_size = 12/.pt,
#             show_percentage = FALSE,
#             fill_alpha = 0.2) +
#   theme_void() +
#   coord_fixed()
# 
# 
# natri_after_list$venn_df %>% 
#   rename(GTEx = gtex_beta,
#          "Natri 2022" := indo_beta,
#          "Estonian Biobank" := lepik_beta,
#          "TwinsUK" := twinsuk_beta) %>% 
#   mutate(across(-contains("egenes"),~as.logical(.))) %>% 
#   as_tibble() %>% 
#   ggplot(aes(A = GTEx, 
#              B = `Estonian Biobank`, 
#              C = `TwinsUK`,
#              D = `Natri 2022`)) +
#   geom_venn(fill_color = c(rep(european_col,3), indonesian_col),
#             stroke_color  =  c(rep(european_col,3), indonesian_col),
#             stroke_size = 0.5,
#             text_size = 12/.pt,
#             set_name_size = 12/.pt,
#             show_percentage = FALSE,
#             fill_alpha = 0.2) +
#   theme_void() +
#   coord_fixed()


########### Plot upset plots: #########

{
  library(ComplexHeatmap)
  
  pdf(file = "thesis_figures/output_pdfs/eGene_upset_overlap.pdf",
      width = 6.22, 
      height = 5.51,
      family = "Helvetica")
  
  pageCreate(width = 158, 
             height = 140, 
             xgrid = 5,
             ygrid = 5,
             showGuides = FALSE,
             default.units = "mm")
  

m_natri_before = make_comb_mat(natri_before_list$venn_df %>% 
                                select(-egenes) %>% 
                                as_tibble() %>% 
                                rename(GTEx = gtex_beta,
                                                   "Natri 2022" := indo_beta,
                                                   "Estonian Biobank" := lepik_beta,
                                                   "TwinsUK" := twinsuk_beta))
ss = set_size(m_natri_before)
cs = comb_size(m_natri_before)

default_pars =  grid::gpar(fontsize = 8,
                           fontfamily="Helvetica")


################# A ######

A = grid.grabExpr(
  draw(
    UpSet(m_natri_before,
          set_order = order(ss),
          comb_order = order(-cs),
          column_title_gp = default_pars,
          row_title_gp = default_pars,
          column_names_gp = default_pars,
          row_names_gp = default_pars,
          top_annotation = upset_top_annotation(m_natri_before, 
                                                gp = default_pars,
                                                annotation_name_rot = 90,
                                                annotation_name_gp = default_pars,
                                                ylim = c(0,5000),
                                                axis_param = list(side = "left",
                                                                  at = seq(from = 0, 
                                                                           to = 5000,
                                                                           by = 1000)
                                                )),
          right_annotation = upset_right_annotation(m_natri_before,
                                                    annotation_name_gp = default_pars,
                                                    gp = grid::gpar(fontsize = 8, 
                                                                                    fontfamily="Helvetica",
                                                                                    fill = c(rep(european_col,3),
                                                                                             indonesian_col),
                                                                                    col = c(rep(european_col,3),
                                                                                            indonesian_col
                                                                                    )),
                                                    axis_param = list(side = "bottom",
                                                                      at = seq(from = 0, 
                                                                               to = 6500,
                                                                               by = 2000)))
                                              
          )
          # top_annotation = HeatmapAnnotation("Number of shared Indonesian eQTL" = 
          #                                      anno_barplot(cs, 
          #                                                   gp = gpar(fontsize = 8),
          #                                                   numbers_gp = gpar(fontsize = 8)),
          #                                    gp = gpar(fontsize = 8)),
          # left_annotation = rowAnnotation("Number of shared Indonesian eQTL" = 
          #                                   anno_barplot(ss,
          #                                                gp = gpar(fontsize = 8),
          #                                                numbers_gp = gpar(fontsize = 8)),
          #                                 gp = gpar(fontsize = 8))  
          
    )
  )

plotGG(plot = A,
       x = 1,
       y = 1.5,
       width = 78,
       height = 70,
       just = c("left", "top"),
       default.units = "mm")


plotText(label = "A", 
         fontsize = 12,
         x = 0.5, 
         y = 2.5, 
         just = "left",
         default.units = "mm",
         fontface = "bold",
         fontfamily="Helvetica")




########## B ################

m_natri_after = make_comb_mat(natri_after_list$venn_df %>% 
                                select(-egenes) %>% 
                                as_tibble() %>% 
                                rename(GTEx = gtex_beta,
                                       "Natri 2022" := indo_beta,
                                       "Estonian Biobank" := lepik_beta,
                                       "TwinsUK" := twinsuk_beta))

ss = set_size(m_natri_after)
cs = comb_size(m_natri_after)


B = grid.grabExpr(
  draw(
    UpSet(m_natri_after,
          set_order = order(ss),
          comb_order = order(-cs),
          column_title_gp = default_pars,
          row_title_gp = default_pars,
          column_names_gp = default_pars,
          row_names_gp = default_pars,
          top_annotation = upset_top_annotation(m_natri_after, 
                                                gp = default_pars,
                                                annotation_name_rot = 90,
                                                annotation_name_gp = default_pars,
                                                ylim = c(0,5000),
                                                axis_param = list(side = "left",
                                                                  at = seq(from = 0, 
                                                                           to = 5000,
                                                                           by = 1000)
                                                                                    )
                                                ),
          right_annotation = upset_right_annotation(m_natri_after,
                                                    gp = grid::gpar(fontsize = 8, 
                                                                    fontfamily="Helvetica",
                                                                    fill = c(rep(european_col,3),
                                                                              indonesian_col),
                                                                    col = c(rep(european_col,3),
                                                                            indonesian_col
                                                                            )),
                                                    annotation_name_gp = default_pars,
                                                    xlim = c(0,6500),
                                                    axis_param = list(side = "bottom",
                                                                      at = seq(from = 0, 
                                                                               to = 6500,
                                                                               by = 2000)
                                                    ))
                                                
         ))
    )


plotGG(plot = B,
       x = 1 + 78 +1,
       y = 1.5,
       width = 78,
       height = 70,
       just = c("left", "top"),
       default.units = "mm")

plotText(label = "B", 
         fontsize = 12,
         x = 0.5 + 78 + 0.5, 
         y = 2.5, 
         just = "left",
         default.units = "mm",
         fontface = "bold",
         fontfamily="Helvetica")

# mogil_before_list$venn_df %>% 
#   mutate(across(-contains("egenes"),~as.logical(.))) %>% 
#   as_tibble() %>% 
#   ggplot(aes(A = aa, B = his, C = ea)) +
#   geom_venn(fill_color = c(african_american_col, hispanic_col,european_col),
#             stroke_color  =  c(african_american_col, hispanic_col,european_col),
#             stroke_size = 0.5,
#             text_size = 12/.pt,
#             set_name_size = 12/.pt,
#             show_percentage = FALSE,
#             fill_alpha = 0.2) +
#   theme_void() +
#   coord_fixed()

################# C #####################

m_mogil_before = make_comb_mat(mogil_before_list$venn_df %>% 
                                as.data.frame() %>% 
                                select(-egenes) %>% 
                                 as_tibble() %>% 
                                 rename("African American" := aa,
                                        "European American" := ea,
                                        "Hispanic" := his))
ss = set_size(m_mogil_before)
cs = comb_size(m_mogil_before)


D = grid.grabExpr(
  draw(
    UpSet(m_mogil_before,
          set_order = order(ss),
          comb_order = order(-cs),
          column_title_gp = default_pars,
          row_title_gp = default_pars,
          column_names_gp = default_pars,
          row_names_gp = default_pars,
          top_annotation = upset_top_annotation(m_mogil_before, 
                                                gp = default_pars,
                                                annotation_name_rot = 90,
                                                annotation_name_gp = default_pars,
                                                ylim = c(0,5000),
                                                axis_param = list(side = "left",
                                                                  at = seq(from = 0, 
                                                                           to = 5000,
                                                                           by = 1000)
                                                )),
          right_annotation = upset_right_annotation(m_mogil_before,
                                                    annotation_name_gp = default_pars,
                                                    gp = grid::gpar(fontsize = 8, 
                                                                    fontfamily="Helvetica",
                                                                    fill = c(african_american_col,
                                                                             european_col,
                                                                             hispanic_col),
                                                                    col = c(african_american_col,
                                                                            european_col,
                                                                            hispanic_col)),
                                                    axis_param = list(side = "bottom",
                                                                      at = seq(from = 0, 
                                                                               to = 6500,
                                                                               by = 2000)))
          
    )
    # top_annotation = HeatmapAnnotation("Number of shared Indonesian eQTL" = 
    #                                      anno_barplot(cs, 
    #                                                   gp = gpar(fontsize = 8),
    #                                                   numbers_gp = gpar(fontsize = 8)),
    #                                    gp = gpar(fontsize = 8)),
    # left_annotation = rowAnnotation("Number of shared Indonesian eQTL" = 
    #                                   anno_barplot(ss,
    #                                                gp = gpar(fontsize = 8),
    #                                                numbers_gp = gpar(fontsize = 8)),
    #                                 gp = gpar(fontsize = 8))  
    
  )
)

plotGG(plot = D,
       x = 1.5,
       y = 1.5 + 70 + 1.5,
       width = 78,
       height = 70*0.87,
       just = c("left", "top"),
       default.units = "mm")

plotText(label = "C", 
         fontsize = 12,
         x = 0.5, 
         y = 2.5 + 70 + 1, 
         just = "left",
         default.units = "mm",
         fontface = "bold",
         fontfamily="Helvetica")


################### D ##########################

m_mogil_after = make_comb_mat(mogil_after_list$venn_df %>% 
                                 as.data.frame() %>% 
                                 select(-egenes) %>% 
                                 as_tibble() %>% 
                                 rename("African American" := aa,
                                        "European American" := ea,
                                        "Hispanic" := his))
ss = set_size(m_mogil_after)
cs = comb_size(m_mogil_after)


E = grid.grabExpr(
  draw(
    UpSet(m_mogil_after,
          set_order = order(ss),
          comb_order = order(-cs),
          column_title_gp = default_pars,
          row_title_gp = default_pars,
          column_names_gp = default_pars,
          row_names_gp = default_pars,
          top_annotation = upset_top_annotation(m_mogil_after, 
                                                gp = default_pars,
                                                annotation_name_rot = 90,
                                                annotation_name_gp = default_pars,
                                                ylim = c(0,5000),
                                                axis_param = list(side = "left",
                                                                  at = seq(from = 0, 
                                                                           to = 5000,
                                                                           by = 1000)
                                                )),
          right_annotation = upset_right_annotation(m_mogil_after,
                                                    annotation_name_gp = default_pars,
                                                    gp = grid::gpar(fontsize = 8, 
                                                                    fontfamily="Helvetica",
                                                                    fill = c(african_american_col,
                                                                             european_col,
                                                                             hispanic_col),
                                                                    col = c(african_american_col,
                                                                            european_col,
                                                                            hispanic_col)),
                                                    axis_param = list(side = "bottom",
                                                                      at = seq(from = 0, 
                                                                               to = 6500,
                                                                               by = 2000)))
          
    )
    # top_annotation = HeatmapAnnotation("Number of shared Indonesian eQTL" = 
    #                                      anno_barplot(cs, 
    #                                                   gp = gpar(fontsize = 8),
    #                                                   numbers_gp = gpar(fontsize = 8)),
    #                                    gp = gpar(fontsize = 8)),
    # left_annotation = rowAnnotation("Number of shared Indonesian eQTL" = 
    #                                   anno_barplot(ss,
    #                                                gp = gpar(fontsize = 8),
    #                                                numbers_gp = gpar(fontsize = 8)),
    #                                 gp = gpar(fontsize = 8))  
    
  )
)


plotGG(plot = E,
       x = 1.5 + 78,
       y = 2.5 + 70 + 1.5,
       width = 78,
       height = 70*0.87,
       just = c("left", "top"),
       default.units = "mm")

plotText(label = "D", 
         fontsize = 12,
         x = 0.5+ 0.5 + 78, 
         y = 2.5 + 70 + 1, 
         just = "left",
         default.units = "mm",
         fontface = "bold",
         fontfamily="Helvetica")

dev.off()

} 
  