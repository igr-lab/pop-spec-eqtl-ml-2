

# Figure 2 - Thesis - Population sharing of eQTLS

setwd(here::here())

############ Load relevant data: ####################

{

natri_mashr_model <- readRDS("output/mashr_results/natri_mashr_em_model.rds")

mogil_mashr_model <- readRDS("./output/mashr_results/mogil_only_mashr_em_model.rds")


natri_mashr_results = readRDS("./output/mashr_results/natri_mashr_em_results.rds")

mogil_mashr_results = readRDS("./output/mashr_results/mogil_only_mashr_em_results.rds")

}

########### Make matrices for Mashr sharing plots: parts A & B #################


{
natri_sharing = mashr::get_pairwise_sharing(natri_mashr_model,
                                            lfsr_thresh = 0.01)


mogil_sharing = mashr::get_pairwise_sharing(mogil_mashr_model,
                                            lfsr_thresh = 0.01)

} 


######### Make upset plot binary matrices: parts D - G #############

{
library(magrittr)
library(dplyr)

hispanic_col = c("#009988")
shared_col = c("#000000")
european_col = c("#0077BB")
indonesian_col = c("#CC3311")
african_american_col = c("#EE3377")
na_col = c("#888888")
}

{

# function to make binary matrix for upset plots
make_binary_mat = function(mashr_results,
                           spec_pops, #primary - or 
                           shared_pops,
                           lfsr = 0.01,
                           beta_thresh = 0.5){
  
  # Get significant eQTLs (from the primary population)
  sig_res = mashr_results$lfsr %>%
    as.data.frame() %>%
    dplyr::filter(dplyr::if_any(dplyr::all_of(spec_pops), ~.x<lfsr)) %>% 
    tibble::rownames_to_column("gene_snp") %$%
    gene_snp
  
  
  # Get the effect sizes of these significant eQTLs
  to_be_called_df = mashr_results$PosteriorMean[sig_res,] %>% 
    as.data.frame() #%>% 
  #tibble::rownames_to_column("gene_snp")
  
  # Call these eQTLs shared between the primary and contrast population dataset
  # if the ratio of effect sizes is within beta_thesh and 1/beta_thresh
  call_shared_spec = to_be_called_df %>% 
    dplyr::mutate(
      dplyr::across(dplyr::all_of(shared_pops),
                    ~dplyr::between(!!dplyr::sym(spec_pops)/(.x),
                                    beta_thresh,
                                    1/beta_thresh
                    )
      )
    )
  
  binary_matrix = call_shared_spec %>% 
    
    dplyr::select(dplyr::all_of(shared_pops),
                  dplyr::all_of(spec_pops)
    ) %>% 
    
    dplyr::mutate(dplyr::across(dplyr::all_of(spec_pops),
                                ~TRUE)
    ) %>% 
    
    dplyr::mutate(dplyr::across(dplyr::everything(),
                                ~as.numeric(.x)
    )
    )
  
  
  return(binary_matrix)
  
}



natri_indo_bin_mat = make_binary_mat(natri_mashr_results, 
                                     c("indo_beta"), 
                                     c("gtex_beta", "lepik_beta", "twinsuk_beta"))

colnames(natri_indo_bin_mat) = c("European (GTEx)", 
                                 "European (Estonian Biobank)", 
                                 "European (TwinsUK)",
                                 "Indonesia")


m_natri_indo = ComplexHeatmap::make_comb_mat(natri_indo_bin_mat)



natri_gtex_bin_mat = make_binary_mat(natri_mashr_results, 
                                     c("gtex_beta"), 
                                     c("indo_beta", "lepik_beta", "twinsuk_beta"))

colnames(natri_gtex_bin_mat) = c("Indonesia",
                                 "European (Estonian Biobank)", 
                                 "European (TwinsUK)",
                                 "European (GTEx)")

m_natri_gtex = ComplexHeatmap::make_comb_mat(natri_gtex_bin_mat)



# African American Upset

mogil_aa_bin_mat = make_binary_mat(mogil_mashr_results, 
                                   c("aa"), 
                                   c("his", "ea"))



colnames(mogil_aa_bin_mat) = c("Hispanic",
                                 "European American",
                                 "African American") 


m_mogil_aa = ComplexHeatmap::make_comb_mat(mogil_aa_bin_mat)


# European American Upset

mogil_ea_bin_mat = make_binary_mat(mogil_mashr_results, 
                                   c("ea"), 
                                   c("aa", "his"))


colnames(mogil_ea_bin_mat) = c("African American",
                                 "Hispanic",
                                 "European American") 

m_mogil_ea = ComplexHeatmap::make_comb_mat(mogil_ea_bin_mat)

# Hispanic

mogil_his_bin_mat = make_binary_mat(mogil_mashr_results, 
                                   c("his"), 
                                   c("aa", "ea"))

colnames(mogil_his_bin_mat) = c("African American",
                                 "European American",
                                 "Hispanic") 

m_mogil_his = ComplexHeatmap::make_comb_mat(mogil_his_bin_mat)


}

######################### Plot setup #########################

{
  library(plotgardener)
  library(dplyr)
  library(magrittr)
}

{ 
  pdf(file = "thesis_figures/output_pdfs/Figure_4.pdf",
      width = 5.59, 
      height = 3.03,
      family = "Helvetica")
  
  pageCreate(width = 142, 
             height = 77, 
             xgrid = 5,
             ygrid = 5,
             showGuides = FALSE,
             default.units = "mm")
  
  
  #
  
  # Heatmap settings 
  
  library(circlize)
  library(viridis)
  library(ComplexHeatmap)
  library(mashr)
  
  
  col_fun = colorRamp2(seq(from = 0, to = 1, by = 0.2), 
                       plasma(n = 6, direction = 1))

############## 2A - Heatmap Sharing, Mogil et al. 2018 (Monocytes) ############

plotText(label = "A", 
         fontsize = 12,
         x = 0.5, 
         y = 2.5, 
         just = "left",
         default.units = "mm",
         fontface = "bold",
         fontfamily="Helvetica")


  colnames(natri_sharing) = c("European (TwinsUK)",
                              "European (GTEx)", 
                              "European (Estonian Biobank)",
                              "Indonesia (Natri. 2022)")
  
  rownames(natri_sharing) =  c("European (TwinsUK)",
                               "European (GTEx)", 
                               "European (Estonian Biobank)",
                               "Indonesia (Natri. 2022)")
  

Fig_2A = grid.grabExpr(draw(
  Heatmap(mat = natri_sharing,  
          col = col_fun,
          name = "Pairwise Sharing",
          column_names_gp = grid::gpar(fontsize = 8),
          row_names_gp = grid::gpar(fontsize = 8),
          clustering_distance_rows = function(m) dist(m),
          heatmap_legend_param = list(direction = "horizontal",
                                      title_position = "topcenter",
                                      labels_gp = gpar(fontsize = 8,
                                                       fontface = "plain"),
                                      title_gp=gpar(fontsize=8, 
                                                    fontface="plain")),
          show_heatmap_legend  = FALSE), #heatmap_legend_side = "bottom",
  
))




plotGG(plot = Fig_2A,
       x = 1.5,
       y = 4.5,
       width =  78,
       height = 73.8,
       just = c("left", "top"), 
       default.units = "mm")




################# 2B. Heatmap sharing Natri et al. 2020 / 2021 #################

plotText(label = "B", 
         fontsize = 12,
         x = 75+8.375, 
         y = 2.5, 
         just = "left",
         default.units = "mm",
         fontface = "bold")

colnames(mogil_sharing) = c("African American", 
                            "Hispanic", 
                            "European American")

rownames(mogil_sharing) =  c("African American", 
                             "Hispanic", 
                             "European American")


Fig_2B = grid.grabExpr(draw(
  Heatmap(mat = mogil_sharing,  
          col = col_fun,
          name = "Pairwise Sharing",
          column_names_gp = grid::gpar(fontsize = 8,fontfamily="Helvetica"),
          row_names_gp = grid::gpar(fontsize = 8, fontfamily="Helvetica"),
          clustering_distance_rows = function(m) dist(m),
          heatmap_legend_param = list(direction = "horizontal",
                                      title_position = "topcenter",
                                      labels_gp = gpar(fontsize = 8,
                                                       fontface = "plain",
                                                       fontfamily="Helvetica"),
                                      title_gp=gpar(fontsize=8, 
                                                    fontface="plain",
                                                    fontfamily="Helvetica"))
  ), heatmap_legend_side = "bottom"
))


plotGG(plot = Fig_2B,
       x = 76.5+8.375,
       y = 4.5,
       width = 58,
       height = 69,
       just = c("left", "top"), 
       default.units = "mm")

# random_mat = function(nr) {
#   m = matrix(rnorm(10*nr), nc = 10)
#   colnames(m) = letters[1:10]
#   return(m)
# }
# 
# y = NULL
# 
# for(nr in c(3, 4)) {
#   ht = draw(Heatmap(random_mat(nr), height = unit(5, "mm")*nr, 
#                     column_title = "foo", # one line text
#                     top_annotation = HeatmapAnnotation(bar = 1:10)))
#   ht_height = sum(component_height(ht)) + unit(4, "mm")
#   ht_height = convertHeight(ht_height, "inch", valueOnly = TRUE)
#   y = c(y, ht_height)
# }
# 
# x = c(3, 4)
# lm(y ~ x)

#pageGuideHide()

dev.off()

}
