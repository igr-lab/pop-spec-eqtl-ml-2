


# Proportion of portable / non-portable over time
{
  
  library(dplyr)
  library(magrittr)
  library(ggplot2)
  library(plotgardener)
  
custom_theme <-
  list(
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        text = element_text(size = 8),
        legend.position = "right",
        legend.box = "vertical",
        strip.background = element_blank(),
        legend.text=element_text(size=8),
        legend.title = element_text(size=8),
        legend.margin=margin(),
        axis.text = element_text(size = 8),
        axis.text.x = element_text(size = 8, angle = 0, vjust = 0.5)
      )
  )

# Plotting colours
hispanic_col = c("#009988")
shared_col = c("#000000")
european_col = c("#0077BB")
indonesian_col = c("#CC3311")
african_american_col = c("#EE3377")
na_col = c("#888888")

}


{
  natri_mashr_results = readRDS("./output/mashr_results/natri_mashr_em_results.rds")
  
  mogil_mashr_results = readRDS("./output/mashr_results/mogil_only_mashr_em_results.rds")
  
}

########## Function: Calculate portable percentage, given data frame and factor name #######

# portable_percent = function(file_name, 
#                             factor,
#                             true_total = TRUE){
#   
#   box::use(dplyr[...])
#   
#   df = data.table::fread(file_name)
#   
#   if(!is.numeric(true_total)){
#   percent_df = df %>% 
#                group_by(Class) %>% 
#                summarise(n = n()) %>%
#                mutate(percent = 100*n / sum(n)) %>% 
#                mutate(factor = factor)
#     
#   }
#   
#    return(percent_df)
# }

############# Function: Get portable percentage for al tested factors #############

# get_all_portable_percent = function(factor_0.5_file_name, 
#                                     factors = c(0,0.2,0.5,0.7),
#                                     true_total = TRUE){
#   
#   
#  list_portable_percent = lapply(factors,
#         function(factor) {portable_percent(file_name = gsub("0.5", 
#                                                             factor, 
#                                                             factor_0.5_file_name),
#                                            factor = factor,
#                                            true_total = true_total)}
#  )
#     
#   final_df = data.table::rbindlist(list_portable_percent) %>% 
#              mutate(definition = case_when(factor == 0 ~ "Same direction",
#                                   TRUE ~ paste0("Within a\nfactor of ", factor)))
#   
#   return(final_df)
#     
# }

###### Make binary dataframe of eQTL portability - like for upset plots ######

make_binary_mat = function(mashr_results,
                           spec_pops, #primary - or 
                           shared_pops,
                           lfsr = 0.01,
                           beta_thresh = 0.5){
  
  # Get significant eQTLs (from the primary population)
  sig_res = mashr_results$lfsr %>%
    as.data.frame() %>%
    dplyr::filter(dplyr::if_any(dplyr::all_of(spec_pops), ~.x<lfsr)) %>% 
    tibble::rownames_to_column("gene_snp") %$%
    gene_snp
  
  
  # Get the effect sizes of these significant eQTLs
  to_be_called_df = mashr_results$PosteriorMean[sig_res,] %>% 
    as.data.frame() #%>% 
  #tibble::rownames_to_column("gene_snp")
  
  if(beta_thresh ==0){
    
    call_shared_spec = to_be_called_df %>% 
      dplyr::mutate(
        dplyr::across(dplyr::all_of(shared_pops),
                      ~sign(.x) == sign(!!dplyr::sym(spec_pops))
        )
      ) 
    
  } else {
  
  # Call these eQTLs shared between the primary and contrast population dataset
  # if the ratio of effect sizes is within beta_thesh and 1/beta_thresh
  call_shared_spec = to_be_called_df %>% 
    dplyr::mutate(
      dplyr::across(dplyr::all_of(shared_pops),
                    ~dplyr::between(!!dplyr::sym(spec_pops)/(.x),
                                    beta_thresh,
                                    1/beta_thresh
                    )
      )
    )
  }
  
  binary_matrix = call_shared_spec %>% 
    
    dplyr::select(dplyr::all_of(shared_pops),
                  dplyr::all_of(spec_pops)
    ) %>% 
    
    dplyr::mutate(dplyr::across(dplyr::all_of(spec_pops),
                                ~TRUE)
    ) %>% 
    
    dplyr::mutate(dplyr::across(dplyr::everything(),
                                ~as.numeric(.x)
    )
    )
  
  
  return(binary_matrix)
  
}


###### Function for using binary matrix to classifiy eQTLs #######

make_port_percent = function(mashr_results,
                             spec_dataset,
                             shared_datasets,
                             factors = c(0,0.2,0.5,0.7)){
  
  list_portable_percent =  lapply(factors,
                                  function(factor){
                                    make_binary_mat(mashr_results,
                                                    spec_dataset, 
                                                    shared_datasets,
                                                    beta_thresh = factor
                                    ) %>% 
                                      mutate(factor = factor)})
  
  final_df = data.table::rbindlist(list_portable_percent) %>% 
    mutate(definition = case_when(factor == 0 ~ "Same direction",
                                  TRUE ~ paste0("Within a\nfactor of ", factor)))
  
  
  return(final_df)
}

############# Function to calc. the percentage of eQTLs, for a given def. of portability - are portable #####

calc_percent = function(df){
  
  percent_df = df %>% 
               collect(.) %>% 
               group_by(factor, Class) %>% 
               summarise(n = n()) %>% 
               mutate(percent = 100*n / sum(n))
  
  return(percent_df)
}


############### Calculate

natri_euro_portable_percent  = make_port_percent(natri_mashr_results,
                  c("gtex_beta"),
                  c("indo_beta", "twinsuk_beta", "lepik_beta")) %>% 
  mutate(Class = case_when(twinsuk_beta + lepik_beta < 1 ~ "Ambigous\n(Filtered)",
                          indo_beta ==1 ~ "Portable\n(Europe to Indonesia)",
                           TRUE ~ "Non-portable\n(Europe to Indonesia)")) %>% 
  calc_percent(.)


natri_indo_portable_percent  = make_port_percent(natri_mashr_results,
                                                             c("indo_beta"),
                                                             c("gtex_beta", "twinsuk_beta", "lepik_beta")) %>% 
  mutate(Class = case_when(twinsuk_beta + lepik_beta + gtex_beta > 1 ~ "Portable\n(Indonesia to Europe)",
                           twinsuk_beta + lepik_beta + gtex_beta == 1 ~ "Ambigious\n(Filtered)",
                           TRUE ~ "Non-portable\n(Indonesia to Europe)")) %>% 
  calc_percent(.)

mogil_ea_portable_percent = make_port_percent(mogil_mashr_results,
                                                c("ea"),
                                                c("aa", "his")) %>% 
  mutate(Class = case_when(aa + his ==2 ~ "Portable\n(European American \nto Hispanic and African American)",
                           his == 1 ~ "Portable\n(European American to Hispanic)",
                           aa == 1 ~ "Portable\n(European American to\nAfrican American)",
                           TRUE ~ "Non-portable\n(European American to\nHispanic and African American)")) %>% 
  calc_percent(.)

mogil_aa_portable_percent = make_port_percent(mogil_mashr_results,
                                                c("aa"),
                                                c("ea", "his")) %>% 
  mutate(Class = case_when(ea + his ==2 ~ "Portable\n(African American to\n Hispanic and European American)",
                           his == 1 ~ "Portable\n(African American to\nHispanic)",
                           ea == 1 ~ "Portable\n(African American to\nEuropean American)",
                           TRUE ~ "Non-portable\n(African American to\nHispanic and European American)")) %>% 
  calc_percent(.)

mogil_his_portable_percent = make_port_percent(mogil_mashr_results,
                                              c("his"),
                                              c("ea", "aa")) %>% 
  mutate(Class = case_when(ea + aa ==2 ~ "Portable\n(Hispanic to\nAfrican American and European American)",
                           aa == 1 ~ "Portable\n(Hispanic to\nAfrican American)",
                           ea == 1 ~ "Portable\n(Hispanic to\nEuropean American)",
                           TRUE ~ "Non-portable\n(Hispanic to African American\n and European American)")) %>% 
  calc_percent(.)


################# Plot - Natri et al. ###########

{ 
  library(plotgardener)
  
  pdf(file = "thesis_figures/output_pdfs/percentage_portable_natri.pdf",
      width = 5.59, 
      height = 5.91,
      family = "Helvetica")
  
  pageCreate(width = 142, 
             height = 150, 
             xgrid = 5,
             ygrid = 5,
             showGuides = FALSE,
             default.units = "mm")

############# A - Natri Indonesia ##########

A = natri_indo_portable_percent %>% 
  mutate(definition = case_when(factor == 0 ~ "Same direction",
                                TRUE ~ paste0("Within a\nfactor of ", factor))) %>%
  # mutate(Class = case_when(Class == "shared" ~ "Portable\n(Indonesia to Europe)",
  #                          grepl("ambigous", Class) ~ "Ambigous\n(Filtered)",
  #                          TRUE ~ "Non-portable\n(Indonesia to Europe)")) %>% 
ggplot() + 
  geom_bar(aes(y=percent, 
               fill = Class, 
               x= definition),
           stat="identity") + 
  custom_theme +
  labs(x = "",
       y = "% of Indonesian (Natri. 2022) eQTLs") + 
  scale_fill_manual(values = c(na_col, 
                               indonesian_col, 
                               shared_col))

  
plotGG(plot = A,
         x = 1.5,
         y = 1.5,
         width =  140,
         height = 70,
         just = c("left", "top"), 
         default.units = "mm")
  

################# B ####################

B = natri_euro_portable_percent %>%
  mutate(definition = case_when(factor == 0 ~ "Same direction",
                                TRUE ~ paste0("Within a\nfactor of ", factor))) %>%
  # mutate(Class = case_when(Class == "shared" ~ "Portable\n(Europe to Indonesia)",
  #                          grepl("ambigous", Class) ~ "Ambigous\n(Filtered)",
  #                          TRUE ~ "Non-portable\n(Europe to Indonesia)")) %>%
  ggplot() +
  geom_bar(aes(y=percent,
               fill = Class,
               x= definition),
           stat="identity") +
  custom_theme +
  labs(x = "",
       y = "% of GTEx eQTLs") +
  scale_fill_manual(values = c(na_col,
                               european_col,
                               shared_col))

plotGG(plot = B,
       x = 1.5,
       y = 1.5 + 70 + 1.5,
       width =  140,
       height = 70,
       just = c("left", "top"), 
       default.units = "mm")

  
dev.off()  
}


################ Plot mogil #####################

{
  
  library(plotgardener)
  library(ggpattern)
  
  pdf(file = "thesis_figures/output_pdfs/percentage_portable_mogil.pdf",
      width = 5.59, 
      height = 8.27,
      family = "Helvetica")
  
  pageCreate(width = 142, 
             height = 210, 
             xgrid = 5,
             ygrid = 5,
             showGuides = FALSE,
             default.units = "mm")
  
  
  A = mogil_aa_portable_percent %>% 
    mutate(definition = case_when(factor == 0 ~ "Same direction",
                                  TRUE ~ paste0("Within a\nfactor of ", factor))) %>%
    # mutate(Class = case_when(Class == "shared" ~ "Portable\n(Indonesia to Europe)",
    #                          grepl("ambigous", Class) ~ "Ambigous\n(Filtered)",
    #                          TRUE ~ "Non-portable\n(Indonesia to Europe)")) %>% 
    ggplot() + 
    geom_bar(aes(y=percent, 
                 fill = Class, 
                 x= definition),
             stat="identity") + 
    # geom_bar_pattern(aes(y=percent, 
    #                      fill = Class, 
    #                      pattern = Class,
    #                      x= definition,
    #                      pattern_angle = Class,
    #                      pattern_fill = Class),
    #                  pattern_size = 0,
    #                  pattern_density = 0.4,
    #                  pattern_spacing = 0.02,
    #                  pattern_colour= shared_col,
    #                  stat="identity") + 
    custom_theme +
    labs(x = "",
         y = "% of African American eQTLs") + 
    scale_fill_manual(values = c(african_american_col,
                                 shared_col,
                                 european_col,
                                 hispanic_col)
                      ) 
    # scale_pattern_manual(values=c('none', 'none', 'stripe', 'stripe'))  + 
    # #scale_pattern_type_manual(values=c(NA, 'triangle', 'sine')) + 
    # scale_pattern_fill_manual(values = c(NA,
    #                                      NA,
    #                                      european_col,
    #                                      hispanic_col)) +
    # scale_pattern_angle_manual(values =c(NA,
    #                                      NA,
    #                                      90,
    #                                      0))
  
  plotGG(plot = A,
         x = 1.5,
         y = 1.5,
         width =  140,
         height = 70,
         just = c("left", "top"), 
         default.units = "mm")
  
  
################ B ##################
  
  B = mogil_his_portable_percent %>% 
    mutate(definition = case_when(factor == 0 ~ "Same direction",
                                  TRUE ~ paste0("Within a\nfactor of ", factor))) %>%
    # mutate(Class = case_when(Class == "shared" ~ "Portable\n(Indonesia to Europe)",
    #                          grepl("ambigous", Class) ~ "Ambigous\n(Filtered)",
    #                          TRUE ~ "Non-portable\n(Indonesia to Europe)")) %>% 
    ggplot() + 
    geom_bar(aes(y=percent, 
                 fill = Class, 
                 x= definition),
             stat="identity") + 
    custom_theme +
    labs(x = "",
         y = "% of Hispanic eQTLs") + 
    scale_fill_manual(values = c(hispanic_col,
                                 shared_col,
                                 african_american_col,
                                 european_col))
  
  
  plotGG(plot = B,
         x = 1.5,
         y = 1.5 + 70 + 1.5,
         width =  140,
         height = 70,
         just = c("left", "top"), 
         default.units = "mm")
  
############# C #####################
  
  
  C = mogil_ea_portable_percent %>% 
    mutate(definition = case_when(factor == 0 ~ "Samedirection",
                                  TRUE ~ paste0("Within a\nfactor of ", factor))) %>%
    # mutate(Class = case_when(Class == "shared" ~ "Portable\n(Indonesia to Europe)",
    #                          grepl("ambigous", Class) ~ "Ambigous\n(Filtered)",
    #                          TRUE ~ "Non-portable\n(Indonesia to Europe)")) %>% 
    ggplot() + 
    geom_bar(aes(y=percent, 
                 fill = Class, 
                 x= definition),
             stat="identity") + 
    custom_theme +
    labs(x = "",
         y = "% of European American eQTLs") + 
    scale_fill_manual(values = c(european_col,
                                 african_american_col,
                                 shared_col,
                                 european_col))
  
  
  plotGG(plot = C,
         x = 1.5,
         y = 1.5 + 70 + 1.5 + 70 + 1.5,
         width =  140,
         height = 70,
         just = c("left", "top"), 
         default.units = "mm")
  
  dev.off()
  
  
  
}
