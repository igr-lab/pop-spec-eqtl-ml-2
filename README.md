
# Population-Specific eQTL Machine Learning Project (pop_spec_eqtl_ml)

### [pop_spec_eqtl_ml project site link](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/index.html)

A [workflowr project](https://github.com/workflowr/workflowr)

> **Overview / Introduction:** This is the project repository made to contain and explain scripts etc. for using multi-population eQTL summary statistics to train machine learning (ML) models to predict whether a given eQTL is shared between human populations. <br> </br>                                            <br> </br> **Useful Notes / Resources:**
<br> </br> To see and perhaps run thorugh an **example of the project pipeline** (from eQTL summary statistics to built machine learning moddels) see this example pipeline [here.](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/example_pipeline.html)
<br>  <br> To see an **visual overview of the project pipeline**, and in particular how data moves through each of the folders in this repository see this figure [here.](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/pipeline_overview.html)
<br> <br> **Software requirements:** This project uses many R and python packages, to track these packages and the versions of R and python we use both: a **conda environment** (for python and the machine learning steps of the pipeline) - and **the R-package renv** (for R package management and tracking - started using renv from commit: 8bea5cd3ac4c4466d05e57d9be286db4db6887a2). <br> <br> To match the package and software versions of the machine learning pipeline steps, you can create a conda environment from the python_envs/ml_pipeline_env.yml file. Furthermore, to easily ensure you are using the same R package versions as this project, after cloning this project, you can install ```renv``` from CRAN, and then run ```renv::restore()``` (Note: you can also specificy a particular commit for which to restore the exact package versions, to get exactly the same results as we did using ```renv::revert()```. For more information on using renv see [here](https://rstudio.github.io/renv/articles/collaborating.html)). <br> <br> We've run the machine learning pipeline with python v3.7.4 (using slurm modules) and also python v3.8.2 (the same version as the virtual environment). The R version used was initally 4.0.0, but we later switched to using 4.1.0 (this change was tracked by renv - so you can reproduce the exact package versions used to produce these earlier results with R v4.0.0 using renv as discussed above). 

<br> </br>


## Repository folder descriptions

| Folder | Description |
| --- | --- |
| **analysis** | .Rmd files documenting analysis scripts + methods, published on the workflowr site (link [here](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/index.html). These were published using ``` workflowr::wflow_publish ```. | 
| **analysis_ml_runs** | Similar to above (analysis folder), but .Rmd documents in this folder exclusively contain investigations into specific machine learning models. Since workflowr only supports building and publishing .Rmd files from the analysis subfolder, these were published using a wrapper function, ```wflow_dir_publish```, which wraps around workflowr functions (``` workflowr::wflow_git_commit ``` and ``` workflowr::wflow_git_build```) to temporarily build a .Rmd file in the analysis subfolder, and publish this, and then remove the analysis subfolder copy. <br> ```wflow_dir_publish``` is available from and documented using roxygen in ```code/workflowr_wrapper_functions.R```. To access the ```wflow_dir_publish``` function using box (install from CRAN) run in R: ``` options(box.path = getwd()); box::use(code/workflowr_wrapper_functions); ```. From here, to use ```wflow_dir_publish``` run  ```workflowr_wrapper_functions$wflow_dir_publish```.  For help / more information about this function you can run:  ```box::help(workflowr_wrapper_functions$wflow_dir_publish)```. |
|**code** | Contains scripts used for analysis and investigation (which are not displayed on the workflowr site) - see folder README. Mostly contains scripts for the machine learning pipeline including functions for feature extraction, calling eQTLs population specific and for building and testing machine learning models. See folder README for more detail. |
| **data** | Contains raw data taken directly from various sources (for example summary statistics from the eQTL catalogue) - see this folder README for more detail - including the date and exact location data was download from.|
| **ml_runs** | Folder containing subfolders with model testing results, plots and information on how each model was produced. Each subfolder refers to a single built model. A summary table of the results of each run can be found [here](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/summary_details_ml_runs.html). See respective folder README for more detail.| 
| **output** | Contains data which has been processed in some way (for example, summary statistics from multiple studies that have been combined into a single dataset) - see respective folder README for more detail. | 
| **public** | .html files and figures which comprise the workflowr site. These were produced running workflowr commands on the .Rmd files in analysis and ml_analysis |
| **python_envs** | Conda environments and .yml files exported from these environments to recreate the exact package versions used in this project with the machine learning / python steps | 
| **renv** | Contains the activate R file - which in conjunction with the renv.lock file enables the use of the R package renv to match the exact packages and their versions used to produce results in this project | 
| **test_data**  | Folder for script to produce testing data for machine learning model (also contains .rds of a random sample of eQTLs from Natri et al). This data is intended to allow other users to quickly test scripts, and try out the machine learning pipeline. This is the data used in the example pipeline, shown [here](https://igr-lab.pages.gitlab.unimelb.edu.au/pop_spec_eqtl_ml/example_pipeline.html) | 
| **segment_liftover** | Folder containing scripts, steps, and ucsc_liftover for using segment_liftover with encode DNAse I footprint .bed files (see respective README in folder). |




